PROJECT_ROOT:=$(shell pwd)
OUTPUT_DIR:=$(PROJECT_ROOT)/output/
OBJECTS_DIR:=$(OUTPUT_DIR)/objects
DEPFILE:=$(OBJECTS_DIR)/deps
TARGET_NAME:=$(OUTPUT_DIR)/bin/app

#定义软件发布名称及包路径
APP_NAME:=embedme
APP_DIR:=$(OUTPUT_DIR)/$(APP_NAME).app

CORE_NUM:=2

#在此定义编译环境
ADD_CFLAGS := -g
#ADD_CFLAGS += -Wall	#警告
#ADD_CFLAGS += -O3		#优化级别

ifeq ($(DEBUG),ON)
ADD_CFLAGS += -DDEBUG_ON
endif

OS_NAME := $(shell uname -o)
LC_OS_NAME := $(shell echo $(OS_NAME) | tr '[A-Z]' '[a-z]')
ifeq ($(findstring cygwin,$(LC_OS_NAME)),cygwin)
OS_TYPE := OS_CYGWIN
ADD_CFLAGS += -DOS_CYGWIN
else ifeq ($(findstring linux,$(LC_OS_NAME)),linux)
OS_TYPE := OS_UNIXLIKE
else
$(error "Not support os $(LC_OS_NAME)")
endif

ifeq ($(SHARED),yes)
SHARED:=yes
else
SHARED:=no
endif

.PHONY:default
default:
	@echo -e '\033[33m\033[1musage: make help|all|clean|opensource|libemb|app|update [HOST=xxx]\033[0m'
#############################################################
#不要改动以下脚本,除非你确切知道你正在做什么!!!
#############################################################
.PHONY:all
all:
	@echo -e '\033[33m\033[1m==============================================';
	@echo -e '==>  EMBEDME project start building......  <==';
	@echo -e '==>  FOR HELP just run "\033[0m\033[31m\033[1mmake help\033[0m\033[33m\033[1m"         <=='
	@echo -e '==============================================\033[0m'
	@sleep 2
	make opensource
	make libemb
	make app
	make release
	
.PHONY:opensource
opensource:
	@echo -e '\033[33m\033[1mopensource building...more info please run\033[0m\033[31m\033[1m "make help"\033[0m';
	make PREFIX=$(OUTPUT_DIR) ADD_CFLAGS="$(ADD_CFLAGS)" HOST=$(HOST) SHARED=$(SHARED) -C $(PROJECT_ROOT)/opensource OS_TYPE=$(OS_TYPE) -j$(CORE_NUM)
	
.PHONY:libemb
libemb:
	@echo -e '\033[33m\033[1mlibemb building...more info please run\033[0m\033[31m\033[1m "make help"\033[0m';
	make PREFIX=$(OUTPUT_DIR) clean -C $(PROJECT_ROOT)/libemb -j$(CORE_NUM)
	make PREFIX=$(OUTPUT_DIR) ADD_CFLAGS="$(ADD_CFLAGS)" HOST=$(HOST) SHARED=$(SHARED) -C $(PROJECT_ROOT)/libemb OS_TYPE=$(OS_TYPE) -j$(CORE_NUM)


	
.PHONY:app
app:
	@echo -e '\033[33m\033[1mapp building...more info please run\033[0m\033[31m\033[1m "make help"\033[0m';
	make PREFIX=$(OUTPUT_DIR) ADD_CFLAGS="$(ADD_CFLAGS)" HOST=$(HOST) SHARED=$(SHARED) -C $(PROJECT_ROOT)/app OS_TYPE=$(OS_TYPE) -j$(CORE_NUM) 

.PHONY:update
update:
	@echo -e '\033[33m\033[1mupdate building...more info please run\033[0m\033[31m\033[1m "make help"\033[0m';
	rm -rf $(OUTPUT_DIR)/inc/*
	make libemb
	make app
	make release

.PHONY:help
help:
	@echo -e "\033[33m\033[1m---------------------------make help--------------------------------"
	@echo -e '\033[33m\033[1musage: make help|all|clean|opensource|libemb|app|update [HOST=xxx]\033[0m'
	@echo -e 'If it is the first time you build it,just run "make clean" at first.'
	@echo -e 'If you want to build all in one time,just run "make all".\033[0m'
	@echo -e "\033[31m\033[1m==>BUILDING STEPS:\033[0m"
	@echo -e '\033[32m\033[1m1.Build opensource lib with "make opensource".'
	@echo -e '2.Build libemb with "make libemb".'
	@echo -e '3.Build your app with "make app".\033[0m'
	@echo -e "\033[31m\033[1m==>IMPORTANT TIPS:\033[0m"
	@echo -e "\033[33m\033[1mIf you want to cross compile,you should add 'HOST=xxx' like this:\033[0m"
	@echo -e "\033[32m\033[1mmake opensource HOST=arm-linux"
	@echo -e "make libemb HOST=arm-linux"
	@echo -e "make app HOST=arm-linux\033[0m"
	@echo -e "\033[33m\033[1m--------------------------------------------------------------------\033[0m"

.PHONY:clean
clean:
	@echo -e '\033[33m\033[1mcleaning the whole project...\033[0m';
	make clean -C $(PROJECT_ROOT)/opensource -j
	make PREFIX=$(OUTPUT_DIR) clean -C $(PROJECT_ROOT)/libemb -j$(CORE_NUM)
	rm -rf $(OUTPUT_DIR)/bin/*
	rm -rf $(OUTPUT_DIR)/inc/*
	rm -rf $(OUTPUT_DIR)/include/*
	rm -rf $(OUTPUT_DIR)/lib/*
	rm -rf $(OUTPUT_DIR)/objects/*
	rm -rf $(OUTPUT_DIR)/share/*
	rm -rf $(APP_DIR)

.PHONY:release
release:
	-rm -rf $(APP_DIR)
	-mkdir $(APP_DIR)
	-mkdir $(APP_DIR)/lib
	@touch $(APP_DIR)/startapp
	@echo "#!/bin/sh" >> $(APP_DIR)/startapp
	@echo 'export LD_LIBRARY_PATH=$$LD_LIBRARY_PATH:$$(dirname $$0)/lib' >> $(APP_DIR)/startapp
	@echo 'cd $$(dirname $$0)/' >> $(APP_DIR)/startapp
	@echo './$(APP_NAME)' >> $(APP_DIR)/startapp
	@chmod a+x $(APP_DIR)/startapp
	@cp $(TARGET_NAME) $(APP_DIR)/$(APP_NAME)
ifeq ($(OS_TYPE),OS_CYGWIN)
	@cp -pR $(OUTPUT_DIR)/bin/*.dll* $(APP_DIR)/
endif
ifeq ($(SHARED),yes)
	@cp -pR $(OUTPUT_DIR)/lib/*.so* $(APP_DIR)/lib
endif
	@-rm -rf $(OBJECTS_DIR)/*
	@echo -e "\033[33m\033[1m==>release successfully,you can get package: $(APP_DIR)\033[0m"
	