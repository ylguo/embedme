#include "Tracer.h"
#include "RegExp.h"
void TestRegExp(void)
{
    RegExp regExp;
    StringArray strArray;
    IntArray posArray;
    std::string pattern="hel*o";
    std::string source="aaaaahelllo";
    std::string result;
    if(regExp.match(pattern,source))
    {
        TRACE_YELLOW("%s match pattern[%s]\n",source.c_str(),pattern.c_str());
    }
    else
    {
        TRACE_RED("%s don't match pattern[%s]\n",source.c_str(),pattern.c_str());
    }

    /* 电话号码 */
    pattern = "[0-9]{3}-[0-9]{8}|[0-9]{4}-[0-9]{8}";//国内电话号码
    //pattern = "\\d{4}-\\d{8}";//国内电话号码--\d有问题,无法匹配
    source="0755-26799999";
    if(regExp.match(pattern,source))
    {
        TRACE_YELLOW("%s match pattern[%s]\n",source.c_str(),pattern.c_str());
    }
    else
    {
        TRACE_RED("%s don't match pattern[%s]\n",source.c_str(),pattern.c_str());
    }

    pattern = "[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}";//ip地址
    source="192.168.1.1";
    if(regExp.match(pattern,source))
    {
        TRACE_YELLOW("%s match pattern[%s]\n",source.c_str(),pattern.c_str());
    }
    else
    {
        TRACE_RED("%s don't match pattern[%s]\n",source.c_str(),pattern.c_str());
    }
    pattern = "\\w+([-+.]\\w+)*@\\w+([-.]\w+)*.\\w+([-.]\\w+)*";//email地址
    source="cblock@126.com";
    if(regExp.match(pattern,source))
    {
        TRACE_YELLOW("%s match pattern[%s]\n",source.c_str(),pattern.c_str());
    }
    else
    {
        TRACE_RED("%s don't match pattern[%s]\n",source.c_str(),pattern.c_str());
    }
    
    pattern="hel*o";
    source="aaaaaheobbhelloccccchelllodddd";
    if(regExp.match(pattern,source,strArray,posArray,10))
    {
        TRACE_YELLOW("%s match pattern[%s],result:%s,pos:%s\n",source.c_str(),pattern.c_str(),strArray.serialize().c_str(),posArray.serialize().c_str());
    }
    else
    {
        TRACE_RED("%s don't match pattern[%s]\n",source.c_str(),pattern.c_str());
    }
}