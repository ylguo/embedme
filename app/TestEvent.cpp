#include "Event.h"
#include "Thread.h"
#include "Tracer.h"

class EventSender: public RunnableProtocol,public EventCenter{
public:
	void run()
	{
		while(1)
		{
			Event evt= Event(0x1234);
			postEvent(evt);
			TRACE_DEBUG_CLASS("Send Event 0x1234.\n");
			sleep(2);
		}
	}
};

class EventReciever: public EventListner{
public:
	void handleEvent(Event & evt)
	{
		switch(evt.getEventId())
		{
			case 0x1234:
				TRACE_DEBUG_CLASS("I CATCH THE RIGHT EVENT:0x1234.\n");
				break;
			default:
				TRACE_DEBUG_CLASS("I GET A UNKNOWN EVENT:0x%x.\n",evt.getEventId());
				break;
		}
	}
	
};

void TestEvent()
{
	EventReciever reciever;
	EventSender sender;
	Thread sendThread; 
	sender.addEventListener(&reciever);
	sendThread.start(&sender);
	
	while(1)
	{
		TRACE_DEBUG("Event Test is running----------\n");
		sleep(5);
	}
}
