#include "KeyInput.h"
#include "Tracer.h"
#ifdef OS_CYGWIN
void TestKeyInput()
{
    TRACE_ERR("Not support KeyInput!\n");
}
#else
class MyKeyListener:public KeyListener{
public:
    void handleKey(int keycode)
    {
        TRACE_PINK("Recieved Key Event:");
        string event;
        switch(keycode)
        {
            case KEY_UP: event = "KEY_UP";break;
            case KEY_DOWN: event = "KEY_DOWN";break;
            case KEY_LEFT: event = "KEY_LEFT";break;
            case KEY_RIGHT: event = "KEY_RIGHT";break;
            case KEY_F1: event = "KEY_OK";break;
            case KEY_F2: event = "KEY_CALL";break;
            case KEY_F3: event = "KEY_HANGUP";break;
            default: event = "KEY_UNKNOWN";break;
        }
        TRACE_PINK("%s\n",event.c_str());
    }
};


//#define INPUT_DEVICE   "/dev/input/event0"
#define INPUT_DEVICE   "/dev/event0"
//#define INPUT_DEVICE   "/dev/event1"

void TestKeyInput()
{
    MyKeyListener listener;
    KeyInput* keyInput=KeyInput::getInstance();
    keyInput->addKeyListener(&listener);
    if(!keyInput->open(INPUT_DEVICE))
    {
        TRACE_ERR("open device failed:%s\n",INPUT_DEVICE);
        return;
    }
    while(1)
    {
        TRACE_DEBUG("Test KeyInput Process is running, \"q\" or \"quit\" for exit.\n");
        std::string inputStr;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) 
        {
            keyInput->close();
            keyInput->delKeyListener(&listener);
            return;
        }
        inputStr.clear();
    }
}
#endif
