#include "MsgQueue.h"
#include "Thread.h"
#include "Tracer.h"

#define MQ_ID	0x1234

class MsgSender:public RunnableProtocol{
public:
	void run()
	{
		MsgQueue *mq = MsgQueueFactory::getInstance()->getMsgQueue(MQ_ID);
		while(1)
		{
			QueueMsg_S msg={2,5,"hello"};
			mq->sendMsg(msg);
			sleep(2);
		}
	}
};

class MsgReciever:public RunnableProtocol{
public:
	void run()
	{
		MsgQueue *mq = MsgQueueFactory::getInstance()->getMsgQueue(MQ_ID);
		while(1)
		{
			QueueMsg_S msg={0};
			if(STATUS_OK==mq->recvMsg(msg,2))
			{
			    TRACE_DEBUG_CLASS("Recieved Message type:%d,len=%d, content:%s.\n",msg.m_msgType,msg.m_dataLen,msg.m_data);
            }
		}
	}
};

void TestMsgQueue()
{
	Thread sendThread,recvThread;
	MsgSender sender;
	MsgReciever recver;
	sendThread.start(&sender);
	recvThread.start(&recver);
	while(1)
	{
		TRACE_DEBUG("MsgQueue Test is running----------\n");
		sleep(5);
	}
}
