/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __VERSION_H__
#define __VERSION_H__

#include "BaseType.h"

typedef struct 
{
    uint8 v1_main; /* 大版本号 */
    uint8 v2_sub;  /* 小版本号 */
    uint8 v3_test; /* 测试次数 */
    uint8 v4_sum;  /* 版本总数 */
}SWVERSION_S;

static SWVERSION_S SWVER = {2, 0, 2, 2}; 

#endif
