#include "BaseType.h"
#include "Tracer.h"
#include "KVProperty.h"

#include <stdlib.h>
#include <iostream>
using namespace std;

void TestKVProperty()
{
    #if 1
    TRACE_YELLOW("------KVProperty read& write Test--------\n");
	KVProperty property;
    if(property.initWithFile("app.property"))
    {
        cout<<"int:"<<property["int"].serialize()<<endl;
        cout<<"double:"<<property["double"].serialize()<<endl;
        cout<<"string:"<<property["string"].serialize()<<endl;
        cout<<"intArray:"<<property["intArray"].serialize()<<endl;
        cout<<"doubleArray:"<<property["doubleArray"].serialize()<<endl;
        cout<<"stringArray:"<<property["stringArray"].serialize()<<endl;
        cout<<"tuple:"<<property["tuple"].serialize()<<endl;
    }
    else
    {
        TRACE_ERR("KVProperty init error.\n");
    }
    property["int"] = 8;
    property["double"] = 8.88888;
    property["string"] = "8.88888";

    IntArray intArray;
    intArray.initWithString("[8,7,6,5,4,3,2,1]");
    property["intArray"] = intArray;

    DoubleArray doubleArray;
    doubleArray<<8.8888<<7.7777<<6.6666<<5.5555;
    property["doubleArray"] = doubleArray;

    StringArray stringArray;
    stringArray<<"copy"<<"copy"<<"copy"<<"copy";
    property["stringArray"] = stringArray;

    Tuple tuple;
    tuple<<8<<"copy"<<8.88888;
    property["tuple"] = tuple;

    TRACE_YELLOW("------KVProperty save Test--------\n");
    property.saveAsFile("app.property_back");
    system("cat app.property_back");


    TRACE_YELLOW("------KVProperty new Test--------\n");
    KVProperty property_new;
    KVValue value;
    value = 1;
    property_new.addProperty("int", value);

    value = "hello";
    property_new.addProperty("string", value);
    
    value.initWithString("[1,2,3,4]");
    property_new.addProperty("array", value);
    
    value.initWithString("(\"Jim\",2)");
    property_new.addProperty("tuple", value);

    property_new.saveAsFile("app.property_new");
    #endif
}