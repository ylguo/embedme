#include "Timer.h"
#include "Event.h"
#include "Tracer.h"

class TimerEventReciever: public EventListner{
public:
	void handleEvent(Event & evt)
	{
		TRACE_DEBUG_CLASS("handleEvent event_id=0x%x.\n",evt.getEventId());
	}
	
};

void TestTimer(void)
{
	/* 注册定时事件监听者 */
	TimerEventReciever ter;
    TimerManager::getInstance()->addEventListener(&ter);

	/* 创建定时器 */
	Timer* timer1= new OneShotTimer(5,TIME_UNIT_SECOND,0x1234);
	Timer* timer2= new PeriodicTimer(1,TIME_UNIT_SECOND,0xFFFF);
	TimerManager::getInstance()->addTimer(timer1);
	TimerManager::getInstance()->addTimer(timer2);
	while(1)
	{
		sleep(10);
		TRACE_DEBUG("Event Test is running----------\n");
        TRACE_DEBUG("reset one shot timer...\n");
        TimerManager::getInstance()->restartTimer(timer1);
	}
}

class CTimerTest:public CTimerProtocol{
public:
    CTimerTest()
    {
        /* 创建定时器 */
        timer01=new CTimer(0x01,this);
        timer02=new CTimer(0x02,this);
    }
    void onCTimer(sint32 timerId)
    {
        switch(timerId)
        {
            case 0x01:
                TRACE_DEBUG_CLASS("ctimer 01...\n");
                break;
            default:
                TRACE_DEBUG_CLASS("no action for timer(%d).\n",timerId);
                break;
        }
    }
	void onTimer02()
    {
		TRACE_DEBUG_CLASS("action on timer 02...\n");
    }
    void startTest()
    {
        /* 启用定时器 */
        timer01->start(1, TIME_UNIT_SECOND);
        timer02->start(5, TIME_UNIT_SECOND, ctimer_selector(CTimerTest::onTimer02));
    }
    void stopTest()
    {
        /* 停止定时器 */
        timer01->stop();
        TRACE_ERR_CLASS("timer01 stopped.\n");
        sleep(10);
        timer02->stop();
        TRACE_ERR_CLASS("timer02 stopped.\n");
    }
private:
    CTimer* timer01;
    CTimer* timer02;
};
void TestCTimer(void)
{
    CTimerTest* test = new CTimerTest();
    test->startTest();
    sleep(10);
    test->stopTest();
}

