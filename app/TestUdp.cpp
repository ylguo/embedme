#include "BaseType.h"
#include "Socket.h"
#include "Thread.h"
#include "Tracer.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#define UDP_PORT	(6062)		/* 定义服务器端口 */

class MyUdpServer:public UdpServer{
public:
	void serviceLoop()
    {
        char buf[64]={0};
        sint32 ret=recvData(buf, sizeof(buf)-1);
        if (ret>0)
        {
            TRACE_DEBUG_CLASS("server recv:[%s]\n",buf);
            char buf[]="hello udp client!";
            sendData(buf,sizeof(buf));
        }
	}
};

void TestUdp(void)
{
	/* 启动服务线程 */
	MyUdpServer server;
    server.initServer(UDP_PORT);
    server.startServer();

	/* 等待服务端启动完毕 */
	sleep(2);

	/* 客户端建立连接 */
	UdpSocket client;
	client.open();
	client.bind(UDP_PORT+1);
	while(1)
	{
		char sendBuf[]="hello udp server!";
        client.connectToHost(UDP_PORT, "127.0.0.1");
        TRACE_DEBUG("client send msg:%s\n",sendBuf);
		client.writeData(sendBuf, sizeof(sendBuf));
        sleep(2);
        char buf[64]={0};
        int ret=client.readData(buf, sizeof(buf), 1000);
        if (ret>0)
        {
            TRACE_YELLOW("client get respond:[%s]\n",buf);
        }
	}
    server.stopServer();
}

