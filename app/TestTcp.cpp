#include "BaseType.h"
#include "Socket.h"
#include "Thread.h"
#include "Tracer.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#define TCP_PORT	(8889)		/* 定义服务器端口 */

class MyTcpServer:public TcpServer{
public:
	void serviceLoop()
    {
        while(1)
        {
            char buf[64]={0};
            sint32 ret=recvData(buf, sizeof(buf)-1);
            if (ret>0)
            {
                TRACE_DEBUG_CLASS("server recv:[%s]\n",buf);
                char buf[]="hello tcp client!";
                sendData(buf,sizeof(buf));  
            }
            else if (ret<0)
            {
                TRACE_ERR_CLASS("server recv error.\n");
                break;
            }
            else/* 客户端已断开连接 */
            {
                break;
            }
        }
    }   
};

void TestTcp(void)
{
	/* 启动服务线程 */
    MyTcpServer server;
    server.initServer(TCP_PORT);
    server.startServer();
	sleep(2);

	/* 客户端建立连接 */
	TcpSocket client;
	client.open();
	client.bind(TCP_PORT+1);
	client.connectToHost(TCP_PORT, "127.0.0.1");
	while(1)
	{
		char sendBuf[]="hello tcp server!";
        TRACE_DEBUG("client send msg:%s\n",sendBuf);
		client.writeData(sendBuf, sizeof(sendBuf),-1);
		sleep(2);
        char buf[64]={0};
        int ret=client.readData(buf, sizeof(buf), 1000);
        if (ret>0)
        {
            TRACE_YELLOW("client get respond:[%s]\n",buf);
        }
        
	}
    server.stopServer();
}