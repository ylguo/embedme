#include "Tracer.h"
#include <iostream>
extern void TestAny(void);
extern void TestUtils(void);
extern void TestCmdExecuter();
extern void TestMsgQueue();
extern void TestEvent();
extern void TestThread();
extern void TestTimer(void);
extern void TestTcp(void);
extern void TestUdp(void);
extern void TestKVProperty();
extern void TestComPort();
extern void TestNetwork();
extern void TestDataBase();
extern void TestTask();
extern void TestHttpClient();
extern void TestJSON(void);
extern void TestCTimer(void);
extern void TestXml(void);
extern void TestStringFilter(void);
extern void TestAudio(void);
extern void TestDirectory(void);
extern void TestGpio(void);
extern void TestKeyInput(void);
extern void TestThreadPool(void);
extern void TestTuple(void);
extern void TestArray(void);
extern void TestConfig(void);
extern void TestLogger(void);
extern void TestTracer(void);
extern void TestRegExp(void);

static void print_menu()
{
    TRACE_YELLOW("==========run test program===========\n");
    TRACE_YELLOW("quit/q ---> quit test program.\n");
    TRACE_YELLOW("00 ---> Quick test.\n");
    TRACE_YELLOW("01 ---> MsgQueue test.\n");
    TRACE_YELLOW("02 ---> Event test.\n");
    TRACE_YELLOW("03 ---> Thread test.\n");
    TRACE_YELLOW("04 ---> Timer test.\n");
    TRACE_YELLOW("05 ---> TcpSocket test.\n");
    TRACE_YELLOW("06 ---> UcpSocket test.\n");
    TRACE_YELLOW("07 ---> Utils test.\n");
    TRACE_YELLOW("08 ---> CmdExecuter test.\n");
    TRACE_YELLOW("09 ---> KeyInput Test.\n");
    TRACE_YELLOW("10 ---> KVProperty test.\n");
    TRACE_YELLOW("11 ---> ComPort test.\n");
    TRACE_YELLOW("12 ---> Network test.\n");
    TRACE_YELLOW("13 ---> DataBase test.\n");
    TRACE_YELLOW("14 ---> Task test.\n");
    TRACE_YELLOW("15 ---> HttpClient test.\n");
    TRACE_YELLOW("16 ---> JSONData test.\n");
    TRACE_YELLOW("17 ---> CTimer test.\n");
    TRACE_YELLOW("18 ---> TinyXml test.\n");
    TRACE_YELLOW("19 ---> StringFilter test.\n");
    TRACE_YELLOW("20 ---> AudioRecorder test.\n");
    TRACE_YELLOW("21 ---> Directory test.\n");
	TRACE_YELLOW("22 ---> GPIO test.\n");
    TRACE_YELLOW("23 ---> ThreadPool Test.\n");
    TRACE_YELLOW("24 ---> Tuple Test.\n");
    TRACE_YELLOW("25 ---> Array Test.\n");
    TRACE_YELLOW("26 ---> Config Test.\n");
    TRACE_YELLOW("27 ---> Logger Test.\n");
    TRACE_YELLOW("28 ---> Tracer Test.\n");
    TRACE_YELLOW("29 ---> RegExp Test.\n");
    TRACE_YELLOW("=====================================\n");
}

void testMain()
{
    std::string inputStr;
    while (1)
    {
        print_menu();
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) return;
        else if ("00" == inputStr)	TestAny();
        else if ("01" == inputStr)	TestMsgQueue();
        else if ("02" == inputStr)	TestEvent();
        else if ("03" == inputStr)	TestThread();
        else if ("04" == inputStr)	TestTimer();
        else if ("05" == inputStr)	TestTcp();
        else if ("06" == inputStr)	TestUdp();
        else if ("07" == inputStr)	TestUtils();
        else if ("08" == inputStr)	TestCmdExecuter();
        else if ("09" == inputStr)	TestKeyInput();
        else if ("10" == inputStr)	TestKVProperty();
        else if ("11" == inputStr)	TestComPort();
        else if ("12" == inputStr)  TestNetwork();
        else if ("13" == inputStr)  TestDataBase();
        else if ("14" == inputStr)  TestTask();
        else if ("15" == inputStr)  TestHttpClient();
        else if ("16" == inputStr)  TestJSON();
        else if ("17" == inputStr)  TestCTimer();
        else if ("18" == inputStr)  TestXml();
        else if ("19" == inputStr)  TestStringFilter();
        else if ("20" == inputStr)  TestAudio();
        else if ("21" == inputStr)  TestDirectory();
		else if ("22" == inputStr)  TestGpio();
        else if ("23" == inputStr)  TestThreadPool();
        else if ("24" == inputStr)  TestTuple();
        else if ("25" == inputStr)  TestArray();
        else if ("26" == inputStr)  TestConfig();
        else if ("27" == inputStr)  TestLogger();
        else if ("28" == inputStr)  TestTracer();
        else if ("29" == inputStr)  TestRegExp();
        else
        {
            ;
        }
        inputStr.clear();
    }
}