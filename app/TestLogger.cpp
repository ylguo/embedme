#include "BaseType.h"
#include "Tracer.h"
#include "Logger.h"
#include "Config.h"
void TestLogger()
{
    TRACE_YELLOW("------Logger test------\n");
	Config config;
	config.initWithFile("logger.cfg");
	Settings logSettings = config["logger"];
	LoggerManager::getInstance()->initWithSettings(logSettings);
    int coreLog = LoggerManager::getInstance()->getLogger(LOG_TYPE_CORE);
    if (coreLog!=LOG_ID_INVALID)
    {
        TRACE_YELLOW("core log id:%d\n",coreLog);
        LOG(coreLog,"this is core log.");
    }
    else
    {
        TRACE_RED("get core log failed.\n");
    }
    int mainLog = LoggerManager::getInstance()->getLogger(LOG_TYPE_LOG4Z);
    if (mainLog!=LOG_ID_INVALID)
    {
        TRACE_YELLOW("main log id:%d\n",mainLog);
        LOG(mainLog,"this is main log.");
    }
    else
    {
        TRACE_RED("get main log failed.\n");
    }
    int smsLog = LoggerManager::getInstance()->getLogger(LOG_TYPE_LOG4Z,"sms");
    if (smsLog!=LOG_ID_INVALID)
    {
        TRACE_YELLOW("sms log id:%d\n",smsLog);
        LOG(smsLog,"this is sms log.");
    }
    else
    {
        TRACE_RED("get sms log failed.\n");
    }
    int callLog = LoggerManager::getInstance()->getLogger(LOG_TYPE_LOG4Z,"call");
    if (callLog!=LOG_ID_INVALID)
    {
        TRACE_YELLOW("call log id:%d\n",callLog);
        LOG(callLog,"this is call log.");
    }
    else
    {
        TRACE_RED("get call log failed.\n");
    }
    
}
