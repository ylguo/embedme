#include "BaseType.h"
#include "Network.h"
#include "Tracer.h"
#ifdef OS_CYGWIN
void TestNetwork()
{
    TRACE_ERR("Not support Network!\n");
}
#else
static void print_menu()
{
	TRACE_YELLOW("==========run test program===========\n");
	TRACE_YELLOW("quit/q ---> quit test program.\n");
	TRACE_YELLOW("00 ---> set interface name.\n");
	TRACE_YELLOW("01 ---> get ip address.\n");
	TRACE_YELLOW("02 ---> set ip address.\n");
	TRACE_YELLOW("03 ---> get netmask.\n");
	TRACE_YELLOW("04 ---> set netmask.\n");
	TRACE_YELLOW("05 ---> get mac address.\n");
	TRACE_YELLOW("06 ---> set mac address.\n");
	TRACE_YELLOW("07 ---> get interface up/down status.\n");
	TRACE_YELLOW("08 ---> set interface up/down.\n");
	TRACE_YELLOW("=====================================\n");
}

void TestNetwork()
{
	std::string intf="eth0"; 
	std::string inputStr;
	while(1)
	{
		print_menu();
		std::cin >> inputStr;
        std::cout << inputStr<<std::endl;
		if("q"==inputStr || "quit"==inputStr) return;
		else if("00"==inputStr)
		{
			std::cout << "input ether interface name:";
			std::cin >> intf;
			std::cout << "set name: "<<intf<<" ok."<<std::endl;
		}
		else if("01"==inputStr)
		{
			std::string ipaddr;
			ipaddr = Network::getInetAddr(intf);
			TRACE_DEBUG("ipaddr : %s\n",ipaddr.c_str());
		}
		else if("02"==inputStr)
		{
			std::string ipaddr;
			std::cout << "input ip address(xxx.xxx.xxx.xxx):";
			std::cin >> ipaddr;
			if(STATUS_ERROR==Network::setInetAddr(intf, ipaddr))
			{
				TRACE_ERR("set ip error!\n");
			}
		}
		else if("03"==inputStr)
		{
			std::string netmask;
			netmask = Network::getMaskAddr(intf);
			TRACE_DEBUG("netmask : %s\n",netmask.c_str());
		}
		else if("04"==inputStr)
		{
			std::string netmask;
			std::cout << "input netmask address(xxx.xxx.xxx.xxx):";
			std::cin >> netmask;
			if(STATUS_ERROR==Network::setMaskAddr(intf, netmask))
			{
				TRACE_ERR("set netmask error!\n");
			}
		}
		else if("05"==inputStr)
		{
			std::string mac;
			mac = Network::getMacAddr(intf);
			TRACE_DEBUG("macaddr : %s\n",mac.c_str());
		}
		else if("06"==inputStr)
		{
			std::string mac;
			std::cout << "input mac address(xx:xx:xx:xx:xx:xx):";
			std::cin >> mac;
			if(STATUS_ERROR==Network::setMacAddr(intf, mac))
			{
				TRACE_ERR("set mac error!\n");
			}
			
		}
		else if("07"==inputStr)
		{
			sint32 downUp  = Network::getIfUpDown(intf);
			TRACE_DEBUG("%s is %s.\n",intf.c_str(),downUp==INTF::DOWN?"Down":"Up");
		}
		else if("08"==inputStr)
		{
			std::string updown;
			std::cout << "input  1--up, 0--down:";
			std::cin >> updown;
			if (updown=="1")
			{
				Network::setIfUpDown(intf, INTF::UP);
			}
			else
			{
				Network::setIfUpDown(intf, INTF::DOWN);
			}
		}
		else {;}
		inputStr.clear();
	}
}
#endif

