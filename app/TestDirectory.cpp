#include "Tracer.h"
#include "Directory.h"

void TestDirectory()
{
    Directory dir;
    dir.enter(".");
    TRACE_YELLOW("111---current path: %s \n",dir.current().c_str());
    vector<string> fileList=dir.listAll();
    for(int i=0;i<fileList.size();i++)
    {
        TRACE_YELLOW("file[%d]:  %s  \n",i,fileList[i].c_str());
    }

    dir.enter("/run/");
    TRACE_YELLOW("222---current path: %s \n",dir.current().c_str());
    fileList=dir.listAll();
    for(int i=0;i<fileList.size();i++)
    {
        TRACE_YELLOW("file[%d]:  %s  \n",i,fileList[i].c_str());
    }

    dir.enter("..");
    TRACE_YELLOW("333---current path: %s \n",dir.current().c_str());
    fileList=dir.listAll();
    for(int i=0;i<fileList.size();i++)
    {
        TRACE_YELLOW("file[%d]:  %s  \n",i,fileList[i].c_str());
    }
}