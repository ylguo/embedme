#include "Tracer.h"
#include "ThreadPool.h"

#include <stdlib.h>
#include <iostream>

class MyRunnable:public Runnable{
public:
    void run()
    {
        while(isRunning())
        {
            TRACE_YELLOW("Runnable---id=%d\n",m_id);
            Thread::msleep(1000);
        }
    }
    void setId(int id)
    {
        m_id = id;
    }
private:
    int m_id;
};


void TestThreadPool(void)
{
    ThreadPool* pool=ThreadPool::getInstance();
    if(!pool->init(5))
    {
        TRACE_RED("ThreadPool init error.\n");
        return;
    }
    MyRunnable r01,r02,r03,r04,r05,r06;
    r01.setId(1);
    r02.setId(2);
    r03.setId(3);
    r04.setId(4);
    r05.setId(5);
    r06.setId(6);
    if(!pool->start(&r01))
    {
        TRACE_RED("start r01 error.\n");
    }
    if(!pool->start(&r02))
    {
        TRACE_RED("start r02 error.\n");
    }
    if(!pool->start(&r03))
    {
        TRACE_RED("start r03 error.\n");
    }
    if(!pool->start(&r04))
    {
        TRACE_RED("start r04 error.\n");
    }
    if(!pool->start(&r05))
    {
        TRACE_RED("start r05 error.\n");
    }
    if(!pool->start(&r06))
    {
        TRACE_RED("start r06 error.\n");
    }
    while(1)
    {
        TRACE_DEBUG("ThreadPool Test --- q:quit,s:start,c:cancel.\n");
        std::string inputStr;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) 
        {
            return;
        }
        else if("c"==inputStr)
        {
            inputStr.clear();
            TRACE_CYAN("input thread id:");
            std::cin >> inputStr;
            std::cout << inputStr << std::endl;
            int id = atoi(inputStr.c_str());
            bool ret=false;
            switch(id)
            {
                case 1: ret=pool->cancel(&r01);break;
                case 2: ret=pool->cancel(&r02);break;
                case 3: ret=pool->cancel(&r03);break;
                case 4: ret=pool->cancel(&r04);break;
                case 5: ret=pool->cancel(&r05);break;
                case 6: ret=pool->cancel(&r06);break;
                default: break;
            }
            if (!ret)
            {
                TRACE_RED("cancel thread %d failed.\n",id);
            }
            else
            {
                TRACE_CYAN("cancel thread %d OK.\n",id);
            }
        }
        else if("s"==inputStr)
        {
            inputStr.clear();
            TRACE_CYAN("input thread id:");
            std::cin >> inputStr;
            std::cout << inputStr << std::endl;
            int id = atoi(inputStr.c_str());
            bool ret=false;
            switch(id)
            {
                case 1: ret=pool->start(&r01);break;
                case 2: ret=pool->start(&r02);break;
                case 3: ret=pool->start(&r03);break;
                case 4: ret=pool->start(&r04);break;
                case 5: ret=pool->start(&r05);break;
                case 6: ret=pool->start(&r06);break;
                default: break;
            }
            if (!ret)
            {
                TRACE_RED("start thread %d failed.\n",id);
            }
            else
            {
                TRACE_CYAN("start thread %d OK.\n",id);
            }
        }
        else if("t"==inputStr)
        {
            TRACE_RED("ThreadPool max:%d,idle:%d\n",pool->maxThreadCount(),pool->idleThreadCount());
        }
        inputStr.clear();
    }
    
}