#include "Tuple.h"
#include "Tracer.h"
#include <iostream>
using namespace std;

void TestTuple(void)
{
    while(1)
    {
        TRACE_DEBUG("Tuple Test --- q:quit.t:test\n");
        std::string inputStr;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) 
        {
            return;
        }
        else
        {
            Tuple tuple;
            #if 0
            TupleItem item01("China");
            TupleItem item02(100);
            TupleItem item03(3.1415926);
            tuple<<item01<<item02<<item03;
            #else
            tuple<<"China"<<100<<3.1415926;
            #endif
            TRACE_YELLOW("print tuple:%s,%d,%f\n",tuple[0].toString().c_str(),tuple[1].toInt(),tuple[2].toDouble());
            TRACE_YELLOW("print tuple:\n");
            cout<<tuple;
            TRACE_YELLOW("print copy tuple:\n");
            Tuple tp(tuple);
            cout<<tp;
            TRACE_YELLOW("print equal tuple:\n");
            tp = tuple;
            cout<<tp;
        }
    }
}