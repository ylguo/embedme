#include "BaseType.h"
#include "Thread.h"
#include "JSONData.h"
#include "HttpClient.h"
#include "Tracer.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

static void testJSONWrite()
{
    /* 初始化一个对象型的JSONData*/
    JSONData jsonData;
    JSONNode* rootNode=jsonData.initWithObjectFormat();

    /* 增加普通结点 */
    rootNode->addSubNode("empty");
    rootNode->addSubNode(1,"yes");
    rootNode->addSubNode(0,"no");
    rootNode->addSubNode(100,"int");
    rootNode->addSubNode(100.123,"double");
    rootNode->addSubNode("hello world.","string");

    /* 增加数组结点 */
    IntArray intArray;
    intArray<<1<<2<<3<<4<<5;
    rootNode->addSubNode(intArray, "intArray");
    DoubleArray doubleArray;
    doubleArray<<1.5<<2.4<<3.3<<4.2<<5.1;
    rootNode->addSubNode(doubleArray, "doubleArray");
    StringArray stringArray;
    stringArray<<"one"<<"two"<<"three"<<"four"<<"five";
    rootNode->addSubNode(stringArray, "stringArray");
    rootNode->addSubNode(stringArray);
    /* 保存到文件中 */
    jsonData.saveAsFile("object.json");
    

    /* 初始化一个数组型的JSONData*/
    JSONData arrayData;
    JSONNode* arrayNode=arrayData.initWithArrayFormat();
    for(int i=0; i<3; i++)
    {
        JSONData data;
        JSONNode* node = data.initWithObjectFormat();
        char buf[32]={0};
        sprintf(buf,"string%02d",i+1);
        node->addSubNode(buf, std::string(buf));
        arrayNode->addSubNode(node);
    }
    /* 保存到文件中 */
    arrayData.saveAsFile("array.json");
}

static void testJSONRead()
{
    JSONData json;
    //JSONNode* rootNode=json.initWithContentOfFile("test.json");
    const char* jsonString = "{\"version\":\"V1.0.0.1\",\"object\":{\"oid\":1234}}";
    JSONNode* rootNode = json.initWithDataString(jsonString);
    JSONNode node = (*rootNode)["version"];
    TRACE_YELLOW("version:%s\n",node.toString().c_str());
    node = (*rootNode)["object"]["oid"];
    TRACE_YELLOW("oid:%d\n",node.toInt());
    node = (*rootNode)["object"].toNode();
    TRACE_YELLOW("---oid:%d\n",node["oid"].toInt());
    
}

static void baiduDictTest(std::string word)
{
#ifdef OS_CYGWIN
	TRACE_ERR("Not support Dict Test.\n");
#else
    HttpClient client;
    std::string url="http://openapi.baidu.com/public/2.0/translate/dict/simple?client_id=";
    url += "faEkfGvdVpwjeQGms6HqzHaQ";
    url += "&q=";
    url += word;
    url +="&from=en&to=zh";
    File* file = new File("/tmp/baidudict.cache",IO_MODE_REWR_ORNEW);
    client.httpGet(url,file);
    delete file;

    JSONData json;
    JSONNode* rootNode=json.initWithContentOfFile("/tmp/baidudict.cache");
    
    #if 0   //显示json文本
    std::string out=json.serialize();
    TRACE_CYAN("%s\n",out.c_str());
    #endif
    
    /* 解析JSON */
    JSONNode node = (*rootNode)["errno"];
    if (node.toInt()==0)
    {
        node = (*rootNode)["data"]["word_name"];
        if(node.toString().empty())
        {
            TRACE_ERR("can't find word \"%s\"!!!\n",word.c_str());
            return;
        }
        TRACE_CYAN("%s\n",node.toString().c_str());
        node=(*rootNode)["data"]["symbols"];
        JSONNode ele_node= node[0]["ph_am"];
        TRACE_CYAN("AM:[%s]\n",ele_node.toString().c_str());
        ele_node = node[0]["ph_en"];
        TRACE_CYAN("EN:[%s]\n",ele_node.toString().c_str());
        
        node = node[0]["parts"][0];
        ele_node= node["part"];
        TRACE_CYAN("%s\n",ele_node.toString().c_str());
        ele_node= node["means"];
        int arraySize = ele_node.getArraySize();
        for(int i=0;i<arraySize; i++)
        {
            TRACE_CYAN("%d.%s\n",i+1,ele_node[i].toString().c_str());
        }
    }
#endif
}

void TestJSON(void)
{
    std::string inputStr;
    while (1)
    {
        TRACE_YELLOW("----JSONData Test----\n");
        TRACE_YELLOW("1: JSON write.\n");
        TRACE_YELLOW("2: JSON read.\n");
        TRACE_YELLOW("3: Baidu Dict test.\n");
        TRACE_YELLOW("q: quit.\n");
        TRACE_YELLOW("please input comand:\n");
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) return;
        else if ("1" == inputStr)	testJSONWrite();
        else if ("2" == inputStr)	testJSONRead();
        else if ("3" == inputStr)
        {
            std::string word;
            TRACE_YELLOW("please input a English word:");
            std::cin >> word;
            baiduDictTest(word);
            word.clear();
        }
        else
        {
            ;
        }
        inputStr.clear();
    }
}

