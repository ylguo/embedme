#include "Array.h"
#include "Tracer.h"
#include <iostream>
using namespace std;

void TestArray(void)
{
     while(1)
    {
        TRACE_DEBUG("Array Test --- q:quit.t:test\n");
        std::string inputStr;
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) 
        {
            return;
        }
        else
        {
            IntArray intArray;
            intArray.initWithString("[1,2,3,4,5]");
            intArray<<6<<7<<8<<9<<10;
            cout<<intArray.serialize()<<endl;

            StringArray stringArray;
            stringArray.initWithString("[\"One\",\"Two\",\"Three\",\"Four\",\"Five\"]");
            stringArray<<"Six"<<"Seven"<<"Eight"<<"Nine"<<"Ten";
            cout<<stringArray.serialize()<<endl;
            
        }
    }
}