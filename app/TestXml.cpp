#include "BaseType.h"
#include "Tracer.h"
#include "XmlData.h"
#include <stdio.h>
#include <iostream>
using namespace std;

const char* todoXml =
		"<?xml version=\"1.0\"  standalone='no' >\n"
		"<!-- Our to do list data -->"
		"<ToDo>\n"
		"<!-- Do I need a secure PDA? -->\n"
		"<Item priority=\"0\" distance='close'> Go to the <bold>Toy store!</bold></Item>"
		"<Item priority=\"1\" distance='none'> Do bills   </Item>"
		"<Item priority=\"2\" distance='far &amp; back'> Look for Evil Dinosaurs! </Item>"
		"haha</ToDo>";

int TestXml()
{
    XmlData xmlData;
    if (false==xmlData.initWithDataString(std::string(todoXml)))
    {
        TRACE_ERR("init xml data failed.\n");
        return STATUS_ERROR;
    }
    XmlNode node = xmlData["ToDo"];
    if (node.isNullNode())
    {
        TRACE_ERR("No node ToDo.\n");
        return STATUS_ERROR;
    }
    TRACE_CYAN("node child number=%d\n",node.childNum());
    TRACE_CYAN("ToDo:%s==>Item:%s\n",node.toString().c_str(),node["Item"].toString().c_str());
    TRACE_CYAN("ToDo->Item[1]:%s\n",node["Item"][1].toString().c_str());

    int priority;
    string distance;
    if (!node["Item"][2].getAttribute("priority",priority))
    {
        TRACE_ERR("Node[Item][2] priority error.\n");
    }
    if (!node["Item"][2].getAttribute("distance",distance))
    {
        TRACE_ERR("Node[Item][2] distance error.\n");
    }
    
    TRACE_CYAN("Node[Item][2] priority:%d,distance:%s\n",priority,distance.c_str());
    if(!node.addSubNode("Item", "Get work"))
    {
        TRACE_ERR("add Node[Item][3] failed.\n");
    }
    #if 1
    if (!node["Item"][3].setAttribute("priority",3))
    {
        TRACE_ERR("Node[Item][3] set priority error.\n");
    }

    if (!node["Item"][3].setAttribute("distance","none"))
    {
        TRACE_ERR("Node[Item][3] set distance error.\n");
    }
    #endif
    TRACE_YELLOW("XmlData Test OK.\n");
    string xmlString=xmlData.serialize();
    TRACE_CYAN("--------------------------\n");
    TRACE_CYAN("%s",xmlString.c_str());
    TRACE_CYAN("--------------------------\n");
    TRACE_YELLOW("---------create new xml--------\n");
    XmlData myXml;
    myXml.addSubNode("BODY");
    myXml["BODY"].addSubNode("HTML","0000000000");
    myXml["BODY"]["HTML"].addSubNode("p1","11111111");
    myXml["BODY"]["HTML"].addSubNode("p2","22222222");
    myXml["BODY"].addSubNode("XML");
    myXml["BODY"]["XML"].addSubNode("p3","33333333");
    myXml["BODY"]["XML"].addSubNode("p4","44444444");
    xmlString=myXml.serialize();
    TRACE_CYAN("%s",xmlString.c_str());
    TRACE_CYAN("--------------------------\n");
    return 0;
}

