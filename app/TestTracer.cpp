#include "Tracer.h"
#include "Thread.h"

class TracePrinter:public RunnableProtocol{
public:
    void run()
    {
        int condition=0;
        TRACE_IF(condition==0);
        TRACE_IF_CLASS(condition==0);
        while(1)
        {
            TRACE_YELLOW("--------------------------\n");
            TRACE_DEBUG_CLASS("TRACE_DEBUG_CLASS.\n");
            TRACE_INFO_CLASS("TRACE_INFO_CLASS.\n");
            TRACE_WARN_CLASS("TRACE_WARN_CLASS.\n");
            TRACE_ERR_CLASS("TRACE_ERR_CLASS.\n");
            TRACE_REL_CLASS("TRACE_REL_CLASS.\n");
            Thread::msleep(1000);
        }

    }
};

void TestTracer(void)
{
    std::string inputStr;
    TracePrinter printer;
    Thread traceThread;
    traceThread.start(&printer);
    while (1)
    {
        TRACE_YELLOW("l/level  -- print current level.\n");
        TRACE_YELLOW("m/more  -- print more debug information.\n");
    	TRACE_YELLOW("e/less  -- print less debug information.\n");
    	TRACE_YELLOW("o/on    -- enable debug information.\n");
    	TRACE_YELLOW("f/off   -- disable debug information.\n");
        TRACE_YELLOW("b/begin -- begin tracer timer.\n");
        TRACE_YELLOW("d/end   -- end tracer timer.\n");
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr)  {traceThread.cancel(); return;}
        else if("l"==inputStr || "level"==inputStr)	printf("Trace Level:%d.\n",Tracer::getInstance()->getLevel());
        else if("m"==inputStr || "more"==inputStr)	Tracer::getInstance()->printMore();
        else if("e"==inputStr || "less"==inputStr)	Tracer::getInstance()->printLess();
		else if("o"==inputStr || "on"==inputStr)	Tracer::getInstance()->setEnable(true);
		else if("f"==inputStr || "off"==inputStr)	Tracer::getInstance()->setEnable(false);
        else if("b"==inputStr || "begin"==inputStr)	Tracer::getInstance()->traceTimerBegin();
        else if("d"==inputStr || "end"==inputStr)
        {   
            Tracer::getInstance()->traceTimerEnd();
            Tracer::getInstance()->tracetimerDuration();
        }
    }
}