#include "Tracer.h"
#include "Utils.h"
#include <iostream>

void TestUtils(void)
{
	std::string inputStr;
	while(1)
	{
		TRACE_YELLOW("q ---> quit.\n");
		TRACE_YELLOW("1 ---> BigEndian.\n");
		TRACE_YELLOW("2 ---> findString.\n");
		TRACE_YELLOW("3 ---> string2code.\n");
		TRACE_YELLOW("4 ---> code2string.\n");
        TRACE_YELLOW("5 ---> utf8 & unicode.\n");
        TRACE_YELLOW("6 ---> cutString.\n");
        TRACE_YELLOW("7 ---> trimBeginWith.\n");
		std::cin >> inputStr;
        std::cout << inputStr<<std::endl;
		if("q"==inputStr) 
		{
			return;
		}
		else if("1"==inputStr)
		{
			uint16 tmp16,x16=0xABCD;
			uint32 tmp32,x32=0x12345678;

			if(Utils::isBigEndian())
			{
				TRACE_DEBUG("Host is BigEndian.\n");
			}
			else
			{
				TRACE_DEBUG("Host is LittleEndian.\n");
			}

			tmp16=Utils::host2LitEndian(x16);
			TRACE_DEBUG("0xABCD change to LittleEndian(0x%04X).\n",tmp16);
			tmp32=Utils::host2LitEndian(x32);
			TRACE_DEBUG("0x12345678 change to LittleEndian(0x%08x).\n",tmp32);
			
			tmp16=Utils::host2BigEndian(x16);
			TRACE_DEBUG("0xABCD change to BigEndian(0x%04X).\n",tmp16);
			tmp32=Utils::host2BigEndian(x32);
			TRACE_DEBUG("0x12345678 change to BigEndian(0x%08x).\n",tmp32);
		}
		else if("2"==inputStr)
		{
			std::string result;
			std::string source="3A = \"AAA\"     \n  3B = BBB;";
			result=Utils::findString(source, "3A", "\"", "\"");
			std::cout<<"test 3A result="<<result<<std::endl;

			result=Utils::findString(source, "3B", "= ", ";");
			std::cout<<"test 3B result="<<result<<std::endl;

			//source="\" 123456  \"\"";
			source="  123456   ";
			source = Utils::trimEndingBlank(source);
			cout<<"trim result="<<source<<endl;
		}
		else if("3"==inputStr)
		{
			std::string str="1234abcd";
			char codebuf[4]={0};
			Utils::string2code(str, codebuf, sizeof(codebuf));
			TRACE_DEBUG("\"%s\"==>:\n",str.c_str());
			TRACE_HEX("str2code",(char*)codebuf, sizeof(codebuf));
		}
		else if("4"==inputStr)
		{
			char codebuf[4]={0x56,0x78,0xab,0xcd};
			std::string str=Utils::code2string(codebuf, sizeof(codebuf));
			TRACE_HEX("code2str",(char*)codebuf, sizeof(codebuf));
			TRACE_DEBUG("codestr==>\"%s\"\n",str.c_str());
		}
        else if("5"==inputStr)
		{
			std::string yan=Utils::unicodeOneToUtf8String(0x4E25);
            TRACE_DEBUG("[0x4E25](unicode)===>[%s](utf8)\n",yan.c_str());
            uint32 unicode=Utils::utf8OneToUnicode(yan.c_str());
            TRACE_DEBUG("[%s](utf8)===>[0x%X](unicode)\n",yan.c_str(),unicode);
		}
        else if("6"==inputStr)
		{
			std::string source=":1,aaaaa,2,bbbbb,3,cccccc\r";
            std::vector<std::string> strVector=Utils::cutString(source, ":", "\r", ",");
            for(int i=0;i<strVector.size();i++)
            {
                TRACE_DEBUG("index:%d---(%s)\n",i,strVector[i].c_str());
            }
		}  
        else if ("7"==inputStr)
        {
            std::string source("  00001234");
            std::string result=Utils::trimBeginWith(source, "0 ");
            TRACE_RED("result=%s\n",result.c_str());
        }
		inputStr.clear();
	}
}