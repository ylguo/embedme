#include "BaseType.h"
#include "Tracer.h"
#include "ComPort.h"
#include "Thread.h"

#define COM_PORT	"/dev/ttySAC4"


ComPort* pCom=NULL;

class ComReciever:public RunnableProtocol{
public:
	void run()
	{
		sint32 ret;
		while(1)
		{
		    if (pCom!=NULL)
			{
			    char buf[64]={0};
        		ret=pCom->readData(buf, 63);
        		if(ret>0)
        		{
        			TRACE_DEBUG("COM recv:[%s]\n",buf);
        		}
        		else
        		{
        			TRACE_ERR("Com recv error,ret=%d\n",ret);
                    usleep(1000);
        		}
            }
			sleep(3);
		}

	}
};


void TestComPort()
{
	sint32 ret;
	Thread recvThread;
	ComReciever comRecv;
    
    pCom = new ComPort();
	pCom->setAttribute(COM_ATTR_BAUDRATE, 19200);
	ret=pCom->open(COM_PORT, IO_MODE_RDWR_ONLY);
	if(STATUS_ERROR==ret)
	{
		TRACE_ERR("open %s error:%s\n", COM_PORT, ERROR_STRING);
		return;
	}
        
	recvThread.start(&comRecv);
	while(1)
	{
        const char* hello="ATD*99#\r\n";
        TRACE_YELLOW("Send:[AT],len=%d\n",strlen(hello));
	    ret=pCom->writeData(hello, strlen(hello));
	    if(ret<=0)
	    {
		    TRACE_ERR("Com send error,ret=%d\n",ret);
	    }
        sleep(3);
	}
}