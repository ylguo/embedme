/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "Version.h"
#include "Tracer.h"

#include <iostream>

extern void testMain();

static void print_version(void)
{
	TRACE_YELLOW("App V%d.%d.%d.%d Build on %s %s.\n",SWVER.v1_main,SWVER.v2_sub,
				 SWVER.v3_test,SWVER.v4_sum,__DATE__,__TIME__);
}

static void print_help(void)
{
	TRACE_YELLOW("verion -- print version information.\n");
	TRACE_YELLOW("help/h -- get help informatiom.\n");
	TRACE_YELLOW("quit/q -- exit.\n");
	TRACE_YELLOW("test   -- run test program.\n");
}

/* ���������� */
int main()
{
    //AppDelegate app;
    //return Application::getInstance()->run();
    
	TRACE_YELLOW("=============================================\n");
	print_version();
	TRACE_YELLOW("=============================================\n");
	std::string inputStr;
    Tracer::getInstance()->setLevel(TRACE_LEVEL_DBG);
	while(1)
    {	
    	print_help();
        std::cin >> inputStr;
        std::cout << inputStr<<std::endl;
		if("help"==inputStr || "h" == inputStr) ;
		else if("quit"==inputStr || "q"==inputStr)return 0;
		else if("version"==inputStr)print_version();
		else if("test"==inputStr)	testMain();
        else
        {
            if(0 != inputStr.length())
            {
            	TRACE_RED("Unknown command: \"%s\"\n",inputStr.c_str());
				TRACE_RED("Try \"help\" to get more information.\n");
            }
        }
        inputStr.clear();
	}
	return 0;
}
