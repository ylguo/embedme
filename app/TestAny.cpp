#include "BaseType.h"
#include "Tracer.h"
#include "Utils.h"
#include "CRCCheck.h"

void testCRC16()
{
    char packet[]={0x02,0x03,0x00,0x05,0x00,0x02};
    CRCCheck crcCheck;
    unsigned short result;
    TRACE_YELLOW("\n-----------8005--------------\n");
    crcCheck.createCRC16Table(0x8005);
    TRACE_YELLOW("\n-----------3d65--------------\n");
    crcCheck.createCRC16Table(0x3d65);

    TRACE_RED("0x0102,16==>0x%04X\n",Utils::bitsReverse(0x0102, 16));
    TRACE_RED("0x0102,8==>0x%04X\n",Utils::bitsReverse(0x0102, 8));
    #if 1
    {
        char packet[]={0xF1,0xA5,0x81,0x00,0x00,0x01,0x02,0x01,0x00,0x00,0xEC,0x00,0x01,0x01,0x01,0x01};//6D DB
        result = 0;
        if (crcCheck.doCRC16Check(packet, sizeof(packet),CRC16_3D65, result))
        {
            TRACE_HEX("packet",packet,sizeof(packet));
            TRACE_YELLOW("crc16_3d65 result:0x%04X\n",result);
        }
        else
        {
            TRACE_RED("do crc16_3d65 check error!\n");
        }
        crcCheck.doCRC16Check(packet, sizeof(packet), 0x3D65, result);
        TRACE_YELLOW("-------------3D65 CRC check:%x--------\n",result);
    }
    #endif
    
    if (crcCheck.doCRC16Check(packet, sizeof(packet), CRC16_8005,result))
    {
        TRACE_HEX("packet",packet,sizeof(packet));
        TRACE_YELLOW("crc16_8005 result:0x%04X\n",result);
    }
    else
    {
        TRACE_RED("do crc16_8005 check error!\n");
    }
    crcCheck.doCRC16Check(packet, sizeof(packet), 0x8005, result);
    TRACE_YELLOW("-------------8005 CRC check:%x--------\n",result);
        
}

void TestAny()
{    
    testCRC16();
}