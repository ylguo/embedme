#include "BaseType.h"
#include "HttpClient.h"
#include "Thread.h"
#include "Tracer.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#ifdef OS_CYGWIN
void TestHttpClient(void)
{
	TRACE_ERR("Not support HttpClient.\n");
}
#else
static void testHttpGet()
{
    HttpClient* client = new HttpClient();
    File* file = new  File("baidu.html", IO_MODE_REWR_ORNEW);
    client->httpGet("www.baidu.com",file);
    TRACE_YELLOW("HTTP GET OK!\n");
    delete(file);
    delete(client);
}

static void testHttpPost()
{
    HttpClient* client = new HttpClient();
    File* file = new File("post.html",IO_MODE_REWR_ORNEW);
    client->httpPost("http://192.168.29.233:8080/cgi-bin/loginpage.cgi",\
                     "user=admin&passwd=123456&login=login", file);
    TRACE_YELLOW("HTTP POST OK!\n");
    delete(file);
    delete(client);
}

static void testFileDownLoad()
{
    HttpClient* client = new HttpClient();
    File* file = new File("download.file",IO_MODE_REWR_ORNEW);
    client->httpFileDownLoad("http://", file);
    TRACE_YELLOW("HTTP File Download OK!\n");
    delete(file);
    delete(client);
}

void TestHttpClient(void)
{
    std::string inputStr;
    while (1)
    {
        TRACE_YELLOW("----HttpClient Test----\n");
        TRACE_YELLOW("01 test http get.\n");
        TRACE_YELLOW("02 test http post.\n");
        TRACE_YELLOW("please input:\n");
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr) return;
        else if ("01" == inputStr)	testHttpGet();
        else if ("02" == inputStr)	testHttpPost();
        else if ("03" == inputStr)	testFileDownLoad();
        else
        {
            ;
        }
        inputStr.clear();
    }
}
#endif