#include <stdlib.h>

#include "BaseType.h"
#include "Tracer.h"

#ifdef DBSUPPORT
#include "DataBase.h"
#include "SqliteWrapper.h"


typedef enum
{
    NAME=0,
    UID,
}TBL_TEST_COLUMN;

void TestDataBase()
{
    SqlResult_S result;
    DataBase* db = new SqliteWrapper();
    db->open("test.db");
    db->exec("select * from tbl_test;", &result);
    for(sint32 i=0; i<result.m_resultStrVect.size(); i++)
    {
        std::string value=result.m_resultStrVect[i];
        TRACE_DEBUG("%s ",value.c_str());
        if (i!=0 && (i+1)%result.m_columns==0)
        {
            TRACE_DEBUG("\n");
        }
    }

    char * buf=NULL;
    sint32 len;
    buf = (char*)malloc(1024);
    memset(buf,0,1024);
    db->selectBlob("select * from tbl_blob;", ":pic", buf, len);
    printf("len=%d,blob=\n%s\n",len,buf);

    db->insertBlob("insert into tbl_blob (pic) values(?);", ":pic", buf, len);
    
}
#else
void TestDataBase()
{
    TRACE_ERR("Unsupport Database Test.\n");
}
#endif
