#include <stdio.h>
#include <string.h>
#include "StringFilter.h"

#include <iostream>

void printStringList(std::vector<std::string> &strVector)
{
    int vectorSize = strVector.size();
    if (vectorSize==0)
    {
        printf("nothing printf!\n");
        return;
    }
    for(int index=0;index<vectorSize; index++)
    {
        printf("string[%d],len=%d:[",index,strVector[index].size());
        for(int i=0;i<strVector[index].size();i++)
        {
            printf("%c",strVector[index][i]);
        }
        printf("]\n");
    }
}
void TestStringFilter()
{
    StringFilter* tf = new StringFilter();
    tf->addFilterRule("<JSON>",6,"</JSON>",7);

	printf("----------------test json data---------------------\n");
    const char* jsonStream="ee<JSON>1234567890";
    std::vector<std::string> strVector = tf->filterOn(jsonStream,strlen(jsonStream));//18
    printStringList(strVector);

    jsonStream="abcdefghijklmn";
    strVector = tf->filterOn(jsonStream,strlen(jsonStream));//32
    printStringList(strVector);

    jsonStream="opqrstuvwxyz</JSON><JSON></JSON>9876543210</JSON>";
    strVector = tf->filterOn(jsonStream,strlen(jsonStream));
    printStringList(strVector);

    jsonStream="<JSON>1111111111</JSON>ff";
    strVector = tf->filterOn(jsonStream,strlen(jsonStream));
    printStringList(strVector);

    jsonStream="<J";
    strVector = tf->filterOn(jsonStream,strlen(jsonStream));
    printStringList(strVector);

    jsonStream="SON>2222222222</JS";
    strVector = tf->filterOn(jsonStream,strlen(jsonStream));
    printStringList(strVector);
    
    jsonStream="ON>dd";
    strVector = tf->filterOn(jsonStream,strlen(jsonStream));
    printStringList(strVector);

    jsonStream="</JSON>33333<JSON>444444<JSON>55555</JSON></JSON><JSON>666666</JSON>";
    strVector = tf->filterOn(jsonStream,strlen(jsonStream));
    printStringList(strVector);
    delete tf;

	printf("----------------test raw data---------------------\n");
    tf = new StringFilter();
    tf->addFilterRule("\r\nOK\r\n", 6,"\r\n",2);
    tf->addFilterRule("\r\n",2,"\r\n",2);
    {
        char rawData[]={'\r','\n','E','R','R','O','R','\r','\n','a','b','c','\r','\n'};
        printf("rawdata size=%d\n",sizeof(rawData));
        std::vector<std::string> strVector = tf->filterOn(rawData,sizeof(rawData));
        printStringList(strVector); 
    }
    {
        char rawData[]={'O','K','\r','\n','C','M','E','+','E','R','R','O','R','\r','\n'};
        printf("rawdata size=%d\n",sizeof(rawData));
        std::vector<std::string> strVector = tf->filterOn(rawData,sizeof(rawData));
        printStringList(strVector); 
    }
    delete tf;
}