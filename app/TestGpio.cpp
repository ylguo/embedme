#include "BaseType.h"
#include "Tracer.h"
#include "Gpio.h"

#include <stdlib.h>

#define GPIO_GET_CTRL_NUM 2
static GpioInfo_S g_gpioGetCfg[GPIO_GET_CTRL_NUM]={
    {1, "GPIO1", 1, GPIO::IN},   //EX_SW_IN
	{1, "GPIO1", 4, GPIO::IN}    //POWER_IN
};

#define GPIO_SET_CTRL_NUM 11
static GpioInfo_S g_gpioSetCfg[GPIO_SET_CTRL_NUM]={
	{2, "GPIO1", 2,  GPIO::OUT},   //LED_OUT_CTL
    {2, "GPIO1", 3,  GPIO::OUT},   //O_PTT
    {2, "GPIO3", 16, GPIO::OUT},   //BEEP_EN
    {2, "GPIO3", 17, GPIO::OUT},   //OPA_SW
    {2, "GPIO3", 18, GPIO::OUT},   //O_SEL_PA 
    {2, "GPIO3", 19, GPIO::OUT},   //O_SEL_PIS
    {2, "GPIO3", 23, GPIO::OUT},   //PWR_CTRL_C
	{2, "GPIO3", 28, GPIO::OUT},   //HS_MIC_EN
	{2, "GPIO3", 30, GPIO::OUT},   //BEEP
	{2, "GPIO3", 31, GPIO::OUT},   //PIS_MIC_EN
	{2, "GPIO5", 2,  GPIO::OUT}    //HS_EAR_EN
};

void printGpioTestMenu()
{
    TRACE_YELLOW("================HELP=================\n");
    TRACE_YELLOW("get : get gpio pin value command.\n");
    TRACE_YELLOW("set : set gpio pin value command.\n");
    TRACE_YELLOW("quit: \'q\' or \'quit\'.\n");
    TRACE_YELLOW("-------------------------------------\n");
}


void TestGpio()
{	
    std::string inputStr;
    while (1)
    {
        printGpioTestMenu();
        std::cin >> inputStr;
        std::cout << inputStr << std::endl;
        if ("q" == inputStr || "quit" == inputStr)
        {
            break;
        }
        else if ("get" == inputStr)
        {
            TRACE_YELLOW("-------------get gpio pin value -----------------\n");
            TRACE_YELLOW("0: get EX_SW_INsignal.\n");
            TRACE_YELLOW("1: get POWER_IN signal.\n");
            TRACE_YELLOW("-------------------------------------\n");
            std::cin >> inputStr;
            std::cout << inputStr << std::endl;
            int index = atoi(inputStr.c_str());
			GpioImp* gpio = GpioFactory::getInstance()->getGpioImp(g_gpioGetCfg[index].m_group, g_gpioGetCfg[index].m_pin);
		    if (gpio!=NULL)
		    {
				gpio->config(g_gpioGetCfg[index].m_dir);

				/* 读取指定GPIO的pin值 */
			    TRACE_YELLOW("============read gpio=========\n");
			    gpio->config(GPIO::IN);
				TRACE_YELLOW("read gpio pin value:%d\n", gpio->getPinValue());
			}
        }
        else if ("set" == inputStr)
        {
            TRACE_YELLOW("-------------set gpio pin value -----------------\n");
            TRACE_YELLOW("0: set LED_OUT_CTL signal.\n");
            TRACE_YELLOW("1: set O_PTT signal.\n");
            TRACE_YELLOW("2: set BEEP_EN signal.\n");
			TRACE_YELLOW("3: set OPA_SW signal.\n");
			TRACE_YELLOW("4: set O_SEL_PA signal.\n");
			TRACE_YELLOW("5: set O_SEL_PIS signal.\n");
			TRACE_YELLOW("6: set PWR_CTRL_C signal.\n");
			TRACE_YELLOW("7: set HS_MIC_EN signal.\n");
            TRACE_YELLOW("8: set BEEP signal.\n");
            TRACE_YELLOW("9: set PIS_MIC_EN signal.\n");
			TRACE_YELLOW("10: set HS_EAR_EN signal.\n");		
            TRACE_YELLOW("-------------------------------------\n");
            std::cin >> inputStr;
            std::cout << inputStr << std::endl;
            int index = atoi(inputStr.c_str());
			GpioImp* gpio = GpioFactory::getInstance()->getGpioImp(g_gpioGetCfg[index].m_group, g_gpioGetCfg[index].m_pin);
		    if (gpio!=NULL)
		    {
				gpio->config(g_gpioGetCfg[index].m_dir);

				TRACE_YELLOW("-------------input pin value -----------------\n");
	            TRACE_YELLOW("0: low.\n");
	            TRACE_YELLOW("1: high.\n");		
	            TRACE_YELLOW("-------------------------------------\n");
	            std::cin >> inputStr;
	            std::cout << inputStr << std::endl;

				/* 设置指定GPIO的pin值 */ 
				if (atoi(inputStr.c_str()) == 0)
				{
				    gpio->setPinValue(GPIO::LOW);
				}
				else
				{
				    gpio->setPinValue(GPIO::HIGH);
				}
			}
        }
        else
        {
            TRACE_RED("Unknown command: %s !!!\n",inputStr.c_str());
        }
        inputStr.clear();
    }
}
