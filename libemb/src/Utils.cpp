/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "Utils.h"
#include "Tracer.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h> 
#include <sys/time.h>
#include <ctype.h>

/**
 *  \brief  生成一个随机数
 *  \param  max 最大取值
 *  \return 返回随机数
 *  \note   生成一个0~max之间的随机数
 */
int Utils::random(int max)
{
    if (max<=0)
    {
        max = RAND_MAX;
    }
    struct timeval current;
    unsigned seed;
    gettimeofday(&current,NULL);
    seed = current.tv_sec+current.tv_usec;
    srand((unsigned)seed);
    return rand()%max;
}

/**
 *  \brief  判断主机是大端还是小端字节序
 *  \param  void
 *  \return 大端字节序返回true,小端字节序返回false
 *  \note   小端模式是数字的低位在低地址,大端模式是高位在低地址
 *			0x1234				---数值
 *			[0x34][0x12]---小端模式中内存存储(计算机逻辑,低地址放低位)
 *			[0x12][0x34]---大端模式中内存存储(更符合人类思维习惯)
 */
bool Utils::isBigEndian(void)
{
	union tmp_u
	{
		sint32 a;
		uint8  b;
	}tmp;
	tmp.a=0x01;
	return (tmp.b)?false:true;
}

/**
 *  \brief  将数字转换成小端字节序
 *  \param  value 16位宽的数字
 *  \return 返回转换后的数字
 *  \note   none
 */
uint16 Utils::host2LitEndian(uint16 value)
{
	uint8 size=2;
	char buf[2]={0};
	while(size>0)
	{
		*(buf+2-size) = value%256;
		value = value>>8;
		size--;
	}
	return *(uint16*)buf;
}

/**
 *  \brief  将数字转换成小端字节序
 *  \param  value 32位宽的数字
 *  \return 返回转换后的数字
 *  \note   none
 */
uint32 Utils::host2LitEndian(uint32 value)
{
	uint8 size=4;
	char buf[4]={0};
	while(size>0)
	{
		*(buf+4-size) = value%256;
		value = value>>8;
		size--;
	}
	return *(uint32*)buf;
}

/**
 *  \brief  将数字转换成大端字节序
 *  \param  value 16位宽的数字
 *  \return 返回转换后的数字
 *  \note   none
 */
uint16 Utils::host2BigEndian(uint16 value)
{
	uint8 size=2;
	char buf[2]={0};
	while(size>0)
	{
		*(buf+size-1) = value%256;
		value = value>>8;
		size--;
	}
	return *(uint16*)buf;;
}

/**
 *  \brief  将数字转换成大端字节序
 *  \param  value 32位宽的数字
 *  \return 返回转换后的数字
 *  \note   none
 */
uint32 Utils::host2BigEndian(uint32 value)
{
	uint8 size=4;
	char buf[4]={0};
	while(size>0)
	{
		*(buf+size-1) = value%256;
		value = value>>8;
		size--;
	}
	return *(uint32*)buf;
}

/**
 *  \brief  unicode编码转换为utf8编码的字符串
 *  \param  unicode 字符编码
 *  \param  utfcode 转换后的UTF8编码缓冲区(最少6字节)
 *  \return 返回转换后的字符,转换失败时返回空字符
 *  \note   none
 */
std::string Utils::unicodeOneToUtf8String(uint32 unicode)
{
    int utflen=0;
    char utf8code[7]={0};

    unicode=host2LitEndian(unicode);
    
    if ( unicode <= 0x0000007F )
    {
        // * U-00000000 - U-0000007F:  0xxxxxxx
        utf8code[0]     = (unicode & 0x7F);
        utflen = 1;
    }
    else if ( unicode >= 0x00000080 && unicode <= 0x000007FF )
    {
        // * U-00000080 - U-000007FF:  110xxxxx 10xxxxxx
        utf8code[1] = (unicode & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 6) & 0x1F) | 0xC0;
        utflen = 2;
    }
    else if ( unicode >= 0x00000800 && unicode <= 0x0000FFFF )
    {
        // * U-00000800 - U-0000FFFF:  1110xxxx 10xxxxxx 10xxxxxx
        utf8code[2] = (unicode & 0x3F) | 0x80;
        utf8code[1] = ((unicode >>  6) & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 12) & 0x0F) | 0xE0;
        utflen = 3;
    }
    else if ( unicode >= 0x00010000 && unicode <= 0x001FFFFF )
    {
        // * U-00010000 - U-001FFFFF:  11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
        utf8code[3] = (unicode & 0x3F) | 0x80;
        utf8code[2] = ((unicode >>  6) & 0x3F) | 0x80;
        utf8code[1] = ((unicode >> 12) & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 18) & 0x07) | 0xF0;
        utflen = 4;
    }
    else if ( unicode >= 0x00200000 && unicode <= 0x03FFFFFF )
    {
        // * U-00200000 - U-03FFFFFF:  111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        utf8code[4] = (unicode & 0x3F) | 0x80;
        utf8code[3] = ((unicode >>  6) & 0x3F) | 0x80;
        utf8code[2] = ((unicode >> 12) & 0x3F) | 0x80;
        utf8code[1] = ((unicode >> 18) & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 24) & 0x03) | 0xF8;
        utflen = 5;
    }
    else if ( unicode >= 0x04000000 && unicode <= 0x7FFFFFFF )
    {
        // * U-04000000 - U-7FFFFFFF:  1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        utf8code[5] = (unicode & 0x3F) | 0x80;
        utf8code[4] = ((unicode >>  6) & 0x3F) | 0x80;
        utf8code[3] = ((unicode >> 12) & 0x3F) | 0x80;
        utf8code[2] = ((unicode >> 18) & 0x3F) | 0x80;
        utf8code[1] = ((unicode >> 24) & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 30) & 0x01) | 0xFC;
        utflen = 6;
    }
    std::string utf8str = utf8code;
    return utf8str;

}
/**
 *  \brief  utf8编码转换为unicode编码的字符串
 *  \param  utfcode UTF8编码(1~6字节)
 *  \return 成功返回unicode编码,失败返回0
 *  \note   none
 */
uint32 Utils::utf8OneToUnicode(const char* utf8code)
{   
    // b1 表示UTF-8编码的高字节, b2 表示次高字节, ...   
    uint8 b1, b2, b3, b4, b5, b6;  
    
    uint8 utfbytes=0;
    uint8 tmp=utf8code[0];
    while((tmp&0x80)!=0)
    {
        utfbytes++;
        tmp = tmp<<1;
    }
    
    uint32 unicode = 0x0;
    uint8 *unibuf = (uint8*)&unicode;  
    switch (utfbytes)  
    {  
        case 0:  
            unibuf[0] = utf8code[0];  
            break;  
        case 2:  
            b1 = utf8code[0];  
            b2 = utf8code[1];  
            if ((b2&0xE0) != 0x80)  
            {
                return 0;
            }
            unibuf[0] = (b1<<6) + (b2&0x3F);  
            unibuf[1] = (b1>>2)&0x07;  
            break;  
        case 3:  
            b1 = utf8code[0];  
            b2 = utf8code[1];  
            b3 = utf8code[2];  
            if (((b2&0xC0) != 0x80) || 
                ((b3&0xC0) != 0x80))  
            {
                return 0;
            }
            unibuf[0] = (b2<<6) + (b3&0x3F);  
            unibuf[1] = (b1<<4) + ((b2>>2)&0x0F);  
            break;  
        case 4:  
            b1 = utf8code[0];  
            b2 = utf8code[1];  
            b3 = utf8code[2];  
            b4 = utf8code[3];  
            if (((b2&0xC0) != 0x80) || 
                ((b3&0xC0) != 0x80)  ||
                ((b4&0xC0) != 0x80))  
            {
                return 0;
            }
            unibuf[0] = (b3<<6) + (b4&0x3F);  
            unibuf[1] = (b2<<4) + ((b3>>2)&0x0F);  
            unibuf[2] = ((b1<<2)&0x1C)  + ((b2>>4)&0x03);  
            break;  
        case 5:  
            b1 = utf8code[0];  
            b2 = utf8code[1];  
            b3 = utf8code[2];  
            b4 = utf8code[3];  
            b5 = utf8code[4];  
            if (((b2 & 0xC0) != 0x80) ||
                ((b3 & 0xC0) != 0x80)  || 
                ((b4 & 0xC0) != 0x80) || ((b5 & 0xC0) != 0x80) )  
            {
                return 0;
            }
            unibuf[0] = (b4<<6) + (b5&0x3F);  
            unibuf[1] = (b3<<4) + ((b4>>2)&0x0F);  
            unibuf[2] = (b2<<2) + ((b3>>4)&0x03);  
            unibuf[3] = (b1<<6);  
            break;  
        case 6:  
            b1 = utf8code[0];  
            b2 = utf8code[1];  
            b3 = utf8code[2];  
            b4 = utf8code[3];  
            b5 = utf8code[4];  
            b6 = utf8code[5];  
            if (((b2&0xC0) != 0x80) ||
                ((b3&0xC0) != 0x80) || 
                ((b4&0xC0) != 0x80) || 
                ((b5&0xC0) != 0x80) || 
                ((b6&0xC0) != 0x80) )  
            {
                return 0;
            }
            unibuf[0] = (b5<< 6) + (b6 & 0x3F);  
            unibuf[1] = (b5<< 4) + ((b6 >> 2)&0x0F);  
            unibuf[2] = (b3<< 2) + ((b4 >> 4)&0x03);  
            unibuf[3] = ((b1<<6)&0x40) + (b2&0x3F);  
            break;  
        default:  
            return 0;  
            break;  
    }  
    return unicode;
}
/**
 *  \brief  将字符串转换为小写
 *  \param  str 源字符串
 *  \return 返回转换好的字符串
 *  \note   none
 */
std::string Utils::toLower(const char* str)
{
    std::string ret="";
    int i=0;
    char ch = *(str+i);
    while(ch!=0)
    {
        ret.append(1,(char)tolower(ch));   
        ch = *(str+(++i));
    }
    return ret;
}
/**
 *  \brief  将字符串转换为大写
 *  \param  str 源字符串
 *  \return 返回转换好的字符串
 *  \note   none
 */
std::string Utils::toUpper(const char* str)
{
    std::string ret="";
    int i=0;
    char ch = *(str+i);
    while(ch!=0)
    {
        ret.append(1,(char)toupper(ch));   
        ch = *(str+(++i));
    }
    return ret;
}
    
/**
 *  \brief  查找start开始与end结束的字符串
 *  \param  source 源字串
 *  \param  start 开始字串
 *  \param  end 结束字串
 *  \return 返回找到的字符串
 *  \note   none
 */
std::string Utils::findString(const string& source, const string& start, const string& end)
{
	std::string restStr;
	uint32 startPos,endPos;
	/* 找出匹配关键字 */
	startPos = source.find(start,0);
	if (startPos==string::npos)
	{
		TRACE_DEBUG("Utils::findString,no start[%s]...\n",start.c_str());
		return "";
	}
	
	endPos = source.find(end,1);
	if (endPos==string::npos)
	{
		TRACE_DEBUG("Utils::findString,no end[%s]...\n",end.c_str());
		return "";
	}

	return source.substr(startPos,endPos-startPos+end.size());
}

/**
 *  \brief  查找匹配字段
 *  \param  source 源字串
 *  \param  pattern 匹配字串
 *  \param  before 匹配字串之前的字串
 *  \param  after 匹配字串之后的字串
 *  \return 返回找到的字符串
 *  \note   none
 */
string Utils::findString(const string& source, const string& pattern, const string& before, const string& after)
{
	std::string restStr;
	uint32 startPos,endPos;
	/* 找出匹配关键字 */
	startPos = source.find(pattern,0);
	if (startPos==string::npos)
	{
		TRACE_DEBUG("Utils::findString,no pattern[%s]...\n",pattern.c_str());
		return "";
	}

	/* 从匹配关键字开始查找字符串 */
	restStr = source.substr(startPos);
	startPos = restStr.find(before,0);
	if (startPos==string::npos)
	{
		TRACE_DEBUG("Utils::findString,no before[%s]...\n",before.c_str());
		return "";
	}
	/* 跳过before字串 */
	startPos += before.size();
	endPos = restStr.find(after,startPos);
	if (endPos==string::npos)
	{
		TRACE_DEBUG("Utils::findString,no after[%s]...\n",after.c_str());
		return "";
	}
	/* 截取before与after之间的字串 */
	return restStr.substr(startPos,endPos-startPos);
}
/**
 *  \brief  从字符串头部去除trimch包含的字符,直至遇到非trimch字符为止
 *  \param  source 源字串
 *  \return 返回trim后得到的字符串
 *  \note   例:result = trimBeginWith(" 0001234","0 ");//result="1234"
 */
string Utils::trimBeginWith(const string& source,const string& trimch)
{
    std::string result;
    int len=source.size();
    int trimLen = trimch.size();
    int i=0;
    while(i<len)
    {
        int j;
        for(j=0;j<trimLen;j++)
        {
            if (source[i]==trimch[j])
            {
                break;
            }   
        }
        if (j==trimLen)
        {
            result=source.substr(i);
            break;
        }
        i++;
    }
    return result;
}
/**
 *  \brief  去除字符串首尾两端的不可见字符
 *  \param  source 源字串
 *  \return 返回trim后得到的字符串
 *  \note   none
 */
std::string Utils::trimEndingBlank(const string& source)
{
	std::string result;
    int len=source.size();
    int i=0;
    while(i<len)
    {
        if (source[i]>32)
        {
            result=source.substr(i);
            break;
        }
        i++;
    }
    len = result.size();
    i=0;
    while(i<len)
    {
        if (result[len-1-i]>32)
        {
            result=result.substr(0,len-i);
            break;
        }
        i++;
    }
	return result;
}

/**
 *  \brief  去除字符串中所有不可见字符
 *  \param  source 源字串
 *  \return 返回trim后得到的字符串
 *  \note   none
 */
std::string Utils::trimAllBlank(const string& source)
{
    int i=0;
    int len=source.size();
    std::string result="";
    while(i<len)
    {
        if (source[i]>32)
        {
            result.append(1,source[i]);
        }
        i++;
    }
    return result;
}

/**
 *  \brief  分割字符串
 *  \param  source 源字串
 *  \return 返回分割后的字串数组
 *  \note   分割从startFlag后到endFlag之前的字串,分割符为cutFlag
 */
vector<string> Utils::cutString(const string& source,const string& startFlag,const string& endFlag,const string& cutFlag)
{
    vector<string> vecResult;
    int startPos = source.find(startFlag) + startFlag.length();
	int endPos = source.find(endFlag);
    
    if (source.empty())
    {
        return vecResult;
    }
	if (endPos != string::npos && endPos <= startPos)
	{
		return vecResult;
	}
	if (cutFlag.empty())
	{
		return vecResult;
    }
    
	string subStr;
	int subPosEnd;
	while (endPos != string::npos && endPos > startPos || endPos == string::npos)
	{
		subPosEnd = source.find(cutFlag, startPos);
		if (subPosEnd == string::npos)
		{	
			if (endPos != string::npos)
				vecResult.push_back(source.substr(startPos, endPos-startPos));
			else
				vecResult.push_back(source.substr(startPos));
			break;
		}
		vecResult.push_back(source.substr(startPos, subPosEnd-startPos));
		startPos = subPosEnd + cutFlag.length();
	}
	return vecResult;
}
/**
 *  \brief  获取匹配次数
 *  \param  source 源字串
 *  \param  pattern 匹配字串
 *  \return 返回匹配次数
 *  \note   none
 */
int Utils::stringPatternCount(const string& source, const string& pattern)
{
	int atFind=0;
	int i =0;
	if(source.empty() || pattern.empty())
	{
		return 0;
	}
	while(1)
	{
		atFind = source.find(pattern,atFind);
		if(atFind!=string::npos)
		{
			atFind += pattern.size();
			i++;
		}
		else
		{
			break;
		}
	}
	return i;
}

/**
 *  \brief  ASCII码转换为数字
 *  \param  ch ASCII码字符
 *  \return ch对应的数字
 *  \note   字符'0'~'9'转换后为数字0~9,'a'~'f'或'A'~'F'转换后为10~16,其余返回值为-1
 */
sint8 Utils::ascii2digital(char ch)
{
	if(ch>='a' && ch <='f')
	{
		return (ch-'a'+0x0A);
	}
	else if(ch>='A' && ch <='F')
	{
		return (ch-'A'+0x0A);
	}
	else if(ch>='0' && ch<='9')
	{
		return (ch-'0');
	}
	else
	{
		return -1;
	}
}

/**
 *  \brief  数字转换为ASCII码
 *  \param  val 数字
 *  \return 数字对应的ASCII码字符
 *  \note   数字0~9转成字符'0'~'9',数字10~16转成'A'~'F',其余返回值为0,为字符串结束符
 */
char Utils::digital2ascii(uint8 val)
{
	if(val>=0 && val<=9)
	{
		return (val+'0');
	}
	else if(val>=0x0A && val<=0x0F)
	{
		return (val-0x0A+'A');
	}
	else
	{
		return 0;
	}
}

/**
 *  \brief  字符串转为编码
 *  \param  codestr 待转换字符创
 *  \param  codebuf 转换好的编码缓存
 *  \param  len 缓存大小
 *  \return 成功返回code的长度,失败返回STATUS_ERROR
 *  \note   "0041001F"  ==>[0x00][0x41][0x00][0x1F]
 */
int Utils::string2code(const std::string codestr,char* codebuf,int len)
{
	int i;
	if (codestr.empty() ||
		NULL==codebuf)
	{
		TRACE_ERR("Utils::string2code parameter error.\n");
		return STATUS_ERROR;
	}
	if (len < codestr.size()/2)
	{
		TRACE_ERR("Utils::string2code buf len(%d) error.\n",len);
		return STATUS_ERROR;
	}
	
	for(i=0; i<codestr.size()/2; i++)
	{
		sint8 a,b;
		a=Utils::ascii2digital(codestr[2*i]);
		b=Utils::ascii2digital(codestr[2*i+1]);
		if(-1==a || -1==b)
		{
			TRACE_ERR("Utils::string2code code string error.\n");
			return STATUS_ERROR;
		}
		codebuf[i]=(a<<4)+b;
	}
	return i+1;
}

/**
 *  \brief  编码转为字符串
 *  \param  codebuf 待转换的编码缓存
 *  \param  len 缓存大小
 *  \return 成功返回转换后的字符串,失败返回空字符串
 *  \note   [0x00][0x41][0x00][0x1F]==>"0041001F"
 */
std::string Utils::code2string(const char* codebuf,int len)
{
	if (codebuf==NULL || len<=0)
	{
		TRACE_ERR("Utils::code2string parameter error.\n");
		return "";
	}

	std::string codestr="";
	for(int i=0; i<len; i++)
	{
		char code[3]={0};
		code[0] = Utils::digital2ascii((codebuf[i]>>4)&0x0F);
		code[1] = Utils::digital2ascii(codebuf[i]&0x0F);
		if (0==code[0] || 0==code[1])
		{
			TRACE_ERR("Utils::code2string unicode error.\n");
			return "";
		}
		codestr += code;
	}
	return codestr;
}
/**
 *  \brief  位反转
 *  \param  
 *  \return 返回结果
 *  \note   
 */
uint16 Utils::bitsReverse(uint16 ref, uint8 bits)
{
	uint16 value = 0;
	for (uint16 i=1; i<(bits+1); i++)
	{
		if (ref&0x01)
        {
            value |= 1 << (bits - i);
        }
		ref >>= 1;
	}
	return value;
}
/**
 *  \brief  将文件路径去掉
 *  \param  filePath 文件全路径
 *  \return 返回文件名
 *  \note   
 */
string Utils::trimFilePath(const char* filePath)
{
    const char* pNameBegin = filePath + strlen(filePath);
    do
    {
        if (*pNameBegin == '\\' || *pNameBegin == '/') 
        { 
            pNameBegin++; 
            break;
        }
        if (pNameBegin == filePath) 
        { 
            break; 
        }
        pNameBegin--;
    } while (1);
    return string(pNameBegin);
}
/**
 *  \brief  算术计算函数
 *  \param  expression 算术表达式,支持整数的加减乘除及括号
 *  \return 返回计算结果
 *  \note   
 */
int Utils::eval(const char* expression)
{
    return 0;
}