/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "Thread.h"
#include "Tracer.h"
#include "Timer.h"
#include <stdlib.h>

Thread::Thread()
{
	m_pRunnableTarget = NULL;
	m_threadStatus = THREAD_STATUS_NEW;
	m_threadID = 0;
	pthread_attr_init(&m_ThreadAttr);
}

Thread::~Thread()
{
	pthread_attr_destroy(&m_ThreadAttr);
}

/**
 *  \brief  启动线程
 *  \param  pRunnable RunnableProtocol对象
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool Thread::start(RunnableProtocol* pRunnable)
{
	if(NULL==pRunnable || m_threadID != 0)
	{
		return false;
	}
	m_pRunnableTarget = pRunnable;
	if(0!=pthread_create(&m_threadID, &m_ThreadAttr, threadEntry, this))
	{
		return false;
	}
	m_threadStatus = THREAD_STATUS_START;
	return true;
}

/* 线程入口点 */
void * Thread::threadEntry(void *arg)
{
	Thread* pThread = (Thread*)arg;
	if(pThread != NULL)
	{
		pThread->run();
	}
	return (void*)0;
}

/* 线程运行函数 */
void Thread::run()
{
	m_threadStatus = THREAD_STATUS_RUNNING;
	m_threadID = pthread_self();
	if(m_pRunnableTarget != NULL)
	{
		m_pRunnableTarget->run();
	}
	m_threadStatus = THREAD_STATUS_EXIT;
	m_threadID = 0;
	pthread_exit(NULL);
}

/**
 *  \brief  判断线程是否正在运行
 *  \param  void
 *  \return 正在运行返回true,否则返回false
 *  \note   none
 */
bool Thread::isRunning()
{
	if(m_threadStatus == THREAD_STATUS_RUNNING)
	{
		return true;
	}
	return false;
}

/**
 *  \brief  取消线程
 *  \param  void
 *  \return void
 *  \note   none
 */
bool Thread::cancel()
{
#ifdef OS_ANDROID
	return false;
#else
    if (m_threadID > 0)
    {
    	if (0!=pthread_cancel(m_threadID))
    	{
    		TRACE_ERR_CLASS("pthread cancel error.\n");
    		return false;
    	}
        pthread_join(m_threadID, NULL);
    }
    m_threadID = 0;
	m_threadStatus = THREAD_STATUS_EXIT;
    return true;
#endif
}

/**
 *  \brief  线程休眠
 *  \param  ms 毫秒数
 *  \return void
 *  \note   none
 */
void Thread::msleep(int ms)
{
    Timer::msDelay(ms);
}

/**
 *  \class  TaskInfo_S
 *  \brief  任务参数结构体 
 */
typedef struct{
    RunnableProtocol* m_owner;
    SEL_task m_task;
    void* m_arg;
}TaskInfo_S;

/**
 *  \brief  创建任务(子线程)
 *  \param  pRunnable RunnableProtocol对象
 *  \param  task 任务方法
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int Thread::createTask(RunnableProtocol* owner,SEL_task task,void* taskArg)
{
    pthread_t threadID;
    /* 一定要使用malloc分配arg内存,等线程跑起来后再free;
     * 千万不能直接定义一个临时变量,否则会出现段错误;
     * 原因是:pthread_create返回时线程实际还没运行,
     * 而返回后此函数退出,定义的变量已经失效,因此出错!
     */
    TaskInfo_S *taskInfo = (TaskInfo_S*)malloc(sizeof(TaskInfo_S));
    taskInfo->m_owner=owner;
    taskInfo->m_task=task;
    taskInfo->m_arg=taskArg;
	if(NULL==owner || task == NULL)
	{
	    TRACE_ERR("Thread::createTask,parameter error!\n");
		return STATUS_ERROR;
	}
	if(0!=pthread_create(&threadID, NULL, taskEntry, taskInfo))
	{
	    TRACE_ERR("Thread::createTask,create task thread failed!\n");
		return STATUS_ERROR;
	}
	return STATUS_OK;   
}

/* 任务入口点 */
void * Thread::taskEntry(void *arg)
{
	TaskInfo_S* pTask = (TaskInfo_S*)arg;
	if(pTask->m_owner!=NULL && pTask->m_task!=NULL)
	{
		((pTask->m_owner)->*(pTask->m_task))(pTask->m_arg);
	}
    free(pTask);
    pTask = NULL;
	return (void*)0;
}
