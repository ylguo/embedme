/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "Mutex.h"

MutexLock::MutexLock()
{
	pthread_mutex_init(&m_mutex,NULL);
}

MutexLock::~MutexLock()
{
	pthread_mutex_destroy(&m_mutex);
}

/**
 *  \brief  互斥锁上锁
 *  \note   none
 */
int MutexLock::lock()
{
	return pthread_mutex_lock(&m_mutex);
}
/**
 *  \brief  互斥锁解锁
 *  \note   none
 */
int MutexLock::unLock()
{
	return pthread_mutex_unlock(&m_mutex);
}
/**
 *  \brief  互斥锁尝试上锁
 *  \note   none
 */
int MutexLock::tryLock()
{
	return pthread_mutex_trylock(&m_mutex);
}

AutoLock::AutoLock(MutexLock* mutexLock):
m_mutexLock(mutexLock)
{
	m_mutexLock->lock();
}

AutoLock::~AutoLock()
{
	m_mutexLock->unLock();
}

MutexCond::MutexCond(MutexLock* lock)
{
    m_lock = lock;
    pthread_cond_init(&m_cond,NULL);
}

MutexCond::~MutexCond()
{
}
/**
 *  \brief  等待条件变量
 *  \note   执行该方法后线程会阻塞并释放锁,一直等待条件变量.
 */
void MutexCond::wait()
{
    pthread_cond_wait(&m_cond,&(m_lock->m_mutex));
}
/**
 *  \brief  满足条件变量,通知等待者
 *  \note   none
 */
void MutexCond::meet()
{
    pthread_cond_signal(&m_cond);
}