/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "Directory.h"
#include "File.h"
#include <limits.h>
#include <stdlib.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <algorithm>

Directory::Directory():
m_currentPath(".")
{
}

Directory::~Directory()
{
}

/**
 *  \brief  判断目录是否为空
 *  \param  name 目录全名(如:/usr/share)
 *  \return 目录为空或不存在返回true,不为空则返回false
 *  \note   none
 */
bool Directory::isEmpty(const char* name)
{
	DIR * dirp = NULL;
    struct dirent *dp=NULL;
    int num=0;
    dirp = opendir(name);
    if (NULL==dirp)
    {
		//TRACE_ERR("Directory::isDirEmpty open dir:%s error(%s).\n",name,ERROR_STRING);
		return true;
    }
    while((dp=readdir(dirp))!=NULL)
    {
        if (strcmp(dp->d_name,".")==0 ||
            strcmp(dp->d_name,"..")==0)
        {
            continue;
        }
        else
        {
            num++;
            break;
        }
    }
    closedir(dirp);
    if(num>0)
    {
        return false;
    }
	return true;
}

/**
 *  \brief  判断目录是否存在
 *  \param  name 目录全名(如:/usr/share)
 *  \return 目录存在返回true,不存在则返回false
 *  \note   none
 */
bool Directory::isExist(const char* name)
{
    return File::isExist(name);
}
/**
 *  \brief  获取文件夹内容大小
 *  \param  name 文件夹名
 *  \return 返回文件夹的大小，以byte为单位
 *  \note   none
 */
int Directory::getContentSize(const char* name)
{
    return File::getContentSize(name);
}

/**
 *  \brief  进入目录
 *  \param  path 目录全名(如:/usr/share)
 *  \return 成功返回true，失败返回false
 *  \note   none
 */
bool Directory::enter(const string & path)
{
    string resolved;
    if (path.empty())
    {
        return false;
    }
    if (path[0]!='/')
    {
        resolved = m_currentPath+"/"+path;
    }
    else
    {
        resolved = path;
    }
    DIR* dirp;
    struct dirent * dp = NULL;
    dirp = opendir(resolved.c_str());
    if (NULL == dirp)
    {
        return false;
    }
    closedir(dirp);
    char buf[1024]={0};
    if (NULL==realpath(resolved.c_str(),buf))
    {
        return false;
    }
    m_currentPath = string(buf);
    return true;
}

/**
 *  \brief  获取当前路径
 *  \param  none
 *  \return 返回绝对路径
 *  \note   none
 */
string Directory::current()
{
    return m_currentPath;
}

/**
 *  \brief  获取目录下所有文件
 *  \param  none
 *  \return 返回所有文件名
 *  \note   none
 */
vector<string> Directory::listAll()
{
    vector<string> fileVector;
    std::string fileName;
    DIR* dirp = opendir(m_currentPath.c_str());
    if (NULL == dirp)
    {
        return fileVector;
    }
    #if 0
    struct dirent *dp1 = (struct dirent*)malloc(sizeof(struct dirent));
    struct dirent *dp2 = (struct dirent*)malloc(sizeof(struct dirent));
    while ((readdir_r(dirp,dp1,&dp2))!=0)
    {
        fileName = string(dp2->d_name);
        fileVector.push_back(fileName);
    }
    closedir(dirp);
    free(dp1);
    free(dp2);
    #else
    struct dirent *dp;
    while ((dp=readdir(dirp))!=NULL)
    {
        fileName = string(dp->d_name);
        fileVector.push_back(fileName);
    }
    closedir(dirp);
    #endif
    sort(fileVector.begin(),fileVector.end());
    return fileVector;
}

