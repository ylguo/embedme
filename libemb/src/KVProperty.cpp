/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "Tracer.h"
#include "KVProperty.h"
#include "Utils.h"
#include <vector>
#include <stdio.h>
#include <stdlib.h>

/**
 *  \brief  属性值构造函数
 *  \param  none
 *  \return none
 */
KVValue::KVValue()
{
    clear();
}
/**
 *  \brief  属性值虚构函数
 *  \param  none
 *  \return none
 */
KVValue::~KVValue()
{
}

/**
 *  \brief  属性值类型
 *  \param  none
 *  \return 返回属性值类型(BASETYPE_E)
 */
int KVValue::baseType()
{
    return m_type;
}

/**
 *  \brief  属性值清零
 *  \param  none
 *  \return none
 */
void KVValue::clear()
{
    m_type = BASETYPE_NONE;
    m_int = 0;
    m_double = 0.0;
    m_string = "";
    m_intArray.clear();
    m_doubleArray.clear();
    m_stringArray.clear();
    m_tuple.clear();
}

/**
 *  \brief  从格式化的字符串中初始化属性值
 *  \param  valueString 格式化字符串
 *  \return 成功返回true,失败返回false
 *  \note   目前支持的属性值格式有:
 */
bool KVValue::initWithString(const string& valueString)
{
    clear();
    string tmpString = Utils::trimEndingBlank(valueString);
    int len = tmpString.size();
    if (tmpString[0]=='\"' && tmpString[len-1]=='\"')
    {
        m_type = BASETYPE_STRING;
    }
    else if (tmpString[0]=='[' && tmpString[len-1]==']')
    {
        if (tmpString.find("\"")!=string::npos)
        {
            m_type = BASETYPE_STRINGARRAY;
        }
        else if (tmpString.find(".")!=string::npos)
        {
            m_type = BASETYPE_DOUBLEARRAY;
        }
        else
        {
            m_type = BASETYPE_INTARRAY;
        }
    }
    else if (tmpString[0]=='(' && tmpString[len-1]==')')
    {
        m_type = BASETYPE_TUPLE;
    }
    else if (tmpString[0]=='<' && tmpString[len-1]=='>')
    {
        if (tmpString.find(".")!=string::npos)
        {
            m_type = BASETYPE_DOUBLE;
        }
        else
        {
            m_type = BASETYPE_INT;
        }
    }
    else
    {
        return false;
    }
    
    switch (m_type)
    {
        case BASETYPE_INT:
        {
            m_int = atoi(tmpString.c_str()+1);
            break;
        }
        case BASETYPE_DOUBLE:
        {
            m_double = atof(tmpString.c_str()+1);
            break;
        }
        case BASETYPE_STRING:
        {
            m_string = tmpString.substr(1,len-2);
            break;
        }
        case BASETYPE_INTARRAY:
        {
            m_intArray.initWithString(tmpString);
            break;
        }
        case BASETYPE_DOUBLEARRAY:
        {
            m_doubleArray.initWithString(tmpString);
            break;
        }
        case BASETYPE_STRINGARRAY:
        {
            m_stringArray.initWithString(tmpString);
            break;
        }
        case BASETYPE_TUPLE:
        {
            m_tuple.initWithString(tmpString);
            break;
        }
        default:
            return false;
    }
    return true;
}
/**
 *  \brief  序列化属性值
 *  \param  none
 *  \return 序列化字符串
 */
string KVValue::serialize()
{
    switch (m_type)
    {
        case BASETYPE_INT:
        {
            char buf[32]={0};
            sprintf(buf,"<%d>",m_int);
            return string(buf);
        }
        case BASETYPE_DOUBLE:
        {
            char buf[32]={0};
            sprintf(buf,"<%lf>",m_double);
            return string(buf);
        }
        case BASETYPE_STRING:
        {
            return string("\""+m_string+"\"");
        }
        case BASETYPE_INTARRAY:
        {
            return m_intArray.serialize();
        }
        case BASETYPE_DOUBLEARRAY:
        {
            return m_doubleArray.serialize();
        }
        case BASETYPE_STRINGARRAY:
        {
            return m_stringArray.serialize();
        }
        case BASETYPE_TUPLE:
        {
            return m_tuple.serialize();
        }
        default:
            return "";
    }
}
/**
 *  \brief  属性值转int
 *  \param  none
 *  \return 整数
 */
int KVValue::toInt()
{
    if (m_type!=BASETYPE_INT)
    {
        return 0;
    }
    return m_int;
}
/**
 *  \brief  属性值转double
 *  \param  none
 *  \return 浮点数
 */
double KVValue::toDouble()
{
    if (m_type!=BASETYPE_DOUBLE)
    {
        return 0.0;
    }
    return m_double;
}

/**
 *  \brief  属性值转string
 *  \param  none
 *  \return 字符串
 */
string KVValue::toString()
{
    if (m_type!=BASETYPE_STRING)
    {
        return "";
    }
    return m_string;
}

/**
 *  \brief  属性值转IntArray
 *  \param  none
 *  \return 整型数组
 */
IntArray KVValue::toIntArray()
{
    if (m_type!=BASETYPE_INTARRAY)
    {
        m_intArray.clear();
    }
    return m_intArray;
}

/**
 *  \brief  属性值转DoubleArray
 *  \param  none
 *  \return 浮点数组
 */
DoubleArray KVValue::toDoubleArray()
{
    if (m_type!=BASETYPE_DOUBLEARRAY)
    {
        m_doubleArray.clear();
    }
    return m_doubleArray;
}

/**
 *  \brief  属性值转StringArray
 *  \param  none
 *  \return 字符串数组
 */
StringArray KVValue::toStringArray()
{
    if (m_type!=BASETYPE_STRINGARRAY)
    {
        m_stringArray.clear();
    }
    return m_stringArray;
}

/**
 *  \brief  属性值转元组
 *  \param  none
 *  \return 元组
 */
Tuple KVValue::toTuple()
{
    if (m_type!=BASETYPE_TUPLE)
    {
        m_tuple.clear();
    }
    return m_tuple;
}

/**
 *  \brief  赋值运算符重载
 *  \param  value 右值
 *  \return 返回属性值
 */
KVValue& KVValue::operator=(const int& value)
{
    clear();
    m_int = value;
    m_type = BASETYPE_INT;
    return *this;
}
/**
 *  \brief  赋值运算符重载
 *  \param  value 右值
 *  \return 返回属性值
 */
KVValue& KVValue::operator=(const double& value)
{
    clear();
    m_double = value;
    m_type = BASETYPE_DOUBLE;
    return *this;
}
/**
 *  \brief  赋值运算符重载
 *  \param  value 右值
 *  \return 返回属性值
 */
KVValue& KVValue::operator=(const string& value)
{
    clear();
    m_string = value;
    m_type = BASETYPE_STRING;
    return *this;
}
/**
 *  \brief  赋值运算符重载
 *  \param  value 右值
 *  \return 返回属性值
 */
KVValue& KVValue::operator=(const IntArray& value)
{
    clear();
    m_intArray= value;
    m_type = BASETYPE_INTARRAY;
    return *this;
}
/**
 *  \brief  赋值运算符重载
 *  \param  value 右值
 *  \return 返回属性值
 */
KVValue& KVValue::operator=(const DoubleArray& value)
{
    clear();
    m_doubleArray = value;
    m_type = BASETYPE_DOUBLEARRAY;
    return *this;
}
/**
 *  \brief  赋值运算符重载
 *  \param  value 右值
 *  \return 返回属性值
 */
KVValue& KVValue::operator=(const StringArray& value)
{
    clear();
    m_stringArray = value;
    m_type = BASETYPE_STRINGARRAY;
    return *this;
}
/**
 *  \brief  赋值运算符重载
 *  \param  value 右值
 *  \return 返回属性值
 */
KVValue& KVValue::operator=(const Tuple& value)
{
    clear();
    m_tuple = value;
    m_type = BASETYPE_TUPLE;
    return *this;
}

/**
 *  \brief  属性表构造函数
 *  \param  none
 *  \return none
 */
KVProperty::KVProperty()
{
}
/**
 *  \brief  属性表析构函数
 *  \param  none
 *  \return none
 */
KVProperty::~KVProperty()
{
}

/**
 *  \brief  从配置文件初始化属性表
 *  \param  file 属性文件名
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool KVProperty::initWithFile(const string fileName)
{
	int ret;
	AutoLock lock(&m_propertyLock);
	if(fileName.empty())
	{
		TRACE_ERR_CLASS("File name is empty.\n");
		return false;
	}	
    File file;
	if(!file.open(fileName.c_str(), IO_MODE_RDWR_ONLY))
	{
		return false;
	}
	int lineNum=0;
	while(1)
	{
		string lineString="";
		ret=file.readLine(lineString);
		if(STATUS_ERROR==ret)
		{
			TRACE_ERR_CLASS("readLine error.\n");
            file.close();
			return false;
		}
		else if(0==ret)
		{
			break;
		}
        string keyword="";
		string comment="";
        KVValue* value = new KVValue();
		parseLine(lineString,keyword,*value,comment);
        #if 0
        comment = Utils::trimBeginWith(comment," ;");
        TRACE_DEBUG_CLASS("parseLine:%s  -->keyword:%s,value:%s,comment:%s\n",
            lineString.c_str(),keyword.c_str(),value->serialize().c_str(),comment.c_str());
        #endif
		if(!keyword.empty())
		{
			//m_propertyTable.insert(make_pair<string,KVValue*>(keyword,value));
            pair<string,KVValue*> propertyPair(keyword,value);
            m_propertyTable.insert(propertyPair);
		}
        else
        {
            delete value;
        }
		//m_lineTable.insert(make_pair<int,string>(lineNum,lineString));
		pair<int,string> linePair(lineNum,lineString);
        m_lineTable.insert(linePair);
		lineNum++;
	}
	return true;
}

/**
 *  \brief  保存属性表,此操作会重写并保存属性文件
 *  \param  fileName 属性文件名
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool KVProperty::saveAsFile(const string fileName)
{
    if(fileName.empty())
	{
		TRACE_ERR_CLASS("File name is empty.\n");
		return false;
	}
	AutoLock lock(&m_propertyLock);

    File file;
	if(file.open(fileName.c_str(), IO_MODE_REWR_ORNEW)==STATUS_ERROR)
	{
		TRACE_ERR_CLASS("reopen setting file failed.\n");
		return false;
	}
	PropertyMap::iterator propertyIter;
	LineMap::iterator lineIter;
	for(lineIter=m_lineTable.begin();lineIter!=m_lineTable.end();lineIter++)
	{
		std::string lineString;
		std::string keyword;
		std::string comment;
        KVValue kvvalue;
		parseLine(lineIter->second,keyword,kvvalue,comment);
        comment = Utils::trimBeginWith(comment," ;");
		if(!keyword.empty())
		{
			propertyIter=m_propertyTable.find(keyword);
			if(propertyIter!=m_propertyTable.end())
			{
				lineString = keyword;
				lineString += " = ";
				lineString += (propertyIter->second)->serialize();/* value值需取最新的值 */
                lineString += "  ;";
				if(!comment.empty())
				{
					lineString += comment;
				}
				lineString += "\n";
			    if(STATUS_ERROR==file.writeData(lineString.c_str(), lineString.size()))
                {
                    return false;
                }         
				//TRACE_DEBUG("write line:%s",lineStr.c_str());
			}
		}
		else
		{
			lineString = ";"+Utils::trimBeginWith(lineIter->second," ;");
			lineString += "\n";
			if(STATUS_ERROR==file.writeData(lineString.c_str(), lineString.size()))
            {
                return false;
            }         
			//TRACE_DEBUG("write line:%s",lineStr.c_str());
		}		
	}
    if(!file.close())
    {
        return false;
    }
	return true;
}

/**
 *  \brief  []运算符重载
 *  \param  keyword 属性名称
 *  \return 返回属性值
 */
KVValue& KVProperty::operator[](string keyword)
{
    if (keyword.empty())
    {
        return m_defaultValue;
    }
    AutoLock lock(&m_propertyLock);
	PropertyMap::iterator iter = m_propertyTable.find(keyword);
	if(iter!=m_propertyTable.end())
	{
		return *(iter->second);
	}
    else
    {
        return m_defaultValue;
    }
}
/**
 *  \brief  增加属性行
 *  \param  keyword 属性名称
 *  \param  value 属性值
 *  \return 成功返回true,失败返回false
 */
bool KVProperty::addProperty(string keyword,KVValue& value)
{
    AutoLock lock(&m_propertyLock);
	PropertyMap::iterator iter = m_propertyTable.find(keyword);
    if(iter!=m_propertyTable.end())
    {
        return false;
    }
    else
    {
        KVValue* kv = new KVValue();
        switch(value.baseType())
        {
            case BASETYPE_INT:
                (*kv)=value.toInt();
                break;
            case BASETYPE_DOUBLE:
                (*kv)=value.toDouble();
                break;
            case BASETYPE_STRING:
                (*kv)=value.toString();
                break;
            case BASETYPE_INTARRAY:
                (*kv)=value.toIntArray();
                break;
            case BASETYPE_DOUBLEARRAY:
                (*kv)=value.toDoubleArray();
                break;
            case BASETYPE_STRINGARRAY:
                (*kv)=value.toStringArray();
                break;
            case BASETYPE_TUPLE:
                (*kv)=value.toTuple();
                break;
            default:
                delete kv;
                return false;
        }
        pair<string,KVValue*> propertyPair(keyword,kv);
        m_propertyTable.insert(propertyPair);
        string lineString=keyword;
        lineString += " = ";
        lineString += value.serialize();
        lineString += ";";
        pair<int,string> linePair(m_lineTable.size(),lineString);
        m_lineTable.insert(linePair);
        return true;
    }
}

/***属性行格式
 *  version = "v1.0.1.1"  ;版本号(注释)
 *  serverip = "192.168.100.100" ;服务器ip
 *  serverport = <8080> ;服务器端口
 *  intArray = [-2,-1,0,1,2,3];整形数组
 *  strArray = ["Jim","Tom","Sim"];字符串数组
 *  tuple    = (1,"Jim",2,3,"Tom");元组
 */
void KVProperty::parseLine(const std::string lineString, std::string& keyword, KVValue& value, std::string& comment)
{
    keyword.clear();
    comment.clear();
    string tmpString = Utils::trimEndingBlank(lineString);
    int equalPos = tmpString.find("=");
    int commentPos = tmpString.find(";");
    if (equalPos==string::npos)/* 无等号 */
    {
        /* 整行都当做注释 */
        comment = tmpString;
        return ;
    }
    else 
    {
        if (commentPos!=string::npos)
        { 
            if (commentPos<equalPos)
            {
                /* 整行都当做注释 */
                comment = tmpString;
                return ;
            }
        }
    }
    keyword = Utils::trimEndingBlank(tmpString.substr(0,equalPos));
    tmpString = Utils::trimEndingBlank(tmpString.substr(equalPos+1));
    string valueString="";
    int len = tmpString.size();
    //TRACE_YELLOW("33333333---%s---tmp:%s\n",keyword.c_str(),tmpString.c_str());
    for(int i=0; i<len; i++)
    {
        char endCh;
        switch(tmpString[i])
        {
            case '\"': endCh='\"';break;
            case '<': endCh='>';break;
            case '[': endCh=']';break;
            case '(': endCh=')';break;
            default: comment = tmpString;break;/* 无值,整行都当做注释 */
        }
        int valueEnd = tmpString.find(endCh,i+1);
        if (valueEnd == string::npos) /* 无结束符,语法错误,整行都当做注释 */
        {   
            comment = tmpString;
            break;
        }
        valueString = tmpString.substr(i,valueEnd-i+1);
        comment = tmpString.substr(valueEnd+1);
        break;
    }
    if (!valueString.empty())
    {
        if(!value.initWithString(valueString))
        {
            value.clear();
        }
    }
    else
    {
        value.clear();  
    }
}