#include "Utils.h"
#include "Array.h"

#include <stdio.h>
#include <stdlib.h>


/**
 *  \brief  数组构造函数
 *  \param  none
 *  \return none
 */
Array::Array()
{
}
/**
 *  \brief  数组析构函数
 *  \param  none
 *  \return none
 */
Array::~Array()
{
}

int Array::type()
{
    return m_type;
}

/**
 *  \brief  整型数组构造函数
 *  \param  none
 *  \return none
 */
IntArray::IntArray()
{
    m_type = BASETYPE_INTARRAY;
    m_array.clear();
}
/**
 *  \brief  整型数组析构函数
 *  \param  none
 *  \return none
 */
IntArray::~IntArray()
{
    m_array.clear();
}

/**
 *  \brief  从格式化的字符串中初始化数组
 *  \param  arrayString 格式化字符串,例如:[0,1,2,3]
 *  \return 成功返回true,失败返回false
 */
bool IntArray::initWithString(const string & arrayString)
{
    m_array.clear();
    vector<string> strArray = Utils::cutString(arrayString, "[", "]", ",");
    int arraySize = strArray.size();
    if (arraySize>0)
    {
        for(int i=0; i<arraySize; i++)
        {
            m_array.push_back(atoi(strArray[i].c_str()));
        }
        return true;
    }
    return false;
}

/**
 *  \brief  序列化字符串数组
 *  \param  none
 *  \return 返回格式化字符串:[0,1,2]
 */
string IntArray::serialize()
{
    int arraySize = m_array.size();
    string arrayString="[";
    for(int i=0; i<arraySize; i++)
    {
        char buf[32]={0};
        sprintf(buf,"%d",m_array[i]);
        arrayString += buf;
        if (i<(arraySize-1))
        {
            arrayString += ",";
        }
    }
    arrayString += "]";
    return arrayString;
}

/**
 *  \brief  获取数组大小
 *  \param  none
 *  \return 返回数组大小(元素个数)
 */
int IntArray::size()
{
    return m_array.size();
}

/**
 *  \brief  清空数组
 *  \param  none
 *  \return none
 */
void IntArray::clear()
{
    m_array.clear();
}

/**
 *  \brief  []运算符重载,获取数组元素
 *  \param  idx
 *  \return 返回索引为idx的数组元素
 */
int& IntArray::operator[](int idx)
{
    return m_array[idx];
}

/**
 *  \brief  <<运算符重载,实现给数组添加元素
 *  \param  none
 *  \return 返回数组
 */
IntArray& operator<<(IntArray& array,int value)
{
    array.m_array.push_back(value);
    return array;
}

/**
 *  \brief  浮点型数组构造函数
 *  \param  none
 *  \return none
 */
DoubleArray::DoubleArray()
{
    m_type=BASETYPE_DOUBLEARRAY;
    m_array.clear();
}
/**
 *  \brief  浮点型数组析构函数
 *  \param  none
 *  \return none
 */
DoubleArray::~DoubleArray()
{
    m_array.clear();
}

/**
 *  \brief  从格式化的字符串中初始化数组
 *  \param  arrayString 格式化字符串,例如:[0.123,1.234,2.345]
 *  \return 成功返回true,失败返回false
 */
bool DoubleArray::initWithString(const string & arrayString)
{
    m_array.clear();
    vector<string> strArray = Utils::cutString(arrayString, "[", "]", ",");
    int arraySize = strArray.size();
    if (arraySize>0)
    {
        for(int i=0; i<arraySize; i++)
        {
            m_array.push_back(atof(strArray[i].c_str()));
        }
        return true;
    }
    return false;
}

/**
 *  \brief  序列化字符串数组
 *  \param  none
 *  \return 返回格式化字符串:[0.123,1.234,2.345]
 */
string DoubleArray::serialize()
{
    int arraySize = m_array.size();
    string arrayString="[";
    for(int i=0; i<arraySize; i++)
    {
        char buf[32]={0};
        sprintf(buf,"%lf",m_array[i]);
        arrayString += buf;
        if (i<(arraySize-1))
        {
            arrayString += ",";
        }
    }
    arrayString += "]";
    return arrayString;
}

/**
 *  \brief  获取数组大小
 *  \param  none
 *  \return 返回数组大小(元素个数)
 */
int DoubleArray::size()
{
    return m_array.size();
}


/**
 *  \brief  清空数组
 *  \param  none
 *  \return none
 */
void DoubleArray::clear()
{
    m_array.clear();
}

/**
 *  \brief  []运算符重载,获取数组元素
 *  \param  idx
 *  \return 返回索引为idx的数组元素
 */
double& DoubleArray::operator[](int idx)
{
    return m_array[idx];
}

/**
 *  \brief  <<运算符重载,实现给数组添加元素
 *  \param  none
 *  \return 返回数组
 */
DoubleArray& operator<<(DoubleArray& array,double value)
{
    array.m_array.push_back(value);
    return array;
}

/**
 *  \brief  字符串数组构造函数
 *  \param  none
 *  \return none
 */
StringArray::StringArray()
{
    m_type = BASETYPE_STRINGARRAY;
    m_array.clear();
}
/**
 *  \brief  字符串数组析构函数
 *  \param  none
 *  \return none
 */
StringArray::~StringArray()
{
    m_array.clear();
}

/**
 *  \brief  从格式化的字符串中初始化数组
 *  \param  arrayString 格式化字符串,例如:["Jim","Tom","Kim"]
 *  \return 成功返回true,失败返回false
 */
bool StringArray::initWithString(const string & arrayString)
{
    m_array.clear();
    string tmpString = Utils::trimEndingBlank(arrayString);
    int strLen = tmpString.size();
    
    if (strLen<2 ||
        tmpString[0]!='[' ||
        tmpString[strLen-1]!=']')
    {
        return false;
    }
    int quotNum=0;
    int commaNum=0;
    string item;
    for(int i=1; i<strLen-1; i++)
    {
        if (tmpString[i]=='\"')
        {
            quotNum++;
            if (quotNum%2==1)
            {
                item = "";
                commaNum = 0;
            }
            else
            {
                m_array.push_back(item);
            }
            continue;
        }
        else
        {
            if (quotNum%2==0)
            {
                if (tmpString[i]==' ')
                {
                    continue;
                }
                else if (tmpString[i]==',')
                {
                    commaNum++;
                    if (commaNum>1)
                    {
                        item="";
                        m_array.push_back(item);
                    }
                }
                else
                {
                    m_array.clear();
                    return false;
                }
            }
            else
            {
                item.append(1,tmpString[i]);                  
            }
        }
    }
    if (quotNum%2==1)
    {
        m_array.clear();
        return false; 
    }
    return true;
}

/**
 *  \brief  序列化字符串数组
 *  \param  none
 *  \return 返回格式化字符串:["a","b","c"]
 */
string StringArray::serialize()
{
    int arraySize = m_array.size();
    string arrayString="[";
    for(int i=0; i<arraySize; i++)
    {
        arrayString += "\"";
        arrayString += m_array[i];
        arrayString += "\"";
        if (i<(arraySize-1))
        {
            arrayString += ",";
        }
    }
    arrayString += "]";
    return arrayString;
}
/**
 *  \brief  获取数组大小
 *  \param  none
 *  \return 返回数组大小(元素个数)
 */
int StringArray::size()
{
    return m_array.size();
}

/**
 *  \brief  清空数组
 *  \param  none
 *  \return none
 */
void StringArray::clear()
{
    m_array.clear();
}

/**
 *  \brief  []运算符重载,获取数组元素
 *  \param  idx
 *  \return 返回索引为idx的数组元素
 */
string& StringArray::operator[](int idx)
{
    return m_array[idx];
}

/**
 *  \brief  <<运算符重载,实现给数组添加元素
 *  \param  none
 *  \return 返回数组
 */
StringArray& operator<<(StringArray& array,string value)
{
    array.m_array.push_back(value);
    return array;
}
