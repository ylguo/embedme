/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "BaseType.h"
#include "Tracer.h"
#include "PortAdapter.h"
#include "Timer.h"

#include <string.h>

PortAdapter::PortAdapter():
m_device(NULL)
{
}

PortAdapter::~PortAdapter()
{
}
/**
 *  \brief  绑定端口设备
 *  \param  device IO设备
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int PortAdapter::bind(IODevice * device,int bufSize)
{
    if (device==NULL || bufSize<=0)
    {
        TRACE_ERR_CLASS("parameter error!\n");
        return STATUS_ERROR;
    }
    if (m_device!=NULL)
    {
        TRACE_ERR_CLASS("port has allready binded device!\n");
        return STATUS_ERROR;
    }
    m_device = device;
    m_recvBuf = new char[bufSize];
    m_bufSize = bufSize;
 
    /* 开启接收线程 */
    m_mainThread.start(this);
    return STATUS_OK;
}
/**
 *  \brief  解绑端口设备
 *  \param  none
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int PortAdapter::unbind()
{
    if (m_device==NULL)
    {
        TRACE_ERR_CLASS("port bind no device yet,please bind first!\n");
        return STATUS_ERROR;
    }
    m_mainThread.cancel();
    delete [] m_recvBuf;
    m_recvBuf = NULL;
    return STATUS_OK;
}
/* 主线程:负责从设备读取数据 */
void PortAdapter::run()
{
    while(1)
    {
        memset(m_recvBuf,0,m_bufSize);
        int ret = m_device->readData(m_recvBuf, m_bufSize);
        if(ret>0)
        {
            recievedData(m_recvBuf, ret);  
        }
        Thread::msleep(0);
    }
}
/**
 *  \brief  发送数据包
 *  \param  packet 数据包
 *  \return 成功返回发送的字节数,失败返回STATUS_ERROR
 *  \note   none
 */
int PortAdapter::sendData(const char * buf, int size)
{
    if(m_device==NULL)
    {
        TRACE_ERR_CLASS("port is not bind!\n");
        return STATUS_ERROR;
    }
    
    if (buf==NULL || size<=0)
    {
        TRACE_ERR_CLASS("Parameter error!\n");
        return STATUS_ERROR;
    }
    TRACE_HEX("port_send", buf, size);
    int ret = m_device->writeData(buf, size);
    if(ret!=size)
    {
        TRACE_ERR_CLASS("write erorr,size=%d,ret=%d\n",size,ret);
    }
    return ret;
}

