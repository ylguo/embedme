/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "BaseType.h"
#include "Tracer.h"
#include "ThreadPool.h"

#include <stdlib.h>

#define THREAD_COUNT_DEFAULT    5  /* 默认线程池线程个数 */

/**
 *  \brief  线程运行体构造函数
 *  \param  none
 *  \return none
 */
Runnable::Runnable()
{
}
/**
 *  \brief  线程运行体析构函数
 *  \param  none
 *  \return none
 */
Runnable::~Runnable()
{
}
/**
 *  \brief  判断运行体是否正在运行
 *  \param  none
 *  \return 正在运行返回true,停止运行返回false
 */
bool Runnable::isRunning()
{
    return m_isRunning;
}

/**
 *  \brief  线程池构造函数
 *  \param  none
 *  \return none
 */
ThreadPool::ThreadPool():
m_initFlag(false),
m_maxThreadCount(0),
m_usedThreadCount(0)
{
}
/**
 *  \brief  线程池析构函数
 *  \param  none
 *  \return none
 */
ThreadPool::~ThreadPool()
{
}
/**
 *  \class  ThreadInfo_S
 *  \brief  线程参数结构体 
 */
typedef struct{
    ThreadPool* m_pool;
    int m_index;
}ThreadInfo_S;

/**
 *  \brief  线程池初始化
 *  \param  maxThreadCount 最大线程个数
 *  \return 成功返回true,失败返回false
 */
bool ThreadPool::init(int maxThreadCount)
{
    if (m_initFlag)
    {
        return false;
    }
    if (maxThreadCount<=0)
    {
       maxThreadCount = THREAD_COUNT_DEFAULT;
    }
    m_maxThreadCount=0;
    m_threadVect.clear();
    for(int i=0; i<maxThreadCount; i++)
    {
        pthread_t threadID;
        ThreadInfo_S *threadInfo = (ThreadInfo_S*)malloc(sizeof(ThreadInfo_S));
        threadInfo->m_pool=this;
        threadInfo->m_index=i; 
    	if(0!=pthread_create(&threadID, NULL, threadEntry, threadInfo))
    	{
    	    TRACE_ERR("ThreadPool::createThread,create thread failed!\n");
    		return false;
    	}
        else
        {
            m_threadVect.push_back(NULL);
            m_maxThreadCount++;
        }
    }
    m_initFlag=true;
    return true;
}
/**
 *  \brief  启动线程
 *  \param  runnable 线程运行体
 *  \param  priority 线程运行优先级
 *  \return 成功返回true,失败返回false
 */
bool ThreadPool::start(Runnable* runnable, int priority)
{
    if (runnable==NULL)
    {
        return false;
    }
    AutoLock lock(&m_vectLock);
    if (m_usedThreadCount>=m_maxThreadCount)
    {
        return false;
    }
    for(int i=0; i<m_maxThreadCount; i++)
    {
        if (m_threadVect[i]==NULL)
        {
			runnable->m_isRunning=true;
            m_threadVect[i]=runnable;
	        return true; 
        }
    }
    return false; 
}
/**
 *  \brief  停止线程
 *  \param  runnable 线程运行体
 *  \return 成功返回true,失败返回false
 */
bool ThreadPool::cancel(Runnable* runnable)
{
    if (runnable==NULL)
    {
        return true;
    }
    AutoLock lock(&m_vectLock);
    for(int i=0; i<m_maxThreadCount; i++)
    {
        if (m_threadVect[i]==runnable)
        {
            m_threadVect[i]->m_isRunning=false;
            m_threadVect[i]=NULL;
            return true;
        }
    }
    return false;
}
/**
 *  \brief  返回当前线程池最大线程个数
 *  \param  none
 *  \return 线程个数
 */
int ThreadPool::maxThreadCount()
{
    return m_maxThreadCount;
}
/**
 *  \brief  返回当前可用线程个数
 *  \param  none
 *  \return 线程个数
 */
int ThreadPool::idleThreadCount()
{
    return m_maxThreadCount-m_usedThreadCount;
}

void ThreadPool::execRunnable(int index)
{
    if (index<m_maxThreadCount)
    {
        m_vectLock.lock();
        Runnable* runnable = m_threadVect[index];
        if (runnable!=NULL)
        {
            m_usedThreadCount++;
            m_vectLock.unLock();
            runnable->run();
            /* 线程使用完毕,正常退出 */
            runnable->m_isRunning=false;
            m_vectLock.lock();
            m_threadVect[index]=NULL;
            m_usedThreadCount--;
            m_vectLock.unLock();
        }
        else
        {
            m_vectLock.unLock();
        }
        
    }
}
/* 线程入口点 */
void * ThreadPool::threadEntry(void *arg)
{
    ThreadInfo_S* threadInfo = (ThreadInfo_S*)arg;
    ThreadPool* threadPool = threadInfo->m_pool;
    int threadIndex = threadInfo->m_index;
	if(threadPool!=NULL && threadIndex>=0)
	{
	    while(1)
	    {
	        threadPool->execRunnable(threadIndex);
            Thread::msleep(100);
        }
	}
    free(threadInfo);
    threadInfo = NULL;
	return (void*)0;
}


