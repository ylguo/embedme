/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "Logger.h"
#include "Tracer.h"

#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <dirent.h>

#include <list>

#include "log4z.h"
using namespace zsummer::log4z;

#define LOG_ID_MAX              (0xFFFF)        /**< 日志ID最大数 */
#define LOG_LENGTH_MAX		    1024		    /**< 每条日志最大长度 */

#define CORELOG_PATH_DEFAULT    "./corelog/"    /**< 日志目录  */
#define CORELOG_NAME_DEFAULT    ".log"          /**< 日志文件后缀名 */
#define CORELOG_FILENUM_MAX     100             /**< 日志文件个数:100 */
#define CORELOG_LINES_MAX       100000          /**< 最大日志行数--10万行 */

#define LOG4ZLOG_MAINLOGGER_PATH_DEFAULT      "./mainlog/"       /**< 主日志目录  */
#define LOG4ZLOG_MAINLOGGER_NAME_DEFAULT      "mainlog"          /**< 主日志名称  */
#define LOG4ZLOG_FILESIZE_MAX      10              /**< 每个日志文件最大为10M */
#define LOG4ZLOG_STORESIZE_MAX     1024            /**< 所有日志最大所占空间:1024M */

/**
 *  \brief  CoreLogger构造函数
 *  \param  none
 *  \return none
 */
CoreLogger::CoreLogger():
m_logfp(NULL),
m_lineNum(0),
m_logFileName("")
{
}
/**
 *  \brief  CoreLogger析构函数
 *  \param  none
 *  \return none
 */
CoreLogger::~CoreLogger()
{
}

bool CoreLogger::initWithDefault()
{
    m_logPath = CORELOG_PATH_DEFAULT;
    m_logName = CORELOG_NAME_DEFAULT;
    m_logFileNumMax = CORELOG_FILENUM_MAX;
    startLog();
    return true;
}

bool CoreLogger::initWithSettings(Settings settings)
{
    m_logPath = settings["logPath"].toString();
    if (m_logPath.empty())
    {
        m_logPath = CORELOG_PATH_DEFAULT; 
    }
    m_logName = settings["logFileName"].toString();
    if (m_logName.empty())
    {
        m_logName = CORELOG_NAME_DEFAULT;
    }
    m_logFileNumMax = settings["logFileNumMax"].toInt();
    if (m_logFileNumMax<=0 || m_logFileNumMax>=CORELOG_FILENUM_MAX)
    {
        m_logFileNumMax = CORELOG_FILENUM_MAX;
    }
    startLog();
    return true;
}

void CoreLogger::startLog()
{
    AutoLock lock(&m_logLock);
    if (!m_isStarted)
    {
        char logFileName[128] = {0};
        time_t timep;
        struct tm * ptm = NULL;
        time(&timep);

        std::string dir = m_logPath;
    	if (dir.empty())
    	{
    		dir = CORELOG_PATH_DEFAULT;
    	}
    	m_logPath = dir + "/";

        /* 拼接文件名 */
        ptm = localtime(&timep);
        sprintf(logFileName, "%d%02d%02d_%02d%02d%02d%s",ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, \
                ptm->tm_hour, ptm->tm_min, ptm->tm_sec,m_logName.c_str());
        m_logFileName = logFileName;
    	
    	std::string logFile = m_logPath+m_logFileName;
        if (m_logfp != NULL)
        {
            fclose(m_logfp);
            m_logfp = NULL;
        }

        /* 先清除旧日志文件 */
        if (!clearLog(m_logFileNumMax- 1)) 
        {
            TRACE_ERR_CLASS("clear log failed.\n");
            return;
        }

        /* 创建新文件,如果有同名文件会覆盖掉原来文件 */
        m_logfp = fopen(logFile.c_str(), "w+");
        if (m_logfp == NULL)
        {
            TRACE_ERR_CLASS("Can't open log file:%s.\n", logFile.c_str());
            return;
        }
        m_lineNum = 0;
        m_isStarted = true;
    }
}

void CoreLogger::stopLog()
{
    AutoLock lock(&m_logLock);
    if (m_isStarted)
    {
        if(m_logfp!=NULL)
        {
            fclose(m_logfp);
            m_logfp = NULL;
        }
        m_isStarted = false;
    }
}

void CoreLogger::makeLog(const char * format, va_list argp)
{
    AutoLock lock(&m_logLock);
    if (m_isStarted)
    {
        char buf[LOG_LENGTH_MAX] = {0};
        char headBuf[64] = {0};
        struct tm * ptm;
        int nsize = 0;
        if (NULL == m_logfp)
        {
            TRACE_ERR_CLASS("Logger not init.\n");
            return;
        }

        if (0 == (m_lineNum % CORELOG_LINES_MAX))
        {
            /* 已达到最大条数,从文件头开始写 */
            fseek(m_logfp, 0, SEEK_SET);
        }

        time_t timep;
        time(&timep);
        ptm = localtime(&timep);
        sprintf(headBuf, "<L%d> [%d-%02d-%02d %02d:%02d:%02d]:", m_lineNum, \
                ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
        memcpy(buf, headBuf, strlen(headBuf));

        nsize = vsprintf(buf + strlen(headBuf), format, argp);
        fwrite(buf, 1, strlen(headBuf) + nsize, m_logfp);
        fwrite("\n",1,1,m_logfp);
        fflush(m_logfp);
        m_lineNum++;
    }
}

bool CoreLogger::clearLog(int maxLogFiles)
{
    std::list<std::string> logFileList;
    std::string fileName;
    DIR * dirp = NULL;
    struct dirent * dp = NULL;

    dirp = opendir(m_logPath.c_str());
    if (NULL == dirp)
    {
        if (ENOENT == errno)	/* 不存在则创建日志目录,并重新打开 */
        {
            std::string cmd = "mkdir -p ";
            cmd += m_logPath;
            system(cmd.c_str());
            dirp = opendir(m_logPath.c_str());
            if (NULL == dirp)
            {
                TRACE_ERR_CLASS("reopen dir:[%s] error:%s.\n", m_logPath.c_str(), ERROR_STRING);
                return false;
            }
        }
        else
        {
            TRACE_ERR_CLASS("open dir:[%s] error:%s.\n", m_logPath.c_str(), ERROR_STRING);
            return false;
        }
    }

    /* 扫描日志文件夹下所有文件 */
    while ((dp = readdir(dirp)) != NULL)
    {
        fileName = dp->d_name;

        if (0 == fileName.compare(".") ||
            0 == fileName.compare(".."))
        {
            continue;
        }
        int len = fileName.size();
        std::string tmpStr;
        if (len>=m_logName.size())
        {
            tmpStr = fileName.substr(len-m_logName.size(),m_logName.size());
        }
        else
        {
            tmpStr = fileName;
        }
        if ((0 == tmpStr.compare(0, m_logName.size(), m_logName)) &&
            (fileName.size() == (m_logFileName.size())))
        {
            /* 如果log文件名匹配且长度匹配,则记录文件名 */
            logFileList.push_back(fileName);
        }
        else
        {
            /* 不匹配则删除该文件 */
            std::string cmd = "rm -rf ";
            cmd += m_logPath;
            cmd += fileName;
            system(cmd.c_str());
        }
    }
    closedir(dirp);

    /* 找出要保留的最近maxLogfiles个文件,其他的删除 */
    int i = 0;
    int listSize = logFileList.size();
    logFileList.sort();	/* 按文件名升序排序 */
    std::list<std::string>::iterator iter = logFileList.begin();
    for (; iter != logFileList.end(); iter++, i++)
    {
        if ((listSize > maxLogFiles) &&
                (i < listSize - maxLogFiles))
        {
            std::string cmd = "rm -rf ";
            cmd += m_logPath;
            cmd += *iter;
            system(cmd.c_str());
        }
    }
    return true;
}
/**
 *  \brief  Log4zLogger构造函数
 *  \param  none
 *  \return none
 */
Log4zLogger::Log4zLogger():
m_isStarted(false)
{
    m_loggerVect.clear();
}
/**
 *  \brief  Log4zLogger析构函数
 *  \param  none
 *  \return none
 */
Log4zLogger::~Log4zLogger()
{
}

bool Log4zLogger::initWithDefault()
{
    AutoLock lock(&m_logLock);
    if (!m_isStarted)
    {
        LoggerInfo_S mainLogger;
        mainLogger.m_id = LOG4Z_MAIN_LOGGER_ID;
        mainLogger.m_name = LOG4ZLOG_MAINLOGGER_NAME_DEFAULT;
        mainLogger.m_path = LOG4ZLOG_MAINLOGGER_PATH_DEFAULT; 
        m_loggerVect.push_back(mainLogger);

        ILog4zManager* log4z = ILog4zManager::getInstance();
        log4z->enableLogger(mainLogger.m_id, true);
        log4z->setLoggerPath(mainLogger.m_id, LOG4ZLOG_MAINLOGGER_PATH_DEFAULT);
        log4z->setLoggerDisplay(mainLogger.m_id, true);                      /* 显示日志信息 */
        log4z->setLoggerFileLine(mainLogger.m_id, false);                    /* 不显示文件及行数 */
        log4z->setLoggerLevel(mainLogger.m_id, LOG_LEVEL_TRACE);             /* 日志级别 */
        log4z->setLoggerLimitsize(mainLogger.m_id, LOG4ZLOG_FILESIZE_MAX);   /* 最大文件大小 */
        log4z->setLoggerMonthdir(mainLogger.m_id, true);                     /* 是否按月份新建文件夹 */
        log4z->start();
        m_isStarted=true;
        return true;
    }
    else
    {
        return false;
    }
}

bool Log4zLogger::initWithSettings(Settings settings)
{
    AutoLock lock(&m_logLock);
    if (!m_isStarted)
    {
        m_logPath = settings["logPath"].toString();
        if (m_logPath.empty())
        {
            m_logPath = LOG4ZLOG_MAINLOGGER_PATH_DEFAULT; 
        }
        
        m_logFileSizeMax = settings["logFileSizeMax"].toInt();
        if (m_logFileSizeMax<=0 || m_logFileSizeMax>=LOG4ZLOG_FILESIZE_MAX)
        {
            m_logFileSizeMax = LOG4ZLOG_FILESIZE_MAX;
        }

        m_logStoreSizeMax = settings["logStoreMax"].toInt();
        if (m_logStoreSizeMax<=0 || m_logStoreSizeMax>=LOG4ZLOG_STORESIZE_MAX)
        {
            m_logStoreSizeMax = LOG4ZLOG_STORESIZE_MAX;
        }
        m_monthDirEnable = !!(settings["monthDirEnable"].toInt());

        /* 主日志 */
        LoggerInfo_S logger;
        logger.m_id = LOG4Z_MAIN_LOGGER_ID;
        logger.m_name = LOG4ZLOG_MAINLOGGER_NAME_DEFAULT;
        logger.m_path = m_logPath;
        m_loggerVect.push_back(logger);

        /* 子日志 */
        int logListSize=settings["loglist"].size();
        for(int i=0; i<logListSize; i++)
        {
            std::string key=settings["loglist"][i]["name"].toString();
            if (key.empty())
            {
                continue;
            }
            std::string path = settings["loglist"][i]["path"].toString();
            if (path.empty())
            {
                path = m_logPath+"/"+key;
            }
            else if (path[0]!='/')
            {
                path = m_logPath+"/"+path;
            }
            
            logger.m_id = i+1;
            logger.m_name = key;
            logger.m_path = path;
            
            m_loggerVect.push_back(logger);
        }

        ILog4zManager* log4z = ILog4zManager::getInstance();
        for(int i=0; i<m_loggerVect.size(); i++)
        {
            if (i!=0)
            {
                log4z->createLogger(m_loggerVect[i].m_name.c_str());
            }
            log4z->enableLogger(m_loggerVect[i].m_id, true);
            log4z->setLoggerPath(m_loggerVect[i].m_id, m_loggerVect[i].m_path.c_str());
            log4z->setLoggerDisplay(m_loggerVect[i].m_id, true);              /* 显示日志信息 */
            log4z->setLoggerFileLine(m_loggerVect[i].m_id, false);            /* 不显示文件及行数 */
            log4z->setLoggerLevel(m_loggerVect[i].m_id, LOG_LEVEL_TRACE);     /* 日志级别 */
            log4z->setLoggerLimitsize(m_loggerVect[i].m_id, m_logFileSizeMax);/* 最大文件大小 */
            log4z->setLoggerMonthdir(m_loggerVect[i].m_id, m_monthDirEnable); /* 是否按月份新建文件夹 */
        }
        
        log4z->start();
        m_isStarted=true;
        return true;
    }
    else
    {
        return false;
    }
}

int Log4zLogger::findLog(std::string key)
{
    AutoLock lock(&m_logLock);
    for(int i=0; i<m_loggerVect.size(); i++)
    {
        if (m_loggerVect[i].m_name == key)
        {
            return m_loggerVect[i].m_id;
        }
    }
    return LOG_ID_INVALID;
}


void Log4zLogger::makeLog(int logid,const char * format, va_list argp)
{
    AutoLock lock(&m_logLock);
    for(int i=0;i<m_loggerVect.size();i++)
    {
        if (m_loggerVect[i].m_id==logid)
        {
             if (m_isStarted)
            {
                char buf[LOG_LENGTH_MAX] = {0};
                vsprintf(buf, format, argp);
                buf[LOG_LENGTH_MAX-1]=0;
                LOG_STREAM(logid,LOG_LEVEL_TRACE,"",0,buf);
            }
        }
    }
}

/**
 *  \brief  LoggerManager构造函数
 *  \param  none
 *  \return none
 */
LoggerManager::LoggerManager()
{
}
/**
 *  \brief  LoggerManager析构函数
 *  \param  none
 *  \return none
 */
LoggerManager::~LoggerManager()
{ 
}
/**
 *  \brief  初始化日志管理器
 *  \param  settings 配置信息
 *  \return 成功返回true,失败返回false
 *  \note   settings为Settings对象.
 */
bool LoggerManager::initWithSettings(Settings logSettings)
{
    if (logSettings.isExist("core"))
    {
        CoreLogger::getInstance()->initWithSettings(logSettings["core"]);
    }
    else
    {
        CoreLogger::getInstance()->initWithDefault();
    }
    if (logSettings.isExist("main"))
    {
        Log4zLogger::getInstance()->initWithSettings(logSettings["main"]);
    }
    else
    {
        Log4zLogger::getInstance()->initWithDefault();
    }
    return true;
}

/**
 *  \brief  获取Logger
 *  \param  type 日志类型
 *  \param  key 日志名字关键字
 *  \return 成功返回日志id,失败返回LOG_ID_INVALID
 */
int LoggerManager::getLogger(int type, const char* key)
{
    int logid = LOG_ID_INVALID;
    if (LOG_TYPE_CORE==type)
    {
        logid = LOG_ID_CORE;
    }
    else if (LOG_TYPE_LOG4Z==type)
    {
        if (key==NULL || std::string(key)=="")
        {
            logid = LOG_ID_MAIN;
        }
        else
        {
            logid = Log4zLogger::getInstance()->findLog(key);  
        }
    }
    return logid;
}
/**
 *  \brief  记录日志
 *  \param  logid 日志id
 *  \param  fmt 参数
 *  \return void
 *  \note   none
 */
void LoggerManager::log(int logid,const char * fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    if (logid==LOG_ID_CORE)
    {    
        CoreLogger::getInstance()->makeLog(fmt, argp);
    }
    else if (logid>=LOG_ID_MAIN && logid<=(LOG_ID_MAIN+LOG_ID_MAX))
    {
        Log4zLogger::getInstance()->makeLog(logid, fmt,argp);
    }
    va_end(argp);
}