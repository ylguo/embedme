/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/

#include <iostream>
#include "Tracer.h"
#include "CmdExecuter.h"

/**
 *  \brief  ִ�����ȡ����������ַ���.
 *  \param  cmd �����ַ���.
 *  \param  result �����������.
 *  \param  len ����������泤��.
 *  \return �ɹ�����STATUS_OK,ʧ�ܷ���STATUS_ERROR.
 */
int CmdExecuter::execute(std::string cmd, char * result, int len)
{
	int ret;
	FILE* fp=NULL;
	fp=popen(cmd.c_str(),"r");
	if (NULL==fp)
	{
		TRACE_ERR("CmdExecuter::execute(%s), popen failed.\n",cmd.c_str());
		return STATUS_ERROR;
	}

	/* ��ȡ����ִ�н�� */
	memset(result,0,len);
	ret=fread(result, 1, len, fp);
	if (ret<=0)
	{
		TRACE_ERR("CmdExecuter::execute(%s),fread error,len=%d,ret=%d.\n",cmd.c_str(),len,ret);
	}
	result[len-1]=0;
	//TRACE_TEXT("%s",result);
	pclose(fp);
	return (ret>0)?(STATUS_OK):(STATUS_ERROR);
}