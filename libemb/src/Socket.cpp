/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "Socket.h"
#include "Tracer.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/ioctl.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

#include <iostream>

Socket::Socket():
m_localAddr(""),
m_sockfd(-1)
{
	m_localAddr = "0.0.0.0";
	m_localPort = 0;
}
Socket::~Socket()
{
}

/**
 *  \brief  关闭socket
 *  \param  void
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool Socket::close()
{
    if (m_sockfd > 0)
    {
        ::close(m_sockfd);
    }
    m_sockfd = -1;
    return true;
}
/**
 *  \brief  绑定socket到指定IP地址的端口上
 *  \param  localPort 要绑定的端口
 *  \param  localAddr 绑定的IP地址,不指定则为默认本机任意地址
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int Socket::bind(uint16 localPort, std::string localAddr)
{
    int ret = -1;
    if (m_sockfd < 0)
    {
        TRACE_ERR_CLASS("socket is not open\n");
        return STATUS_ERROR;
    }

	m_localAddr = localAddr;
    m_localPort = localPort;
	
#if 0
    /* 默认设置socket为非阻塞方式读写 */
    int flags = fcntl(m_sockfd, F_GETFL);
    if (flags < 0)
    {
        TRACE_ERR_CLASS("fcntl error.\n");
        return -1;
    }
    else
    {
        flags |= O_NONBLOCK;
        ret = fcntl(m_sockfd, F_SETFL, flags);
        if (ret < 0)
        {
            TRACE_ERR_CLASS("set flag error.\n");
        }
    }
#endif

    /*为TCP链接设置IP和端口等信息*/
    bzero(&m_localSockAddr, sizeof(m_localSockAddr));
    m_localSockAddr.sin_family = AF_INET;
    m_localSockAddr.sin_port = htons(m_localPort);

    /* 如果本机ip地址没指定,则设置本机任意地址 */
    if (m_localAddr.empty())
    {
        m_localSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    }
    else
    {
        m_localSockAddr.sin_addr.s_addr = inet_addr(m_localAddr.c_str());
    }
    /*地址与socket绑定bind*/
    ret = ::bind(m_sockfd, (struct sockaddr *)&m_localSockAddr, sizeof(m_localSockAddr));
    if (ret < 0)
    {
        TRACE_ERR_CLASS("bind(%s:%d) error:%s.\n", inet_ntoa(m_localSockAddr.sin_addr), 
						m_localPort, ERROR_STRING);
        return STATUS_ERROR;
    }

    TRACE_REL_CLASS("bind ok %s:%d\n", inet_ntoa(m_localSockAddr.sin_addr), m_localPort);

    return STATUS_OK;
}
/**
 *  \brief  检查socket是否已打开
 *  \param  void
 *  \return 打开返回true,关闭返回false
 *  \note   none
 */
int Socket::connectToHost(uint16 peerPort, std::string peerAddr)
{
    m_peerPort = peerPort;
    m_peerAddr = peerAddr;
	return STATUS_OK;
}


/**
 *  \brief  检查socket是否已打开
 *  \param  void
 *  \return 打开返回true,关闭返回false
 *  \note   none
 */
bool Socket::isAlive()
{
	return (m_sockfd>0)?true:false;
}

/**
 *  \brief  设置socket类型
 *  \param  socketType SOCKET_TYPE_E
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int Socket::setAttribute(int attr, int value)
{
	int level;	/* SOL_SOCKET,IPPROTO_TCP,IPPROTO_IP,IPPROTO_IPV6 */
	int option;
    int optionVal;
	int ret;
    switch(attr)
    {
        case SOCKET_ATTR_CASTTYPE:
        	if (value==SOCKET_BROADCAST)
        	{
        		level = SOL_SOCKET;
        		option = SO_BROADCAST;
        		optionVal = 1;
        	}
        	else if(value==SOCKET_MULTICAST)
        	{

        	}
        	else if(value==SOCKET_GROUPCAST)
        	{

        	}
        	else
        	{
        		TRACE_ERR_CLASS("Unsupport socket type:%d\n",value);
        		return STATUS_ERROR;
        	}
            break;
        case SOCKET_ATTR_RCVBUF:
        {   
            level = SOL_SOCKET;
    		option = SO_RCVBUF;
    		optionVal = value;
            break;
        }
        case SOCKET_ATTR_SNDBUF:
        {   
            level = SOL_SOCKET;
    		option = SO_SNDBUF;
    		optionVal = value;
            break;
        }
        default:
            TRACE_ERR_CLASS("Unsupport socket attr:%d\n",attr);
        	return STATUS_ERROR;
    }
    ret=setsockopt(m_sockfd, level, option, &optionVal, sizeof(optionVal));
	if (ret<0)
	{
		return STATUS_ERROR;
	}
	return STATUS_OK;
}

int Socket::getAttribute(int attr)
{
    int level;	/* SOL_SOCKET,IPPROTO_TCP,IPPROTO_IP,IPPROTO_IPV6 */
	int option;
    int optionVal;
    int optionLen;
	int ret;
    switch(attr)
    {
        case SOCKET_ATTR_RCVBUF:
        {   
            level = SOL_SOCKET;
    		option = SO_RCVBUF;
            break;
        }
        case SOCKET_ATTR_SNDBUF:
        {   
            level = SOL_SOCKET;
    		option = SO_SNDBUF;
            break;
        }
        default:
            TRACE_ERR_CLASS("Unsupport socket attr:%d\n",attr);
        	return STATUS_ERROR;
    }
    ret=getsockopt(m_sockfd, level, option, &optionVal, (socklen_t*)&optionLen);
	if (ret<0)
	{
		return STATUS_ERROR;
	}
    return optionVal;
}

UdpSocket::UdpSocket()
{
}
UdpSocket::~UdpSocket()
{
}
/**
 *  \brief  打开UDP socket
 *  \param  void
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool UdpSocket::open()
{
    if (m_sockfd < 0)
    {
        m_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (m_sockfd < 0)
        {
            TRACE_ERR_CLASS("open socket error:%s\n", ERROR_STRING);
            return false;
        }
    }
    return true;
}

/**
 *  \brief  关闭UDP socket
 *  \param  void
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool UdpSocket::close()
{
    return Socket::close();
}

/**
 *  \brief  UDP socket接收数据
 *  \param  buf 接收缓存
 *  \param  len 缓存大小
 *  \param  timeoutMs 超时时间,-1:阻塞,0:立即返回,>0:超时时间ms
 *  \param  peerAddr 对端IP地址
 *  \param  peerPort 对端端口
 *  \return 成功返回接收的数据长度,失败返回STATUS_ERROR
 *  \note   none
 */
int UdpSocket::readData(char * buf, int len, int timeoutMs)
{
    int ret;
    if (NULL==buf || len<=0)
    {
        TRACE_ERR_CLASS("param error.\n");
        return STATUS_ERROR;
    }
	if (m_sockfd<0)
	{
		TRACE_ERR_CLASS("socket not open.\n");
		return STATUS_ERROR;
	}

    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(m_sockfd, &readfds);
    
    struct timeval tv,*ptv;
    if (timeoutMs<0)
    {
        ptv=NULL;
    }
    else
    {
        tv.tv_sec = timeoutMs/1000;
        tv.tv_usec = (timeoutMs%1000)*1000;
        ptv = &tv;
    }
    
    ret = select(m_sockfd + 1, &readfds, NULL, NULL, ptv);
    if (0 == ret)
    {
        //TRACE_ERR_CLASS("recv timeout\n");
        return STATUS_ERROR;
    }
    else if ((ret < 0) && (errno != EINTR))
    {
        TRACE_ERR_CLASS("select error:%s.\n",ERROR_STRING);
        return STATUS_ERROR;
    }
    /* 检查fd是否可读 */
    if (!FD_ISSET(m_sockfd, &readfds))
    {
        TRACE_ERR_CLASS("readfds error.\n");
        return STATUS_ERROR;
    }

    struct sockaddr_in cliSockAddr;
    socklen_t addLen = sizeof(cliSockAddr);
    int recvLen = recvfrom(m_sockfd, buf, len, 0,
                              (struct sockaddr *)&cliSockAddr,
                              &addLen);
    if (recvLen < 0)
    {
        if (errno != EAGAIN)
        {
            TRACE_ERR_CLASS("socket read error:%s\n", ERROR_STRING);
            return STATUS_ERROR;
        }
    }
    m_peerAddr = inet_ntoa(cliSockAddr.sin_addr);
    m_peerPort = ntohs(cliSockAddr.sin_port);
    //TRACE_DEBUG_CLASS("UDP recvfrom(%s:%d)\n",m_peerAddr.c_str(),m_peerPort);
    return recvLen;
}

/**
 *  \brief  UDP socket发送数据
 *  \param  data 发送数据缓存
 *  \param  len 缓存大小
 *  \param  timeoutMs 超时时间,-1:阻塞,0:立即返回,>0:超时时间ms
 *  \param  peerAddr 对端IP地址
 *  \param  peerPort 对端端口
 *  \return 成功返回发送的数据长度,失败返回STATUS_ERROR
 *  \note   none
 */
int UdpSocket::writeData(const char * data, int len, int timeoutMs)
{
    int ret;
    if (m_peerAddr == "" || m_peerPort == 0)
    {
        TRACE_ERR_CLASS(".\n");
        return STATUS_ERROR;
    }
    if (NULL == data || len<=0)
    {
        TRACE_ERR_CLASS("param error.\n");
        return STATUS_ERROR;
    }
	
	if (m_sockfd<0)
	{
		TRACE_ERR_CLASS("socket not open.\n");
		return STATUS_ERROR;
	}

    fd_set writefds;
    FD_ZERO(&writefds);
    FD_SET(m_sockfd, &writefds);

    struct timeval tv,*ptv;
    if (timeoutMs<0)
    {
        ptv=NULL;
    }
    else
    {
        tv.tv_sec = timeoutMs/1000;
        tv.tv_usec = (timeoutMs%1000)*1000;
        ptv = &tv;
    }
    
    ret = select(m_sockfd + 1, NULL, &writefds, NULL, ptv);
    if (0==ret)
    {
        //TRACE_ERR_CLASS("send timeout\n");
        return STATUS_ERROR;
    }
	else if(ret<0)
	{
		TRACE_ERR_CLASS("select error:%s.\n",ERROR_STRING);
        return STATUS_ERROR;
	}
		
    /* 检查fd是否可写 */
    if (!FD_ISSET(m_sockfd, &writefds))
    {
        TRACE_ERR_CLASS("writefds error.\n");
        return STATUS_ERROR;
    }

    /* 防止在socket关闭后,write会产生SIGPIPE信号导致进程退出 */
    signal(SIGPIPE, SIG_IGN);

    struct sockaddr_in peerSockAddr;
    peerSockAddr.sin_family = AF_INET;
    peerSockAddr.sin_port = htons(m_peerPort);
    peerSockAddr.sin_addr.s_addr = inet_addr(m_peerAddr.c_str());

    //TRACE_DEBUG_CLASS("UDP sendto(%s:%d)\n",m_peerAddr.c_str(),m_peerPort);
    int sndLen = sendto(m_sockfd, data, len, 0,
                           (struct sockaddr *)&peerSockAddr,
                           sizeof(peerSockAddr));
    if (sndLen < 0)
    {
        TRACE_ERR_CLASS("write error:%s\n", ERROR_STRING);
        return STATUS_ERROR;
    }

    return sndLen;
}

TcpSocket::TcpSocket()
{
}
TcpSocket::~TcpSocket()
{
}
/**
 *  \brief  打开TCP socket
 *  \param  void
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool TcpSocket::open()
{
    if (m_sockfd < 0)
    {
        m_sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (m_sockfd < 0)
        {
            TRACE_ERR_CLASS("open socket error:%s\n", ERROR_STRING);
            return false;
        }
    }
    return true;
}

/**
 *  \brief  关闭TCP socket
 *  \param  void
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool TcpSocket::close()
{
    return Socket::close();
}
/**
 *  \brief  TCP socket接收数据
 *  \param  buf 接收缓存
 *  \param  len 缓存大小
 *  \param  timeoutMs 超时时间,-1:阻塞,0:立即返回,>0:超时时间ms
 *  \return 成功返回接收的数据长度,失败返回STATUS_ERROR
 *  \note   none
 */
int TcpSocket::readData(char * buf, int len,int timeoutMs)
{
    int ret;
    if (NULL == buf || len <= 0)
    {
        TRACE_ERR_CLASS("param error.\n");
        return STATUS_ERROR;
    }
	
	if (m_sockfd<0)
	{
		TRACE_ERR_CLASS("socket not open.\n");
		return STATUS_ERROR;
	}

    struct timeval tv,*ptv;
    fd_set readfds;
    if (timeoutMs<0)
    {
        ptv=NULL;
    }
    else
    {
        ptv = &tv;
        tv.tv_sec = timeoutMs/1000;
        tv.tv_usec = (timeoutMs%1000)*1000;
    }
    FD_ZERO(&readfds);
    FD_SET(m_sockfd, &readfds);
    ret = select(m_sockfd + 1, &readfds, NULL, NULL, ptv);
    if (0 == ret)
    {
        //TRACE_INFO_CLASS("recv timeout.\n");
        return STATUS_ERROR;
    }
    else if ((ret < 0) && (errno != EINTR))
    {
        TRACE_ERR_CLASS("select error:%s\n",ERROR_STRING);
        return STATUS_ERROR;
    }
    /* 检查fd是否可读 */
    if (!FD_ISSET(m_sockfd, &readfds))
    {
        TRACE_ERR_CLASS("readfds error\n");
        return STATUS_ERROR;
    }

    int recvLen = read(m_sockfd, buf, len);
    if (recvLen < 0)
    {
        if (errno == ECONNABORTED)
        {
            TRACE_ERR_CLASS("socket read error:%s\n", ERROR_STRING);
            return 0;
        }
        else if (errno != EAGAIN)
        {
            TRACE_ERR_CLASS("socket read error:%s\n", ERROR_STRING);
            return STATUS_ERROR;
        }
    }
    return recvLen;
}
/**
 *  \brief  TCP socket发送数据
 *  \param  data 发送数据缓存
 *  \param  len 缓存大小
 *  \param  timeoutMs 超时时间,-1:阻塞,0:立即返回,>0:超时时间ms
 *  \return 成功返回发送的数据长度,失败返回STATUS_ERROR
 *  \note   none
 */
int TcpSocket::writeData(const char* buf, int len,int timeoutMs)
{
    int ret;
    if ((NULL == buf) || len<=0)
    {
        TRACE_ERR_CLASS("param error.\n");
        return STATUS_ERROR;
    }
	
	if (m_sockfd<0)
	{
		TRACE_ERR_CLASS("socket not open.\n");
		return STATUS_ERROR;
	}

    struct timeval tv,*ptv;
    fd_set writefds;
    if (timeoutMs<0)
    {
        ptv=NULL;
    }
    else
    {
        ptv = &tv;
        tv.tv_sec = timeoutMs/1000;
        tv.tv_usec = (timeoutMs%1000)*1000;
    }
    FD_ZERO(&writefds);
    FD_SET(m_sockfd, &writefds);
    ret = select(m_sockfd + 1, NULL, &writefds, NULL, ptv);
    if (0==ret)
    {
        //TRACE_ERR_CLASS("send timeout.\n");
        return STATUS_ERROR;
    }
	else if(ret<0)
	{
		TRACE_ERR_CLASS("select error:%s\n",ERROR_STRING);
        return STATUS_ERROR;
	}
	
    /* 检查fd是否可写 */
    if (!FD_ISSET(m_sockfd, &writefds))
    {
        TRACE_ERR_CLASS("writefds error\n");
        return STATUS_ERROR;
    }

    /* 防止在socket关闭后,write会产生SIGPIPE信号导致进程退出 */
    signal(SIGPIPE, SIG_IGN);
    int sndLen = write(m_sockfd, buf, len);
    if (sndLen < 0)
    {
        TRACE_ERR_CLASS("write error:%s\n", ERROR_STRING);
        return STATUS_ERROR;
    }

    return sndLen;
}

/**
 *  \brief  TCP socket连接远端
 *  \param  port 对端端口
 *  \param  ipaddr 对端IP地址
 *  \return 成功返回接收的数据长度,失败返回STATUS_ERROR
 *  \note   none
 */
int TcpSocket::connectToHost(uint16 port, std::string ipaddr)
{
    int ret;
	struct sockaddr_in peerSockAddr;
    bzero(&peerSockAddr, sizeof(peerSockAddr));
    peerSockAddr.sin_family = AF_INET;
    peerSockAddr.sin_port = htons(port);
    if (ipaddr.empty())
    {
        TRACE_ERR_CLASS("peer ip addr not set\n");
        return STATUS_ERROR;
    }
    else
    {
        peerSockAddr.sin_addr.s_addr = inet_addr(ipaddr.c_str());
    }

    /*开始连接服务器connect*/
    TRACE_REL_CLASS("connect to %s:%d\n", inet_ntoa(peerSockAddr.sin_addr), ntohs(peerSockAddr.sin_port));
    int constatus = ::connect(m_sockfd, (struct sockaddr *)&peerSockAddr, sizeof(peerSockAddr));
    if (0 == constatus) //连接正常
    {
        struct timeval tv;
        fd_set writefds;

        tv.tv_sec = 5;
        tv.tv_usec = 0;

        FD_ZERO(&writefds);
        FD_SET(m_sockfd, &writefds);

        ret = select(m_sockfd + 1, NULL, &writefds, NULL, &tv);
        if (0 == ret)
        {
            //TRACE_ERR_CLASS("select timeout.\n");
            return STATUS_ERROR;
        }
		else if (ret < 0)
        {
            TRACE_ERR_CLASS("select error:%s.\n",ERROR_STRING);
            return STATUS_ERROR;
        }

        if (!FD_ISSET(m_sockfd, &writefds))
        {
            TRACE_ERR_CLASS("socket fd error.\n");
            return STATUS_ERROR;
        }
        TRACE_REL_CLASS("=========TcpSocket open success.\n");
        return STATUS_OK;
    }
    else
    {
        TRACE_ERR_CLASS("connect error:%s.\n", ERROR_STRING);
        if (errno != EINPROGRESS)
        {
            TRACE_ERR_CLASS("connect error.\n");
        }
    }
    return STATUS_ERROR;
}

/**
 *  \brief  TCP socket监听连接
 *  \param  maxpend 最大挂起连接数
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int TcpSocket::listenConnection(int maxpending)
{
    int ret;
    ret = ::listen(m_sockfd, maxpending);
    if (ret < 0)
    {
        TRACE_ERR_CLASS("listen error:%s.\n", ERROR_STRING);
        return STATUS_ERROR;
    }
    return STATUS_OK;
}

/**
 *  \brief  TCP socket接受连接
 *  \param  newsocket 接受的新连接
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int TcpSocket::acceptConnection(TcpSocket & newsocket)
{
    int ret;
    struct sockaddr_in client_sockaddr;
    int addr_size = sizeof(struct sockaddr_in);
    ret = ::accept(m_sockfd, (struct sockaddr *)&client_sockaddr, (socklen_t *)&addr_size);
    if (ret < 0)
    {
        TRACE_ERR_CLASS("accept error:%s\n", ERROR_STRING);
        sleep(3);
        return STATUS_ERROR;
    }
    newsocket.m_localAddr = m_localAddr;
    newsocket.m_localPort = m_localPort;
    newsocket.m_sockfd = ret;

    TRACE_REL_CLASS("tcp connection from %s:%d\n", inet_ntoa(client_sockaddr.sin_addr), ntohs(client_sockaddr.sin_port));
    return STATUS_OK;
}

UdpServer::UdpServer()
{
}

UdpServer::~UdpServer()
{
    m_serverSocket->close();
    delete m_serverSocket;
    m_serverSocket = NULL;
    delete m_clientSocket;
    m_clientSocket = NULL;
}

/**
 *  \brief  初始化服务器
 *  \param  serverPort 服务器端口
 *  \param  maxConAllows 服务器最多支持挂起连接数(仅TCP时有效)
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
bool UdpServer::initServer(uint16 serverPort,std::string serverIP,int maxPendings)
{
    TcpServer* tcpServer = dynamic_cast<TcpServer*>(this);
    if (tcpServer!=NULL)
    {
        m_serverSocket = new TcpSocket();
        m_clientSocket = new TcpSocket(); 
    }
    else
    {
        m_serverSocket = new UdpSocket();
        m_clientSocket = new UdpSocket();
    }

    if(STATUS_ERROR==m_serverSocket->open())
    {
        return false;
    }
    if (STATUS_ERROR==m_serverSocket->bind(serverPort,serverIP))
    {
        return false;
    }

    TcpSocket* serverSocket=dynamic_cast<TcpSocket*>(m_serverSocket);
    if (serverSocket!=NULL && serverSocket->listenConnection(maxPendings)==STATUS_ERROR)
    {
        return false;
    }
    
    return true;
}

/**
 *  \brief  启动服务器
 *  \param  void
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool UdpServer::startServer()
{
    m_mainThread.start(this);
    return true;
}
/**
 *  \brief  停止服务器
 *  \param  void
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool UdpServer::stopServer()
{
    m_mainThread.cancel();
    return true;
}

/**
 *  \brief  获取服务端Socket
 *  \param  void
 *  \return 返回服务端Socket
 *  \note   none
 */
Socket** UdpServer::getServerSocket()
{
    return &m_serverSocket;
}

/**
 *  \brief  获取客户端Socket
 *  \param  void
 *  \return 返回客户端Socket
 *  \note   none
 */
Socket** UdpServer::getClientSocket()
{
    return &m_clientSocket;
}

/**
 *  \brief  服务器接收数据方法
 *  \param  buf 接收缓存
 *  \param  len 缓存的大小
 *  \return 返回值ret大于0表示接收到ret个数据,等于0表示断开连接,小于0表示接收失败
 *  \note   默认为Socket的接收方法,用户可覆盖以定制自己的接收方法
 */
int UdpServer::recvData(char* buf, int len)
{
    return m_serverSocket->readData(buf, len,500);
}
/**
 *  \brief  服务器发送数据方法
 *  \param  buf 发送数据
 *  \param  len 数据长度
 *  \return 成功返回发送的数据长度,失败返回STATUS_ERROR
 *  \note   默认为Socket的发送方法,用户可覆盖以定制自己的发送方法
 */
int UdpServer::sendData(const char* buf, int len)
{
    int ret=m_serverSocket->writeData(buf, len,500);
    if(ret<len)
    {
        TRACE_ERR_CLASS("write erorr,len(%d),ret(%d).\n",len,ret);
        return STATUS_ERROR;
    }
    return ret;
}

void UdpServer::run()
{
    TcpSocket* server = dynamic_cast<TcpSocket*>(m_serverSocket);
    while(1)
    {
        if (server!=NULL)
        {
            if(STATUS_ERROR==server->acceptConnection(*(TcpSocket*)(m_clientSocket)))
            {
                continue;
            }
            serviceLoop();
            m_clientSocket->close();
        }
        else
        {
            serviceLoop();
        }
    }
}

TcpServer::TcpServer()
{
}

TcpServer::~TcpServer()
{
}
/**
 *  \brief  服务器接收数据方法
 *  \param  buf 接收缓存
 *  \param  len 缓存的大小
 *  \return 返回值ret大于0表示接收到ret个数据,等于0表示断开连接,小于0表示接收失败
 *  \note   默认为Socket的接收方法,用户可覆盖以定制自己的接收方法
 */
int TcpServer::recvData(char * buf, int len)
{
    TcpSocket* client = (TcpSocket*)(*(getClientSocket()));
    return client->readData(buf, len,500);
}
/**
 *  \brief  服务器发送数据方法
 *  \param  buf 发送数据
 *  \param  len 数据长度
 *  \return 成功返回发送的数据长度,失败返回STATUS_ERROR
 *  \note   默认为Socket的发送方法,用户可覆盖以定制自己的发送方法
 */
int TcpServer::sendData(const char * buf, int len)
{
    TcpSocket* client = (TcpSocket*)(*(getClientSocket()));
    int ret=client->writeData(buf, len,500);
    if(ret<len)
    {
        TRACE_ERR_CLASS("write erorr,len(%d),ret(%d).\n",len,ret);
        return STATUS_ERROR;
    }
    return ret;
}

