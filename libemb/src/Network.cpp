/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifdef OS_CYGWIN
#else
#include "Tracer.h"
#include "Network.h"
#include "CmdExecuter.h"
#include "Utils.h"

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>
Network::Network()
{
}

Network::~Network()
{
}

bool Network::isValidIP(const std::string ip)
{
    struct in_addr addr;
    int ret=inet_pton(AF_INET,ip.c_str(),&addr);
    if (ret>0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::string Network::getSubnetIP(const std::string gatewayIP,const std::string subnetMask,int subIndex)
{
    int ret;
    struct in_addr ip,mask,result;
    if (inet_pton(AF_INET,gatewayIP.c_str(),&ip)<=0)
    {
        return "";
    }
    if (inet_pton(AF_INET,subnetMask.c_str(),&mask)<=0)
    {
        return "";
    }
    
    result.s_addr = ip.s_addr & mask.s_addr;
    result.s_addr = result.s_addr & (subIndex<<24);
    char tmp[16]={0};
    if (NULL==inet_ntop(AF_INET,&result,tmp,16))
    {
        return "";
    }
    return std::string(tmp);
}

/**
 *  \brief  获取网口ip地址
 *  \param  intf 网口名称,如eth0
 *  \return IP地址字符串,获取失败返回空字符串
 *  \note   none
 */
std::string Network::getInetAddr(const std::string intf)
{
	struct sockaddr_in* addr;
	struct ifreq ifr;
	int sockfd;
	int ret;
	if (intf.size()>= IFNAMSIZ)
	{
		return "";
	}
	strcpy(ifr.ifr_name, intf.c_str());
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		return "";
	}
	
	ret=ioctl(sockfd, SIOCGIFADDR, &ifr);
	if (ret<0)
	{
		TRACE_ERR("Network::getInetAddr,ioctl error:SIOCGIFADDR,%s.\n",ERROR_STRING);
		return "";
	}
	addr = (struct sockaddr_in *)&(ifr.ifr_addr);
	char* ipstr = inet_ntoa(addr->sin_addr);
	if (ipstr==NULL)
	{
		TRACE_ERR("Network::getInetAddr,inet_ntoa error.\n");
		return "";
	}
	std::string ipaddr = ipstr;
	close(sockfd);
	return ipaddr;
}
/**
 *  \brief  设置网口ip地址
 *  \param  intf 网口名称,如eth0
 *  \param  ipaddr ip地址字符串
 *  \return 设置成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int Network::setInetAddr(const std::string intf, const std::string ipaddr)
{
	int ret;
	struct ifreq ifr;
	if (intf.size() >= IFNAMSIZ)
	{
		return STATUS_ERROR;
	}
	strcpy( ifr.ifr_name, intf.c_str());
	
	int sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		return STATUS_ERROR;
	}
	
	ret=ioctl( sockfd, SIOCGIFADDR, &ifr);
	if (ret<0)
	{
		TRACE_ERR("Network::setInetAddr,ioctl error:SIOCGIFADDR,%s.\n",ERROR_STRING);
		return STATUS_ERROR;
	}
	struct sockaddr_in *addr = (struct sockaddr_in *)&(ifr.ifr_addr);
	addr->sin_family = AF_INET;
	ret=inet_aton(ipaddr.c_str(), &(addr->sin_addr));
	if (ret==0)
	{
		TRACE_ERR("Network::setInetAddr,inet_aton error.\n");
		return STATUS_ERROR;
	}
	ret=ioctl( sockfd, SIOCSIFADDR, &ifr);
	if(ret<0)
	{
		TRACE_ERR("Network::setInetAddr,ioctl error:SIOCSIFADDR,%s.\n",ERROR_STRING);
		return STATUS_ERROR;
	}
	return STATUS_OK;
}
/**
 *  \brief  获取网口子网掩码地址
 *  \param  intf 网口名称,如eth0
 *  \return IP地址字符串,获取失败返回空字符串
 *  \note   none
 */
std::string Network::getMaskAddr(const std::string intf)
{
	struct sockaddr_in* addr;
	struct ifreq ifr;
	int sockfd;
	int ret;
	if (intf.size()>= IFNAMSIZ)
	{
		return "";
	}
	strcpy(ifr.ifr_name, intf.c_str());
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		return "";
	}
	ret=ioctl(sockfd, SIOCGIFNETMASK, &ifr);
	if (ret<0)
	{
		TRACE_ERR("Network::getMaskAddr,ioctl error:SIOCGIFNETMASK,%s.\n",ERROR_STRING);
		return "";
	}
	addr = (struct sockaddr_in *)&(ifr.ifr_addr);
	char* ipstr = inet_ntoa(addr->sin_addr);
	if (ipstr==NULL)
	{
		TRACE_ERR("Network::getMaskAddr,inet_ntoa error.\n");
		return "";
	}
	std::string netmask = ipstr;
	close(sockfd);
	return netmask;
}
/**
 *  \brief  设置网口子网掩码地址
 *  \param  intf 网口名称,如eth0
 *  \param  netmask 子网掩码ip地址字符串
 *  \return 设置成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int Network::setMaskAddr(const std::string intf,const std::string netmask)
{
	int ret;
	struct ifreq ifr;
	if (intf.size() >= IFNAMSIZ)
	{
		return STATUS_ERROR;
	}
	strcpy( ifr.ifr_name, intf.c_str());
	
	int sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		return STATUS_ERROR;
	}
	ret=ioctl( sockfd, SIOCGIFADDR, &ifr);
	if (ret<0)
	{
		TRACE_ERR("Network::setMaskAddr,ioctl error:SIOCGIFADDR,%s.\n",ERROR_STRING);
		return STATUS_ERROR;
	}
	struct sockaddr_in *addr = (struct sockaddr_in *)&(ifr.ifr_addr);
	addr->sin_family = AF_INET;
	ret = inet_aton(netmask.c_str(), &(addr->sin_addr));
	if (ret==0)
	{
		TRACE_ERR("Network::setMaskAddr,inet_aton error.\n");
		return STATUS_ERROR;
	}
	
	ret=ioctl( sockfd, SIOCSIFNETMASK, &ifr);
	if(ret<0)
	{
		return STATUS_ERROR;
	}
	return STATUS_OK;
}
/**
 *  \brief  获取网口mac地址
 *  \param  intf 网口名称,如eth0
 *  \return mac地址字符串,获取失败返回空字符串
 *  \note   none
 */
std::string Network::getMacAddr(const std::string intf)
{
	int sockfd;
	int ret;
	char mac[6];
	struct ifreq ifr;
	if (intf.size()>= IFNAMSIZ)
	{
		return "";
	}
	
	strcpy(ifr.ifr_name, intf.c_str());
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		return "";
	}
	
	ret=ioctl(sockfd, SIOCGIFHWADDR, &ifr);
	if(ret<0)
	{
		TRACE_ERR("Network::getMacAddr,ioctl error:SIOCGIFHWADDR,%s.\n",ERROR_STRING);
		return "";
	}
	memcpy(mac, ifr.ifr_hwaddr.sa_data, sizeof(mac));
	char buf[20]={0};
	sprintf(buf,"%02X:%02X:%02X:%02X:%02X:%02X",
			mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	std::string macaddr=buf;
	return macaddr;
}
/**
 *  \brief  设置网口mac地址
 *  \param  intf 网口名称,如eth0
 *  \param  mac mac地址字符串
 *  \return 设置成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int Network::setMacAddr(const std::string intf,const std::string mac)
{
	int sockfd;
	int ret;
	char macaddr[6]={0};
	struct ifreq ifr;
	if (intf.size()>= IFNAMSIZ || mac.size()!=17)
	{
		return STATUS_ERROR;
	}
	strcpy(ifr.ifr_name, intf.c_str());
	
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		return STATUS_ERROR;
	}

	const char *pMacStr = mac.c_str();
	for(int i=0; i<6; i++)
	{
		*(macaddr+i) = Utils::ascii2digital(*(pMacStr+3*i))*16+
					   Utils::ascii2digital(*(pMacStr+3*i+1));
	}
	
	ifr.ifr_hwaddr.sa_family = ARPHRD_ETHER;
	memcpy((unsigned char*)ifr.ifr_hwaddr.sa_data, macaddr, 6);
	ret=ioctl(sockfd, SIOCSIFHWADDR, &ifr);
	if(ret<0)
	{
		TRACE_ERR("Network::setMacAddr[%s],ioctl error:SIOCSIFHWADDR,%s.\n",mac.c_str(),ERROR_STRING);
		return STATUS_ERROR;
	}
	return STATUS_OK;
}
/**
 *  \brief  获取网口状态
 *  \param  intf 网口名称,如eth0
 *  \return 成功返回网口状态(INTF::DOWN或者INTF::UP),获取失败返回STATUS_ERROR
 *  \note   none
 */
int Network::getIfUpDown(const std::string intf)
{
	int ret;
	struct ifreq ifr;
	int sockfd;
	if (intf.size()>= IFNAMSIZ)
	{
		return STATUS_ERROR;
	}
	strcpy(ifr.ifr_name, intf.c_str());

	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		TRACE_ERR("Network::getIfUpDown, open socket error.\n");
		return STATUS_ERROR;
	}
	ret=ioctl(sockfd, SIOCGIFFLAGS, &ifr);
	if (ret<0)
	{
		TRACE_ERR("Network::getIfUpDown,ioctl error:SIOCGIFFLAGS,%s.\n",ERROR_STRING);
		return STATUS_ERROR;
	}
	
	if(ifr.ifr_flags & IFF_UP)
	{
		return INTF::UP;
	}
	else
	{
		return INTF::DOWN;
	}
}
/**
 *  \brief  设置网口状态
 *  \param  intf 网口名称,如eth0
 *  \param  upOrDown INTF::DOWN或者INTF:UP
 *  \return 成功返回STATUS_OK,获取失败返回STATUS_ERROR
 *  \note   none
 */
int Network::setIfUpDown(const std::string intf, int upOrDown)
{
	int ret;
	struct ifreq ifr;
	int sockfd;

	if (intf.size()>= IFNAMSIZ)
	{
		return STATUS_ERROR;
	}
	strcpy(ifr.ifr_name, intf.c_str());
	
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0)
	{
		return STATUS_ERROR;
	}
	/* 先读取ifflags */
	ret=ioctl( sockfd, SIOCGIFFLAGS, &ifr);
	if (ret<0)
	{
		TRACE_ERR("Network::getIfUpDown,ioctl error:SIOCGIFFLAGS,%s.\n",ERROR_STRING);
		return STATUS_ERROR;
	}

	/* 再设置ifflags */
	if (upOrDown==INTF::DOWN)
	{
		ifr.ifr_flags &= (~IFF_UP);
	}
	else
	{
		ifr.ifr_flags |= IFF_UP;
	}
	ret=ioctl( sockfd, SIOCSIFFLAGS, &ifr);
	if (ret<0)
	{
		TRACE_ERR("Network::getIfUpDown,ioctl error:SIOCSIFFLAGS,%s.\n",ERROR_STRING);	
		return STATUS_ERROR;
	}

	return STATUS_OK;
}

/**
 *  \brief  dhcp动态获取ip地址
 *  \param  intf 网口名称,如eth0
 *  \return 动态获取成功返回IP地址字符串,获取失败返回空字符串
 *  \note   none
 */
std::string Network::dhcpClientGetIP(const std::string intf)
{
	/* udhcpc -i ra0  -nq  */
	char buf[1024];
	std::string cmd;
	cmd = "udhcpc -i ";
	cmd+= intf;
	cmd+= " -nq 2>&1";
	
	if(STATUS_OK==CmdExecuter::execute(cmd, buf, sizeof(buf)))
	{
		std::string bufStr(buf);
		/* Lease of 192.168.100.102 obtained, lease time 7200 */
		std::string result =Utils::findString(bufStr, "Lease of ", "obtained, lease time");
		if(!result.empty())
		{
			result=result.substr(8);
			return Utils::trimEndingBlank(result);
		}
	}
	return "";
}

int Network::checkSubnetIP(const std::string gatewayIP,const std::string subnetMask,std::string& subnetIP)
{
    int ret;
    struct in_addr ip,mask,sub,result;
    if (inet_pton(AF_INET,gatewayIP.c_str(),&ip)<=0)
    {
        return STATUS_ERROR;
    }

    if (inet_pton(AF_INET,subnetMask.c_str(),&mask)<=0)
    {
        return STATUS_ERROR;
    }
    
    if (inet_pton(AF_INET,subnetIP.c_str(),&sub)<=0)
    {
        return STATUS_ERROR;
    }

    if ((sub.s_addr & mask.s_addr)==(ip.s_addr & mask.s_addr))
    {
        return STATUS_OK;
    }
    
    result.s_addr = (ip.s_addr & mask.s_addr);
    result.s_addr =result.s_addr | ((~mask.s_addr) & sub.s_addr);
    char tmpStr[16]={0};
    if (NULL==inet_ntop(AF_INET,&result,tmpStr,16))
    {
        return STATUS_ERROR;
    }
    subnetIP = std::string(tmpStr);
    return STATUS_OK;
}

int Network::getIfLinkUpDown(const std::string intf)
{
	int ret;
	FILE *fp = NULL;
	char cmd[64] = {0}, buf[32] = {0};

	if (intf.size()>= IFNAMSIZ)
	{
		return STATUS_ERROR;
	}

    memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "cat /sys/class/net/%s/operstate", intf.c_str());
    if(STATUS_OK==CmdExecuter::execute(cmd, buf, sizeof(buf)))
	{
	    std::string bufStr = buf;
		if(bufStr.find("up") != std::string::npos)
		{
			return INTF_LINK_UP;
		}
	}
    
	return INTF_LINK_DOWN;
}
#endif