/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#include "BaseType.h"
#include "Tracer.h"
#include "File.h"
#include "JSONData.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 *  \brief  JSONNode构造函数
 *  \param  none
 *  \return none
 *  \note   JSONNode代表一个用key:value表示的JSON子结点
 */
JSONNode::JSONNode():
m_object(NULL),
m_name("")
{
}

/**
 *  \brief  JSONNode拷贝构造函数
 *  \param  none
 *  \return none
 *  \note   用于值传递用,浅拷贝(不拷贝cJSON里的数据)
 */
JSONNode::JSONNode(const JSONNode& node)
{
    this->m_object = node.m_object;
    this->m_name = node.m_name;
}

/**
 *  \brief  JSONNode析构函数
 *  \param  none
 *  \return none
 */
JSONNode::~JSONNode()
{
}

/**
 *  \brief  返回串行化后的字符串
 *  \param  bool fmt 是否格式化
 *  \return 结点字符串
 *  \note   none
 */
std::string JSONNode::serialize(bool fmt)
{
    char* out=NULL;
    if (fmt)
    {
        out = cJSON_Print(m_object);
    }
    else
    {
        out = cJSON_PrintUnformatted(m_object);
    }
    if (out!=NULL)
    {
        std::string jsonString = std::string(out);
        free(out);
        return jsonString;
    }
    return "";
}

/**
 *  \brief  判断是否存在nodeName结点
 *  \param  nodeName 结点名称字符串
 *  \return 存在返回true,不存在返回false
 *  \note   none
 */
bool JSONNode::hasSubNode(const std::string nodeName)
{
    if(NULL==cJSON_GetObjectItem(m_object, nodeName.c_str()))
    {
        return false;
    }
    return true;
}

/**
 *  \brief  判断结点是否是NULL结点
 *  \param  none
 *  \return false or true
 *  \note   none
 */
bool JSONNode::isNullNode()
{
    if (NULL==m_object)
    {
        return false;
    }
    return (m_object->type==cJSON_NULL)?true:false;
}
/**
 *  \brief  判断结点是否是数组型结点
 *  \param  none
 *  \return false or true
 *  \note   none
 */
bool JSONNode::isArrayNode()
{
    if (NULL==m_object)
    {
        return false;
    }
    return (m_object->type==cJSON_Array)?true:false;
}

/**
 *  \brief  根据index获取数组的大小
 *  \param  none
 *  \return 返回数组结点里的元素(结点)个数
 *  \note   none
 */
int JSONNode::getArraySize()
{
    JSONNode node;
    if (NULL==m_object)
    {
        return 0;
    }
    return cJSON_GetArraySize(m_object);
}

/**
 *  \brief  根据结点名称nodeName获取子结点
 *  \param  nodeName 结点名称字符串
 *  \return 返回结点对象
 *  \note   none
 */
JSONNode JSONNode::operator[](const std::string nodeName)
{
    JSONNode node;
    if (NULL==m_object||
        nodeName.empty())
    {
        return node;
    }
    node.m_object = cJSON_GetObjectItem(m_object, nodeName.c_str());
    node.m_name = nodeName;
    return node;
}

/**
 *  \brief  根据index获取数组结点里的元素结点
 *  \param  index 索引值
 *  \return 返回index对应的数组元素
 *  \note   数组元素也是一个结点,因为数组里可以包含任意类型的结点
 */
JSONNode JSONNode::operator[](int index)
{
    JSONNode node;
    if (NULL==m_object)
    {
        return node;
    }
    node.m_object = cJSON_GetArrayItem(m_object, index);
    node.m_name = "";
    return node;
}

/**
 *  \brief  返回结点的布尔值
 *  \param  none
 *  \return false or true
 *  \note   none
 */
bool JSONNode::toBool()
{
    if (NULL==m_object || 
        m_object->type!=cJSON_False ||
        m_object->type!=cJSON_True)
    {
        return false;
    }
    return (m_object->type==cJSON_True)?true:false;
}
/**
 *  \brief  返回结点的整形值
 *  \param  none
 *  \return int
 *  \note   none
 */
int JSONNode::toInt()
{
    if (NULL==m_object || m_object->type!=cJSON_Number)
    {
        return 0;
    }
    return m_object->valueint;
}
/**
 *  \brief  返回结点的浮点值
 *  \param  none
 *  \return double
 *  \note   none
 */
double JSONNode::toDouble()
{
    if (NULL==m_object || m_object->type!=cJSON_Number)
    {
        return 0;
    }
    return m_object->valuedouble;
}
/**
 *  \brief  返回结点的字符串值
 *  \param  none
 *  \return 返回字符串
 *  \note   none
 */
std::string JSONNode::toString()
{
    if (NULL==m_object || m_object->type!=cJSON_String)
    {
        return "";
    }
    char* value= m_object->valuestring;
    if (value==NULL)
    {
        return "";
    }
    std::string str=value;
    return str;
}

JSONNode JSONNode::toNode()
{
    JSONNode node;
    if (NULL==m_object || m_object->type!=cJSON_Object)
    {
        return node;
    }
    node.m_object = m_object;
    node.m_name = "";
    return node;
}

/**
 *  \brief  加入布尔值子结点
 *  \param  nodeName 名称字串
 *  \return 成功返回true,失败返回false
 *  \note   当JSON数据为数组格式时,nodeName可省略
 */
bool JSONNode::addSubNode(bool value,const std::string nodeName)
{
    if (m_object==NULL)
    {
        return addNullNode(nodeName);
    }
    cJSON_AddItemToObject(m_object, nodeName.c_str(), cJSON_CreateBool((int)value));
    return true;   
}
/**
 *  \brief  加入数字值子结点
 *  \param  nodeName 名称字串
 *  \param  value 整数或浮点数
 *  \return 成功返回true,失败返回false
 *  \note   当JSON数据为数组格式时,nodeName可省略
 */
bool JSONNode::addSubNode(int value,const std::string nodeName)
{
    if (m_object==NULL)
    {
        return addNullNode(nodeName);
    }
    cJSON_AddItemToObject(m_object, nodeName.c_str(), cJSON_CreateNumber(value));
    return true;  
}

/**
 *  \brief  加入数字值子结点
 *  \param  nodeName 名称字串
 *  \param  value 整数或浮点数
 *  \return 成功返回true,失败返回false
 *  \note   当JSON数据为数组格式时,nodeName可省略
 */
bool JSONNode::addSubNode(double value,const std::string nodeName)
{
    if (m_object==NULL)
    {
        return addNullNode(nodeName);
    }
    cJSON_AddItemToObject(m_object, nodeName.c_str(), cJSON_CreateNumber(value));
    return true; 
}
/**
 *  \brief  加入字符串子结点
 *  \param  nodeName 名称字串
 *  \return 成功返回true,失败返回false
 *  \note   当JSON数据为数组格式时,nodeName可省略
 */
bool JSONNode::addSubNode(const char* value,const std::string nodeName)
{
    if (m_object==NULL)
    {
        return addNullNode(nodeName);
    }
    cJSON_AddItemToObject(m_object, nodeName.c_str(), cJSON_CreateString(value));
    return true;
}

/**
 *  \brief  加入数组子结点
 *  \param  nodeName 名称字串
 *  \return 成功返回true,失败返回false
 *  \note   当JSON数据为数组格式时,nodeName可省略
 */
bool JSONNode::addSubNode(Array& array,const std::string nodeName)
{
    if (m_object==NULL)
    {
        return addNullNode(nodeName);
    }
    JSONNode* pNode = new JSONNode();
    pNode->m_name = nodeName;
    switch (array.type())
    {
        case BASETYPE_INTARRAY:
            pNode->m_object = cJSON_CreateIntArray(NULL,0);
            break;
        case BASETYPE_DOUBLEARRAY:
            pNode->m_object = cJSON_CreateDoubleArray(NULL,0);
            break;
        case BASETYPE_STRINGARRAY:
            pNode->m_object = cJSON_CreateStringArray(NULL,0);
            break;
        default:
            return false;
    }
    int asize = array.size();
    for(int i=0; i<asize; i++)
    {
        cJSON* item=NULL;
        switch (array.type())
        {
            case BASETYPE_INTARRAY:
            {
                IntArray *intArray = (IntArray*)(&array);
                item = cJSON_CreateNumber((*intArray)[i]);
                break;
            }
            case BASETYPE_DOUBLEARRAY:
            {
                DoubleArray *doubleArray = (DoubleArray*)(&array);
                item = cJSON_CreateNumber((*doubleArray)[i]);
                break;
            }
            case BASETYPE_STRINGARRAY:
            {
                StringArray *stringArray = (StringArray*)(&array);
                item = cJSON_CreateString((*stringArray)[i].c_str());
                break;
            }
        }
        cJSON_AddItemToArray(pNode->m_object, item);
    }
    cJSON_AddItemToObject(m_object, nodeName.c_str(), pNode->m_object);
    return true;
}
/**
 *  \brief  加入JSON对象子结点
 *  \param  nodeName 名称字串
 *  \return 成功返回true,失败返回false
 *  \note   当JSON数据为数组格式时,nodeName可省略
 */
bool JSONNode::addSubNode(JSONNode* object,const std::string nodeName)
{
    if (m_object==NULL)
    {
        return addNullNode(nodeName);
    }
    cJSON_AddItemToObject(m_object, nodeName.c_str(),cJSON_Duplicate(object->m_object,1));
    return true; 
}

/**
 *  \brief  加入NULL子结点
 *  \param  nodeName 名称字串
 *  \return 成功返回true,失败返回false
 *  \note   当JSON数据为数组格式时,nodeName可省略
 */
bool JSONNode::addNullNode(const std::string nodeName)
{
    if (m_object==NULL)
    {
        return false;
    }
    cJSON_AddItemToObject(m_object, nodeName.c_str(), cJSON_CreateNull());
    return true;
}

/**
 *  \brief  JSONData构造函数
 *  \param  none
 *  \return none
 *  \note   JSONData有两种形式,一种是对象形式:形如"{key:value,...}",用大括号
            将所有子结点包围.另一种是更简化的数组形式:形如"[{...},{...}]",用
            中括号[]将同一类型的子结点包围,数组中可以包含同一类型的对象.
            JSONData的根结点就是它本身,其他的都视为子结点。
 */
JSONData::JSONData():
m_rootNode(NULL)
{
}

JSONData::~JSONData()
{
    if (m_rootNode!=NULL)
    {
        if (m_rootNode->m_object!=NULL)
        {
            //TRACE_CYAN("destroy nodes:%x->%x\n",m_rootNode,m_rootNode->m_object);
            /* 删除根结点下的所有结点 */
            cJSON_Delete(m_rootNode->m_object);
        }
        delete m_rootNode;
        m_rootNode = NULL;
    }
}
/**
 *  \brief  初始化JSONData为对象形式
 *  \param  none
 *  \return 成功返回根结点,失败返回NULL
 *  \note   none
 */
JSONNode* JSONData::initWithObjectFormat()
{
    if(m_rootNode!=NULL)
    {
        TRACE_ERR_CLASS("Reinit JSONData!\n");
        return NULL;
    }
    m_rootNode = new JSONNode();
    m_rootNode->m_object = cJSON_CreateObject();
    return m_rootNode;
}
/**
 *  \brief  初始化JSONData为数组形式
 *  \param  none
 *  \return 成功返回根结点,失败返回NULL
 *  \note   none
 */
JSONNode* JSONData::initWithArrayFormat()
{
    if(m_rootNode!=NULL)
    {
        TRACE_ERR_CLASS("Reinit JSONData!\n");
        return NULL;
    }
    m_rootNode = new JSONNode();
    m_rootNode->m_object = cJSON_CreateArray();
    return m_rootNode;
}
/**
 *  \brief  读取JSON字符串进行初始化
 *  \param  none
 *  \return 成功返回根结点,失败返回NULL
 *  \note   none
 */
JSONNode* JSONData::initWithDataString(const char* jdata)
{
    m_rootNode = new JSONNode();	
	m_rootNode->m_object = cJSON_Parse(jdata);
	if (!m_rootNode->m_object)
    {
        TRACE_ERR_CLASS("Error before: [%s]\n",cJSON_GetErrorPtr());
        return NULL;
    }
    return m_rootNode;
}
/**
 *  \brief  读取JSON文件内容进行初始化
 *  \param  none
 *  \return 成功返回根结点,失败返回NULL
 *  \note   none
 */
JSONNode* JSONData::initWithContentOfFile(const std::string fileName)
{
    if(m_rootNode!=NULL)
    {
        TRACE_ERR_CLASS("Reinit JSONData!\n");
        return NULL;
    }
    
    File file(fileName,IO_MODE_RD_ONLY);
    int len = file.getSize();
    char* jdata=(char*)malloc(len+1);
    if (NULL==jdata)
    {
        return NULL;
    }
    if(file.readData(jdata,len)<=0)
    {
        return NULL;
    }
    *(jdata+len)=0;

    JSONNode* pNode=initWithDataString(jdata);
    free(jdata);
    return pNode;
}
/**
 *  \brief  写入JSON数据到文件中
 *  \param  none
 *  \return 成功返回true,失败返回false
 *  \note   none
 */
bool JSONData::saveAsFile(const std::string fileName,bool fmt)
{
    bool ret=false;
    File *file = new File();
    if(!file->open(fileName.c_str(),IO_MODE_REWR_ORNEW))
    {
        if (m_rootNode!=NULL)
        {
            char* out=NULL;
            if (fmt)
            {
                out=cJSON_Print(m_rootNode->m_object);
            }
            else
            {
                out=cJSON_PrintUnformatted(m_rootNode->m_object);
            }
            if (out!=NULL)
            {
                int len = strlen(out);
                if (file->writeData(out,len)!=len)
                {
                    TRACE_ERR_CLASS("write file error:%s\n",fileName.c_str());
                }
    	        free(out);
            }
            ret = true;
        }
    }
    delete file;
    return ret;
}

/**
 *  \brief  串行化JSONData成字符串
 *  \param  none
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
std::string JSONData::serialize(bool fmt)
{
    char* out=NULL;
    if (fmt)
    {
        out=cJSON_Print(m_rootNode->m_object);
    }
    else
    {
        out=cJSON_PrintUnformatted(m_rootNode->m_object);
    }
    if (out!=NULL)
    {
        std::string jsonString=string(out);
        free(out);
        return jsonString;
    }
    return "";
}

