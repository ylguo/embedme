/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __PORT_ADAPTER_H__
#define __PORT_ADAPTER_H__

#include "BaseType.h"
#include "IODevice.h"
#include "Thread.h"
#include "Mutex.h"

#define PORT_BUFF_SIZE      64      /* �˿ڶ������С */

/**
 *  \file   PortAdapter.h   
 *  \class  PortAdapter
 *  \brief  �˿���������
 *  \note   none
 */
class PortAdapter:public RunnableProtocol{
public:
    PortAdapter();
    virtual ~PortAdapter();
    IODevice* getPort(){return m_device;}
    int bind(IODevice* device,int bufSize);
    int unbind();
    int sendData(const char* buf,int size);
    virtual void recievedData(const char* buf, int size)=0;
private:
    void run();
    void recvTask(void* arg);
private:
    IODevice* m_device;
    char* m_recvBuf;
    int m_bufSize;
    Thread m_mainThread;
};
#endif