/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __SQLITEWRAPPER_H__
#define __SQLITEWRAPPER_H__

#ifdef OS_CYGWIN
#else
#include "sqlite3.h"
#include "DataBase.h"
#include "Mutex.h"

/**
 *  \file   SqliteWrapper.h   
 *  \class  SqliteWrapper
 *  \brief  Sqlite��װ����	
 */
class SqliteWrapper:public DataBase{
public:
    SqliteWrapper();
    ~SqliteWrapper();
    bool open(std::string dbName);
    bool connect(std::string user, std::string passwd);
    bool close();
    bool exec(const std::string& sql, SqlResult_S* result=NULL);
    bool insertBlob(const std::string& sql,std::string bindVarName,char* buf, int len);
    bool selectBlob(const std::string& sql,std::string bindVarName,char* buf, int &len);
private:
    sqlite3* m_db;
    MutexLock m_dbLock;
};
#endif
#endif