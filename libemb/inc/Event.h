/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __EVENT_H__
#define __EVENT_H__

#include "BaseType.h"
#include "Thread.h"
#include "Mutex.h"

#include<list>

#define EVENT_PARAM_MAXLEN      128      /**< 定义事件传递的参数最大长度 */

/**
 *  \file   Event.h   
 *  \class  Event
 *  \brief  事件类.
 */

class Event{
public:
    Event(int eventID, const char* param=NULL, int len=0);
    Event(const Event&);
    ~Event();
    int getEventId();
    int getParam(char* buf,int len);  /* 获取事件携带的数据 */
private:
    int  m_eventID;
    char    m_eventParam[EVENT_PARAM_MAXLEN];
    int  m_eventParamLen;
};


/**
 *  \file   Event.h   
 *  \class  EventListner
 *  \brief  事件处理者抽象类.
 */
class EventListner{
public:
    EventListner(){};
    virtual ~EventListner(){};
    virtual void handleEvent(Event& evt)=0;
};


/**
 *  \file   Event.h   
 *  \class  EventDispatcher
 *  \brief  Observer模式实现的事件管理器.
 */

class EventDispatcher:public RunnableProtocol{
public:
    EventDispatcher();
    virtual ~EventDispatcher();
    virtual void registerListener(EventListner* pEvtHandler);
    virtual void unregisterListener(EventListner* pEvtHandler);
    virtual void postEvent(Event& evt);
private:
    void run();
private:
    Thread m_ecThread;
    std::list<EventListner*> m_evtListenerList;
    std::list<Event> m_eventList;
    MutexLock m_eventLock;
    MutexLock m_listenerLock;
};


/**
 *  \file   Event.h   
 *  \class  EventCenter
 *  \brief  事件中心类,EventDispatcher的适配器,便于继承.
 */
class EventCenter{
public:
    EventCenter();
    virtual ~EventCenter();
    virtual void addEventListener(EventListner* listener);
    virtual void delEventListener(EventListner* listener);
    virtual void postEvent(Event& evt);
private:
    EventDispatcher* m_eventDispatcher;
};


#endif
