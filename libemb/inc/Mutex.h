/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MUTEXLOCK_H__
#define __MUTEXLOCK_H__

#include "BaseType.h"

#include <pthread.h>

/**
 *  \file   Mutex.h   
 *  \class  MutexLock
 *  \brief  互斥锁类	
 */
class MutexLock{
public:
    MutexLock();
    ~MutexLock();
    int lock();
    int unLock();
    int tryLock();
private:
    pthread_mutex_t m_mutex;
friend class MutexCond;
};
/**
 *  \file   Mutex.h   
 *  \class  AutoLock
 *  \brief  自动释放的互斥锁类	
 */
class AutoLock{
public:
    AutoLock(MutexLock* mutexLock);
    virtual ~AutoLock();
private:
    MutexLock* m_mutexLock;
};

/**
 *  \file   Mutex.h   
 *  \class  MutexCond
 *  \brief  条件变量
 */
class MutexCond{
public:
    MutexCond(MutexLock* lock);
    ~MutexCond();
    void wait();
    void meet();
private:
    MutexLock* m_lock;
    pthread_cond_t m_cond;
};

#endif