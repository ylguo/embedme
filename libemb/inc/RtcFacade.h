/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __CRTCFACADE_H__
#define __CRTCFACADE_H__
#include "BaseType.h"

/**
 *  \class  RtcTime_S
 *  \brief  时间结构体      
 */
typedef struct
{
    uint32 m_second;   /**< 秒 */
    uint32 m_minute;   /**< 分 */
    uint32 m_hour;     /**< 时 */
    uint32 m_date;     /**< 日 */
    uint32 m_month;    /**< 月 */
    uint32 m_year;     /**< 年 */
}RtcTime_S;

/**
 *  \file   RtcFacade.h   
 *  \class  RtcFacade
 *  \brief  实时时钟设备类	
 */
class RtcFacade{
public:
    /**
     *  \brief  获取RtcFacade单例
     *  \param  void
     *  \return RtcFacade* 
     *  \note   none
     */
    static RtcFacade* getInstance()
    {
        static RtcFacade instance;
        return &instance;
    }
    ~RtcFacade();
    void showTime(RtcTime_S rtcTime);   /* 打印RTC时间 */
    int readTime(RtcTime_S& rtcTime);/* 读取RTC时间 */
    int writeTime(RtcTime_S rtcTime);/* 改写RTC时间 */
private:
    RtcFacade();
};
#endif
