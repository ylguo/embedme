/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __IODEVICE_H__
#define __IODEVICE_H__

#include "BaseType.h"
#include <iostream>

/** 定义输入输出模式 */
typedef enum
{
    IO_MODE_RD_ONLY,    /**< 只可读(r) */
    IO_MODE_WR_ONLY,    /**< 只可写 */
    IO_MODE_RDWR_ONLY,  /**< 只可读写(r+) */
    IO_MODE_APPEND_ONLY,/**< 只增加 */
    IO_MODE_REWR_ORNEW, /**< 文件重写,没有则创建(w) */
    IO_MODE_RDWR_ORNEW, /**< 文件可读写,没有则创建(w+) */
    IO_MODE_APPEND_ORNEW,/**< 文件可增加,没有则创建(a) */
    IO_MODE_RDAPPEND_ORNEW,/**< 文件可读或可增加,没有则创建(a+) */
    IO_MODE_INVALID=0xFF,
}IO_MODE_E;

/**
 *  \file   IODevice.h   
 *  \class  IODevice
 *  \brief  IO设备抽象类
 */

class IODevice{
public:
    IODevice();
    virtual ~IODevice();
    virtual bool open(const char *device=NULL, int ioMode=IO_MODE_INVALID);
    virtual bool close();
    virtual int readData(char *buf, int count, int timeoutMs=-1)=0;
    virtual int writeData(const char *buf, int count, int timeoutMs=-1)=0;
    virtual int setAttribute(int attr, int value)=0;
    virtual int getAttribute(int attr)=0;
protected:
    int m_fd;
    std::string m_devName;
    int m_openMode;
};

#endif
