/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __FILE_H__
#define __FILE_H__

#include "IODevice.h"
#include <iostream>
using namespace std;

typedef enum{
    FILE_ATTR_READABLE=0x01,
    FILE_ATTR_WRITEABLE=0x02,
}FILE_ATTR_E;

/**
 *  \file   File.h   
 *  \class  File
 *  \brief  文件类
 */
class File:public IODevice{
public:
    File();
    File(const std::string fileName,int ioMode);
    virtual ~File();
    static bool isExist(const char* filePath);
	static int getContentSize(const char* fileName);
    
    /* 以下方法继承自IODevice,请参考IODevice定义. */
    virtual bool open(const char* fileName,int ioMode); 
    virtual bool close();
    virtual int readData(char* buf, int count, int timeoutMs=-1);
    virtual int writeData(const char*buf, int count, int timeoutMs=-1);
    virtual int setAttribute(int attr, int value);
    virtual int getAttribute(int attr);

    void* mapMemory();
    int unmapMemory();
    int readLine(std::string& lineStr);
    int getSize();
    std::string getName();   
    FILE* getFp();
    int getFd();
    bool isEnd();
    int getPos();
    int setPos(int pos);
private:
    FILE* m_fp;
    std::string m_name;
    void* m_mmaddr;
    int m_attr;
};

#endif