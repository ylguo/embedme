#ifndef __DATABASE_H__
#define __DATABASE_H__
#include "BaseType.h"
#include <iostream>
#include <vector>


/**
 *  \class   SqlResult_S
 *  \brief  SQL查询结果结构体 
 */
typedef struct{
    int m_columns;  /**< sql查询结果字串的列数(字段数) */
    std::vector<std::string> m_resultStrVect;   /**< 查询结果集 */
}SqlResult_S;

/**
 *  \file   DataBase.h   
 *  \class  DataBase
 *  \brief  数据库抽象类.
 */

class DataBase{
public:
    virtual bool open(std::string dbName)=0;
    virtual bool connect(std::string user, std::string passwd)=0;
    virtual bool close()=0;
    virtual bool exec(const std::string& sql,SqlResult_S* result=NULL)=0;
    virtual bool insertBlob(const std::string& sql,std::string bindVarName,char* buf, int len)=0;
    virtual bool selectBlob(const std::string& sql,std::string bindVarName,char* buf, int& len)=0;
};

#endif

