/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __GPIO_H__
#define __GPIO_H__

#include "BaseType.h"
#include <map>
#include <iostream>

/**
 *  \file   Gpio.h   
 *  \class  GPIO
 *  \brief  GPIO状态常量
 */

class GPIO{
public:
    static const int LOW  = 0;
    static const int HIGH = 1;
    static const int IN   = 0;
    static const int OUT  = 1;
};

/**
 *  \class  GpioInfo_S
 *  \brief  GPIO信息结构体 
 */
typedef struct{
    int m_label;         /**< GPIO标签 */
    std::string m_group;    /**< GPIO组名 */
    int m_pin;           /**< GPIO引脚号 */
    int m_dir;           /**< GPIO方向 */
}GpioInfo_S;

/**
 *  \file   Gpio.h   
 *  \class  GpioImp
 *  \brief  Gpio实例类
 */

class GpioFactory;
class GpioImp{
public:
    ~GpioImp();
    int config(int dir, int value=GPIO::LOW);
    int setPinValue(int value);
    int getPinValue();
private:
    GpioImp(int fd);
    friend class GpioFactory;
private:
    int m_fd;
    std::string m_gpioDir;
    std::string m_gpioExportFile;
    std::string m_gpioDirectionFile;
    std::string m_gpioValueFile;
};
/**
 *  \file   GpioFactory.h   
 *  \class  GpioFactory
 *  \brief  Gpio工厂类,负责管理创建和管理Gpio
 */

class GpioFactory{
public:
     /**
     *  \brief  获取GpioFactory单例
     *  \param  void
     *  \return GpioFactory*
     *  \note   none
     */
    static GpioFactory* getInstance()
    {
        static GpioFactory instance;
        return &instance;
    }
    ~GpioFactory(){};
    GpioImp* getGpioImp(std::string gpioName);
    GpioImp* getGpioImp(std::string group, int pin);
private:
    GpioFactory(){};
    typedef std::map<int, GpioImp*> GpioImpMap;
    GpioImpMap m_gpioImpTable;
};

#endif
