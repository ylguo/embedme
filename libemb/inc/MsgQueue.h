/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MSGQUEUE_H__
#define __MSGQUEUE_H__

#include "BaseType.h"
#include "Mutex.h"

#define USE_MQ_SYSV     (0)     /* 使用sysv的消息队列,否则使用POSIX消息队列 */

#include <sys/types.h>
#if USE_MQ_SYSV
#include <sys/ipc.h>
#include <sys/msg.h>
#else
#include <fcntl.h>
#include <sys/stat.h>
#ifdef OS_ANDROID
struct mq_attr {
    long mq_flags;       /* Flags: 0 or O_NONBLOCK */
    long mq_maxmsg;      /* Max. # of messages on queue */
    long mq_msgsize;     /* Max. message size (bytes) */
    long mq_curmsgs;     /* # of messages currently in queue */
    };
extern mqd_t mq_open(const char *name, int oflag);
extern mqd_t mq_open(const char *name, int oflag, mode_t mode,struct mq_attr *attr);
extern int mq_send(mqd_t mqdes, const char *msg_ptr,size_t msg_len, unsigned msg_prio);
extern ssize_t mq_receive(mqd_t mqdes, char *msg_ptr,size_t msg_len, unsigned *msg_prio);
extern int mq_unlink(const char *name);
#else
#include <mqueue.h>
#endif
#endif
#include <iostream>
#include <map>

#define MSG_SIZE_MAX    128             /**< 最大消息长度 */
#define POSIX_MQFS      "/dev/mqueue"   /**< Posix消息队列挂载点 */
/**
 *  \class  QueueMsg_S
 *  \brief  msgbuf消息结构体
 */
typedef struct{
    int m_msgType;                  /**< 消息类型 */
    unsigned char m_dataLen;        /**< 数据长度 */
    char m_data[MSG_SIZE_MAX-1];    /**< 消息数据 */
}QueueMsg_S;

/**
 *  \file   MsgQueue.h   
 *  \class  MsgQueue
 *  \brief  消息队列类		
 */

class MsgQueue{
public:
    ~MsgQueue();
    int sendMsg(QueueMsg_S& msg);
    int recvMsg(QueueMsg_S& msg, int msgType=0);
    int clearMsg(int msgType=0);
private:
    MsgQueue(key_t key);
    bool initialize();
    std::string getMqName(int key); 
private:
    int m_msgID;
    key_t   m_key;
    friend class MsgQueueFactory;
};

/**
 *  \file   MsgQueue.h   
 *  \class  MsgQueueFactory
 *  \brief  消息队列工厂类	
 */

class MsgQueueFactory{
public:
    /**
     *  \brief  获取MsgQueueFactory单例
     *  \param  void
     *  \return MsgQueueFactory* 
     *  \note   none
     */
    static MsgQueueFactory* getInstance()
    {
        static MsgQueueFactory instance;
        return &instance;
    }
    ~MsgQueueFactory(){;}
    MsgQueue* getMsgQueue(key_t key);
private:
    MsgQueueFactory(){;}
private:
    MutexLock m_lock;
    std::map<key_t,MsgQueue*> m_msgQueueMap;
}; 

#endif

