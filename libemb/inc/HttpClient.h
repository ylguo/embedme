/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __HTTP_CLIENT_H__
#define __HTTP_CLIENT_H__

#ifdef OS_CYGWIN
#else
#include "BaseType.h"
#include "Thread.h"
#include "Mutex.h"
#include "File.h"

#include <iostream>
using namespace std;

typedef enum{
    HTTP_OK = 200,// OK: Success!
    HTTP_BAD_REQUEST = 400,// Bad Request: The request was invalid. 
    HTTP_NOT_AUTHORIZED = 401,// Not Authorized: Authentication credentials were missing or incorrect.
    HTTP_FORBIDDEN = 403,// Forbidden: The request is understood, but it has been refused.  
    HTTP_NOT_FOUND = 404,// Not Found: The URI requested is invalid or the resource requested, such as a user, does not exists.
    HTTP_NOT_ACCEPTABLE = 406,// Not Acceptable.
    HTTP_INTERNAL_SERVER_ERROR = 500,// Internal Server Error: Something is broken. 
    HTTP_BAD_GATEWAY = 502,// Bad Gateway
}HTTP_ACK_E;

/**
 *  \file   HttpClient.h   
 *  \class  HttpClient
 *  \brief  http�ͻ�����.
 */
class HttpClient:RunnableProtocol{
public:
    HttpClient();
    ~HttpClient(); 
    int httpGet(const std::string url,File* cacheFile,int timeoutSeconds=20);
    int httpPost(const std::string url,const std::string postString,File* cacheFile,int timeoutSeconds=20);
    int httpFileDownLoad(const std::string url,File* cacheFile);
private:
    void run();
    void fileDownLoadTask(void* arg);
    int curlPerform(int reqType,const char* url,FILE* fp);
private:
    Thread m_mainThread;
    MutexLock m_reqLock;
};
#endif
#endif
