/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __SEMAPHORE_H__
#define __SEMAPHORE_H__

#ifdef OS_CYGWIN
#else
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>

#include <iostream>
/**
 *  \file   Semaphore.h   
 *  \class  Semaphore
 *  \brief  信号量抽象类(基于POSIX接口)	
 */
class Semaphore{
public:
    Semaphore();
    virtual ~Semaphore();
    virtual bool open(int value=1)=0;
    virtual bool close()=0;
    bool wait();
    bool tryWait();
    bool post();
    bool getValue(int& value);
protected:
    sem_t m_sem;
};

/**
 *  \file   Semaphore.h   
 *  \class  NSemaphore
 *  \brief  有名信号量,一般用于进程间通信
 */
class NSemaphore:public Semaphore{
public:
    NSemaphore(char* name);
    ~NSemaphore();
    bool open(int value=1);
    bool close();
private:
    std::string m_name;
};
/**
 *  \file   Semaphore.h   
 *  \class  UNSemaphore
 *  \brief  无名信号量,一般用于线程间通信	
 */
class UNSemaphore:public Semaphore{
public:
    UNSemaphore();
    ~UNSemaphore();
    bool open(int value=1);
    bool close();
};
#endif
#endif