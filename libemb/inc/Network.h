/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __NETWORK_H__
#define __NETWORK_H__

#ifdef OS_CYGWIN
#else
#include "BaseType.h"
#include<iostream>

/**
 *  \file   Network.h   
 *  \class  INTF
 *  \brief  网口状态常量	
 */

class INTF{
public:
    static const int DOWN=0; /**< 网口已被ifconfig down */
    static const int UP=1;   /**< 网口已被ifconfig up */
};

typedef enum{
    INTF_LINK_DOWN = 0,   /**< 网口未插入网线,link down */
    INTF_LINK_UP,       /**< 网口已插入网线,link up */
}INTF_LINK_STATUS;
/**
 *  \file   Network.h   
 *  \class  Network
 *  \brief  网络类,提供对网络设别的常用操作.
 */

class Network{
public:
    Network();
    ~Network();
    static bool isValidIP(const std::string ip);
    std::string getSubnetIP(const std::string gatewayIP,const std::string subnetMask,int subIndex);
    static std::string getInetAddr(const std::string intf);
    static std::string getMaskAddr(const std::string intf);
    static std::string getMacAddr(const std::string intf);
    static int getIfUpDown(const std::string intf);
    static int setInetAddr(const std::string intf,const std::string ipaddr);
    static int setMaskAddr(const std::string intf,const std::string netmask);
    static int setMacAddr(const std::string intf,const std::string mac);
    static int setIfUpDown(const std::string intf, int upOrDown);
    static std::string dhcpClientGetIP(const std::string intf);
    static int checkSubnetIP(const std::string gatewayIP,const std::string subnetMask,std::string& subnetIP);
    static int getIfLinkUpDown(const std::string intf);
};

#endif
#endif
