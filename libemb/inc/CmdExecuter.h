/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __CMDEXECUTER_H__
#define __CMDEXECUTER_H__

/**
 *  \file   CmdExecuter.h   
 *  \class  CmdExecuter
 *  \brief  ����ִ������.
 */

#include "BaseType.h"
class CmdExecuter{
public:
    CmdExecuter(){};
    ~CmdExecuter(){};
    static int execute(std::string cmd, char* result, int len);
};

#endif
