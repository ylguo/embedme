#ifndef __REGEXP_H__
#define __REGEXP_H__

#include "Array.h"
#include <iostream>

/*----------------------------------* 
  RegExp支持的元字符:
  *     匹配0个或多个在*字符前的字符
  .     匹配任意字符
  ^     匹配行首;后面的字符非
  $     匹配行尾
  []    字符集合
  \     转义符
  <>    精确匹配
  {n}   匹配前面的字符出现n次
  {n,}  匹配前面的字符至少出现n次
  {n,m} 匹配前面的字符出现n-m次
  RegExp支持的扩展元字符:
  \w
  
 *----------------------------------*/

/**
 *  \file   RegExp.h   
 *  \class  RegExp
 *  \brief  正则表达式类.
 */
class RegExp{
public:
    RegExp();
    ~RegExp();
    bool match(const std::string& pattern,const std::string& source);
    bool match(const std::string& pattern,const std::string& source,std::string& result,int& pos);
    bool match(const std::string& pattern,const std::string& source,StringArray& strArray,IntArray& posArray,int maxMatches=1);
};


#endif