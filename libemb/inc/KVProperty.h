/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __KVPROPERTY_H__
#define __KVPROPERTY_H__

#include "BaseType.h"
#include "File.h"
#include "Mutex.h"
#include "Tuple.h"
#include "Array.h"

#include <iostream>
#include <map>
using namespace std;

/**
 *  \file   KVValue.h   
 *  \class  KVValue
 *  \brief  属性值	
 */

class KVValue{
public:
    KVValue();
    ~KVValue();
    int baseType();
    void clear();
    bool initWithString(const string& valueString);
    string serialize();
    int toInt();
    double toDouble();
    string toString();
    IntArray toIntArray();
    DoubleArray toDoubleArray();
    StringArray toStringArray();
    Tuple toTuple();
    KVValue& operator=(const int&);
    KVValue& operator=(const double&);
    KVValue& operator=(const string&);
    KVValue& operator=(const IntArray&);
    KVValue& operator=(const DoubleArray&);
    KVValue& operator=(const StringArray&);
    KVValue& operator=(const Tuple&);
private:
    int m_type;
    int m_int;
    double m_double;
    string m_string;
    IntArray m_intArray;
    DoubleArray m_doubleArray;
    StringArray m_stringArray;
    Tuple m_tuple;
};

/******************************************************************************
 *  KVProperty类,一个简单的读取及写入属性列表的类
 *  配置文件格式要求如下:
 *  1.一行为一条配置,以键值对的文本形式存在,如  key1 = value1
 *  2.key和value必须为可见字符串,并且必须为单词(空格作为一个单词的结束符)
 *  3.以";"号作为有效配置结束符,";"号后面直至本行结束的文本均为注释文本
 *  4.int和double用<>,string用"",数组用[],元组用().
 *  属性配置文件例子:
 *  int = <123456>          ;this is a int value.
 *  double = <3.1415926>    ;this is a double value
 *  string = "hello,world"  ;this is a string value
 *  intArray = [1,2,3,4,5,6,7,8];
 *  doubleArray = [1.0, 2.0, 3.0, 4.0];
 *  stringArray = ["one","two","three","four"];
 *  tuple = ("one",2,"three",4.0);
 *  this is a wild line
 *  ;this is a comment line
 *****************************************************************************/

/**
 *  \file   KVProperty.h   
 *  \class  KVProperty
 *  \brief  一个简单的属性列表类	
 */
class KVProperty{
public:
    KVProperty();
    ~KVProperty();
    bool initWithFile(const string fileName);
    bool saveAsFile(const string fileName);
    KVValue& operator[](string keyword);
    bool addProperty(string keyword,KVValue& value);
private:
    void parseLine(const std::string lineString, std::string& keyword, KVValue& value, std::string& comment);
private:
    typedef map<string,KVValue*> PropertyMap;
    PropertyMap m_propertyTable;
    typedef map<int,string> LineMap;
    LineMap m_lineTable;
    MutexLock m_propertyLock;
    KVValue m_defaultValue;
};
#endif
