/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __JSON_DATA_H__
#define __JSON_DATA_H__

#include "cJSON.h"
#include "Array.h"
#include <iostream>

/**
 *  \file   JSONData.h   
 *  \class  JSONNode
 *  \brief  JSON结点类.
 */
class JSONNode{
public:
    JSONNode();
    ~JSONNode();
    JSONNode(const JSONNode& node);
    std::string serialize(bool fmt=false);
    bool hasSubNode(const std::string nodeName);
    bool isNullNode();
    bool isArrayNode();
    int getArraySize();
    JSONNode operator[](const std::string nodeName);
    JSONNode operator[](int index);
    bool toBool();
    int toInt();
    double toDouble();
    std::string toString();
    JSONNode toNode();
    bool addSubNode(bool value,const std::string nodeName="");
    bool addSubNode(int value,const std::string nodeName="");
    bool addSubNode(double value,const std::string nodeName="");
    bool addSubNode(const char* value,const std::string nodeName="");
    bool addSubNode(Array& array,const std::string nodeName="");
    bool addSubNode(JSONNode* object,const std::string nodeName="");
private:
    bool addNullNode(const std::string nodeName="");
private:
    cJSON* m_object;
    std::string m_name;
    friend class JSONData;
};
/**
 *  \file   JSONData.h   
 *  \class  JSONData
 *  \brief  JSON数据类.
 */
class JSONData{
public:
    JSONData();
    ~JSONData();
    JSONNode* initWithObjectFormat();
    JSONNode* initWithArrayFormat();
    JSONNode* initWithDataString(const char* jdata);
    JSONNode* initWithContentOfFile(const std::string fileName);
    bool saveAsFile(const std::string fileName,bool fmt=false);
    std::string serialize(bool fmt=false);
private:
    JSONNode* m_rootNode;
};

#endif