/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __LOGGER_H__
#define __LOGGER_H__

#include "Mutex.h"
#include "Config.h"

#include <string>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>

#include <iostream>
#include <vector>

typedef enum{
    LOG_TYPE_LOG4Z=0,
    LOG_TYPE_CORE=0x10000,
    LOG_TYPE_INVALID = -1
}LOG_TYPE_E;

typedef enum{
    LOG_ID_MAIN=LOG_TYPE_LOG4Z,
    LOG_ID_CORE=LOG_TYPE_CORE,
    LOG_ID_INVALID=-1
}LOG_ID_E;


/** 日志信息打印(用在C中) */
#define LOG(logid,fmt,arg...)   do{LoggerManager::getInstance()->log(logid,"<@%s,L%d>"fmt,__FUNCTION__,__LINE__,##arg);}while(0)
/** 日志信息打印(用在C++中) */
#define LOG_CLASS(logid,fmt,arg...)   do{LoggerManager::getInstance()->log(logid,"<@%s::%s,L%d>"fmt,CLASSNAME_STR,__FUNCTION__,__LINE__,##arg);}while(0)

/* Logger配置文件样例(Config文件):
 ----------------------------
 logger:
 {
    core:
    {
        logPath="/var/log/core";  //日志根目录
        logFileName=".log";      //日志文件后缀
        logFileNumMax=100;       //日志文件最大个数
    };
    main:
    {
        logPath="/var/log/main";  //日志根目录
        logStoreMax=1024;        //日志最大存储容量,单位为M
        logFileSizeMax=10;       //日志文件最大大小,单位为M
        monthDirEnable=1;        //日志按月份建立文件夹
        loglist=(
        {name="sms";path="./sms/"},
        {name="call";path="./call/"},
        );  
    };
 };
 ----------------------------
 */

class LoggerManager;
/**
 *  \file   Logger.h   
 *  \class  CoreLogger
 *  \brief  核心日志类(用来存储错误信息日志).
 */
class CoreLogger{
public:
    /**
     *  \brief  获取CoreLogger单例
     *  \param  void
     *  \return CoreLogger*
     *  \note   none
     */
    static CoreLogger* getInstance()
    {
        static CoreLogger instance;
        return &instance;
    };
    ~CoreLogger();
private:
    friend class LoggerManager;
    bool initWithDefault();
    bool initWithSettings(Settings settings);
    void startLog();
    void stopLog();
    void makeLog(const char * fmt,va_list argp);
private:
    CoreLogger();
    bool clearLog(int maxfiles);
private:
    std::string m_logPath;      /* log存储目录 */
    std::string m_logName;      /* log文件后缀名 */
    int m_logFileNumMax;        /* 最大日志文件个数 */
    bool m_isStarted;
    std::string m_logFileName;  /* 日志文件全名 */    
    unsigned int m_lineNum;
    FILE* m_logfp;
    MutexLock m_logLock;
};

/**
 *  \file   Logger.h   
 *  \class  Log4zLogger
 *  \brief  Log4z日志封装类(用来存储用户日志).
 */
class Log4zLogger{
public:
    /**
     *  \brief  获取Log4zLogger单例
     *  \param  void
     *  \return Log4zLogger*
     *  \note   none
     */
    static Log4zLogger* getInstance()
    {
        static Log4zLogger instance;
        return &instance;
    };
    ~Log4zLogger();
private:
    friend class LoggerManager;
    bool initWithDefault();
    bool initWithSettings(Settings settings);
    int findLog(std::string key);
    void makeLog(int logid, const char * fmt, va_list argp);
private:
    Log4zLogger();    
private:
    MutexLock m_logLock;
    typedef struct{
        int m_id;
        std::string m_name;
        std::string m_path;
    }LoggerInfo_S;
    std::vector<LoggerInfo_S> m_loggerVect;
    std::string m_logPath;  /* log存储目录 */
    int m_logFileSizeMax;   /* 最大单个日志文件大小,单位为兆字节(M) */
    int m_logStoreSizeMax;  /* 最大日志空间,单位为兆字节(M) */
    bool m_monthDirEnable;  /* 使能月份文件夹 */
    bool m_isStarted;
};

/**
 *  \file   Logger.h   
 *  \class  LoggerManager
 *  \brief  日志管理类.
 */
class LoggerManager{
public:
    /**
     *  \brief  获取LoggerManager单例
     *  \param  void
     *  \return LoggerManager*
     *  \note   none
     */
    static LoggerManager* getInstance()
    {
        static LoggerManager instance;
        return &instance;
    }
    ~LoggerManager();
    bool initWithSettings(Settings logSettings);
    int getLogger(int type,const char * key=NULL);
    void log(int logid,const char * fmt, ...);
private:
    LoggerManager();
};


#endif

