/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __THREAD_H__
#define __THREAD_H__

#include "BaseType.h"
#include <pthread.h>
#include <unistd.h>

/**
 *  \file   Thread.h   
 *  \class  RunnableProtocol
 *  \brief  线程接口	
 */

class RunnableProtocol{
public:
	RunnableProtocol(){};
	virtual ~RunnableProtocol(){};
	virtual void run()=0;
};

typedef enum{
    THREAD_STATUS_NEW=0,
    THREAD_STATUS_START,
    THREAD_STATUS_RUNNING,
    THREAD_STATUS_EXIT,
}THREAD_STATUS_E;

typedef enum{
    TASK_STATUS_STOP=0,
    TASK_STATUS_START,
    TASK_STATUS_EXIT,
}TASK_STATUS_E;

/* 请注意:任务方法原型必须是void task(void*arg),否则在任务中可能无法使用成员变量!!! */
typedef void (RunnableProtocol::*SEL_task)(void* arg);
#define task_selector(SELECTOR) (SEL_task)(&SELECTOR)

/**
 *  \file   Thread.h   
 *  \class  Thread
 *  \brief  线程类	
 */

class Thread{
public:
	Thread();
	virtual ~Thread();
	bool start(RunnableProtocol* pRunnable);
    bool cancel();
	bool isRunning();
    static void msleep(int ms);
	pthread_t getThreadID(){return m_threadID;}
	int getThreadStatus(){return m_threadStatus;}
    static int createTask(RunnableProtocol* owner,SEL_task task,void* taskArg);
private:
    void run();
    static void* threadEntry(void *arg);
    static void* taskEntry(void *arg);
private:
	RunnableProtocol* m_pRunnableTarget;
	pthread_t m_threadID;
	pthread_attr_t m_ThreadAttr;
	THREAD_STATUS_E m_threadStatus;
};

#endif
