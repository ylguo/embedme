/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __THREAD_POOL_H__
#define __THREAD_POOL_H__
#include "Thread.h"
#include "Mutex.h"
#include <iostream>
#include <vector>
using namespace std;

class ThreadPool;

/**
 *  \file   ThreadPool.h   
 *  \class  Runnable
 *  \brief  线程运行体	
 */
class Runnable{
public:
    Runnable();
    virtual ~Runnable();
    virtual void run()=0;
    bool isRunning();
private:
    friend class ThreadPool;
    bool m_isRunning;
};

/**
 *  \file   ThreadPool.h   
 *  \class  ThreadPool
 *  \brief  线程池
 */
class ThreadPool{
public:
    static ThreadPool* getInstance()
    {
        static ThreadPool instance;
        return &instance;
    }
    ~ThreadPool();
    bool init(int maxThreadCount);
    bool start(Runnable* runnable,int priority=0);
    bool cancel(Runnable* runnable);
    int maxThreadCount();
    int idleThreadCount();
private:
    ThreadPool();
    void execRunnable(int index);
    static void* threadEntry(void *arg);
private:
    bool m_initFlag;
    MutexLock m_vectLock;
    vector<Runnable*> m_threadVect;
    int m_maxThreadCount;
    int m_usedThreadCount;
};


#endif
