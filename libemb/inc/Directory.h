/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __DIRECTORY_H__
#define __DIRECTORY_H__

#include <vector>
#include <iostream>
using namespace std;

/**
 *  \file   Directory.h   
 *  \class  Directory
 *  \brief  �ļ�Ŀ¼��.
 */
class Directory{
public:
    Directory();
    ~Directory();
    static bool isEmpty(const char* name);
    static bool isExist(const char* name);
    static int getContentSize(const char* name);
    bool enter(const string& path);
    string current();
    vector<string> listAll(); 
private:
    string m_currentPath;
};

#endif