/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/

#ifndef __OSS_AUDIO_H__
#define __OSS_AUDIO_H__
#ifdef OS_CYGWIN
#else
#include "Thread.h"
#include "File.h"
#include <iostream>
#include <vector>
using namespace std;

/** 定义OSS音频设备号 */
typedef enum{
    AUDIO_DEV_DEFAULT=0,    /**< 默认音频设备 */
    AUDIO_DEV_NUM,
}AUDIO_DEV_E;

/**
 *  \file   OssAudio.h   
 *  \class  OssAudio
 *  \brief  Oss音频设备类.
 */
 

class OssAudio:public RunnableProtocol{
public:
    /**
     *  \brief  获取OssAudio单例
     *  \param  void
     *  \return OssAudio*
     *  \note   none
     */
    static OssAudio* getInstance()
    {
        static OssAudio instance;
        return &instance;
    }
    ~OssAudio();
    bool config(int device,int channels,int rate,int bits);
    bool recordStart(int device,const string& fileName);
    bool recordStop(int device);
    bool playStart(int device,const string& fileName);
    bool playStop(int device);
private:
    OssAudio();
    void run(){};
    void recordTask(void* args);
    void playTask(void* args);
private:
    typedef struct{
        int m_chns;
        int m_rate;
        int m_bits;
        int m_flag;
        int m_recFd;
        int m_playFd;
        File* m_recFile;
        File* m_playFile;
    }AudioAttr_S;
    vector<AudioAttr_S> m_audioAttr;
};
#endif
#endif
