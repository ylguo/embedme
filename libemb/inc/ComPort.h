/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __COMPORT_H__
#define __COMPORT_H__

#include "IODevice.h"

#include <termios.h>
#include <unistd.h>

/** 串口波特率值 */
typedef enum
{
    BAUD_1200      = 1200,
    BAUD_2400      = 2400,
    BAUD_4800      = 4800,
    BAUD_9600      = 9600,
    BAUD_19200     = 19200,
    BAUD_38400     = 38400,
    BAUD_57600     = 57600,
    BAUD_115200    = 115200,
    BAUD_460800    = 460800
}BAUDRATE_E;

/** 串口数据位值 */
typedef enum
{
    DATA_5=5,
    DATA_6,
    DATA_7,
    DATA_8
}DATABITS_E;

/** 串口停止位值 */
typedef enum
{
    STOP_1=1,
    STOP_2
}STOPBITS_E;

/** 串口校验位值 */
typedef enum
{
    PARITY_NONE=0,
    PARITY_ODD,
    PARITY_EVEN,
    PARITY_SPACE
}PARITY_E;

/** 串口流控位值 */
typedef enum{
    FLOWCTRL_NONE=0,
    FLOWCTRL_HW,
    FLOWCTRL_SW
}FLOWCTRL_E;

/** 串口属性类型 */
typedef enum{
    COM_ATTR_BAUDRATE=0,
    COM_ATTR_DATABITS,
    COM_ATTR_STOPBITS,
    COM_ATTR_PARITY,
    COM_ATTR_FLOWCTRL,
}COM_ATTR_E;

/**
 *  \file   ComPort.h   
 *  \class  ComPort
 *  \brief  串口设备类.
 */
class ComPort:public IODevice{
public:
    ComPort();
    virtual ~ComPort();
    virtual bool open(const char* devName,int ioMode);
    virtual bool close();
    virtual int readData(char * buf, int len, int timeoutMs=-1);
    virtual int writeData(const char *buf, int len, int timeoutMs=-1);
    virtual int setAttribute(int attr, int value);
    virtual int getAttribute(int attr);
private:
    typedef struct
    {
        BAUDRATE_E m_bandrate;
        DATABITS_E m_databits;
        STOPBITS_E m_stopbits;
        PARITY_E   m_parity;
        FLOWCTRL_E m_flowctrl;
    }ComPortAttr_S;
    ComPortAttr_S m_attr;
    uint32 m_errorCount;
};

#endif
