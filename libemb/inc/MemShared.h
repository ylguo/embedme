/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MEM_SHARED_H__
#define __MEM_SHARED_H__

#include "IODevice.h"
#include <iostream>


typedef enum{
    MEMSHARED_ATTR_SIZE=0,
}MEMSHARED_ATTR_E;

typedef enum{
    MEMSHARED_TYPE_SHM=0,
    MEMSHARED_TYPE_FILE,
}MEMSHARED_TYPE_E;
/**
 *  \file   MemShared.h   
 *  \class  MemShared
 *  \brief  �����ڴ���
 */
class MemShared:public IODevice{
public:
    MemShared(int type);
    virtual ~MemShared();
    virtual bool open(const char *name=NULL, int ioMode=IO_MODE_INVALID);
    virtual bool close();
    virtual int readData(char *buf, int count, int timeoutMs=-1);
    virtual int writeData(const char *buf, int count, int timeoutMs=-1);
    virtual int setAttribute(int attr, int value);
    virtual int getAttribute(int attr);
    void* getMemAddr();
    void* attach();
    int detach();
    
private:
    int m_shmType;
    int m_shmSize;
    void* m_shmAddr;
};

#endif