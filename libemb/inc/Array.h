#ifndef __ARRAY_H__
#define __ARRAY_H__

#include "BaseType.h"
#include <vector>
#include <iostream>
using namespace std;

/**
 *  \file   BaseType.h   
 *  \class  Array
 *  \brief  抽象数组类	
 */
class Array{
public:
    Array();
    virtual ~Array();
    virtual bool initWithString(const string& arrayString)=0;
    int type();
    virtual int size()=0;
    virtual void clear()=0;
    virtual string serialize()=0;
protected:
    int m_type;
};


/**
 *  \file   Array.h   
 *  \class  IntArray
 *  \brief  整型数组类	
 */
class IntArray:public Array{
public:
    IntArray();
    ~IntArray();
    bool initWithString(const string& arrayString);
    string serialize();
    int size();
    void clear();
    int& operator[](int idx);
    friend IntArray& operator<<(IntArray&,int);
private:
    vector<int> m_array;
};

/**
 *  \file   Array.h   
 *  \class  DoubleArray
 *  \brief  浮点型数组类	
 */
class DoubleArray:public Array{
public:
    DoubleArray();
    ~DoubleArray();
    bool initWithString(const string& arrayString);
    string serialize();
    int size();
    void clear();
    double& operator[](int idx);
    friend DoubleArray& operator<<(DoubleArray&,double);
private:
    vector<double> m_array;
};

/**
 *  \file   Array.h   
 *  \class  StringArray
 *  \brief  字符串型数组类	
 */
class StringArray:public Array{
public:
    StringArray();
    ~StringArray();
    bool initWithString(const string& arrayString);
    string serialize();
    int size();
    void clear();
    string& operator[](int idx);
    friend StringArray& operator<<(StringArray&,string);
private:
    vector<string> m_array;
};
#endif
