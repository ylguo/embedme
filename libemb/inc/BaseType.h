/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __BASETYPE_H__
#define __BASETYPE_H__

/** 
 *  \file  BaseType.h
 *  \brief 
 */
typedef signed char        sint8;
typedef signed short       sint16;
typedef signed int         sint32;
typedef signed long long   sint64;
typedef unsigned char      uint8;
typedef unsigned short     uint16;
typedef unsigned int       uint32;
typedef unsigned long long uint64;

/** 定义函数返回值 */
typedef enum
{
    STATUS_OK = 0,      /**< 返回值:正常 */
    STATUS_ERROR = -1,  /**< 返回值:出错 */
    STATUS_TIMEOUT = -2,/**< 返回值:超时 */
}STATUS_E;

/** 定义数据类型 */
typedef enum
{
    BASETYPE_NONE=0,        /**< 未知类型 */
    BASETYPE_INT,           /**< 整数 */
    BASETYPE_DOUBLE,        /**< 浮点数 */
    BASETYPE_STRING,        /**< 字符串 */
    BASETYPE_INTARRAY,      /**< 整数数组 */
    BASETYPE_DOUBLEARRAY,   /**< 浮点数数组 */
    BASETYPE_STRINGARRAY,   /**< 字符串数组 */
    BASETYPE_TUPLE,         /**< 元组 */
}BASETYPE_E;

/** 计算数组大小(元素个数) \a array.*/
#define ARRAY_SIZE(array)   (sizeof(array)/sizeof(array[0]))    

/** 判断一个字符是否是十进制数字 \a ch.*/
#define IS_DIGITAL(ch)   ((((ch)>='0')&&((ch)<='9'))?true:false)

/** 判断一个字符是否是16进制数字 \a ch.*/
#define IS_HEXDIG(ch)    (((((ch)>='0')&&((ch)<='9'))||(((ch)>='A')&&((ch)<='F'))||(((ch)>='a')&&((ch)<='f')))?true:false)

/** 判断一个字符是否是大写字母 \a ch.*/
#define IS_CAPITAL(ch)   ((((ch)>='A')&&((ch)<='Z'))?true:false)

/** 判断一个字符是否是字母 \a ch.*/
#define IS_ALPHABET(ch)  (((((ch)>='A')&&((ch)<='Z'))||(((ch)>='a')&&((ch)<='b')))?true:false)

/** 返回 \a x 和 \a y 中较大值. */
#define MAX(x,y) (((x)>(y))?(x):(y))

/** 返回 \a x 和 \a y 中较小值. */
#define MIN(x,y) (((x)<(y))?(x):(y))

/** 在 \a min 和 \a max 区间中取与 \a x 大小较接近的数值. */
#define CLIP(min,x,max) (((x)>(max))?(max):(((x)<(min))?(min):(x)))

#define PI              (3.1415926)     /**< 圆周率. */
#define SQUARE(x)       ((x)*(x))       /**< \a x 的平方. */
#define R_AREA(r)       ((PI)*(r)*(r))  /**< 计算半径为\a r 的圆的面积. */

#define BIT_GET(value,bit)      (!!((0x0001<<(bit))&(value))) /**< 获取value的第bit位的值 */
#define BIT_SET(value,bit)      ((0x0001<<(bit))|(value))       /**< 设置value的第bit位的值 */
#define BIT_CLR(value,bit)      ((~(0x0001<<(bit)))&(value))    /**< 清除value的第bit位的值 */

#endif
