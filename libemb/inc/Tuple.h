/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __TUPLE_H__
#define __TUPLE_H__

#include "BaseType.h"
#include "Array.h"
#include <iostream>
#include <vector>
using namespace std;


/**
 *  \file   Tuple.h   
 *  \class  TupleItem
 *  \brief  元组元素	
 */
 
class TupleItem{
public:
    TupleItem(int);
    TupleItem(double);
    TupleItem(string);
    TupleItem(const TupleItem&);
    virtual ~TupleItem();
    int baseType();
    int toInt();
    double toDouble();
    string toString();
private:
    int m_type;
    double m_value;
    string m_string;
};

/**
 *  \file   Tuple.h   
 *  \class  Tuple
 *  \brief  元组	
 */
class Tuple:public Array{
public:
    Tuple();
    Tuple(const Tuple&);
    virtual ~Tuple();
    bool initWithString(const string& tupleString);
    int size();
    int type();
    void clear();
    string serialize();
    TupleItem& operator[](int idx);
    Tuple& operator=(const Tuple&);
    friend Tuple& operator<<(Tuple&,int);
    friend Tuple& operator<<(Tuple&,double);
    friend Tuple& operator<<(Tuple&,const char*);
    friend Tuple& operator<<(Tuple&,TupleItem&);
    friend ostream& operator<<(ostream&,Tuple&);
private:
    vector<TupleItem*> m_valueVect;
};
#endif