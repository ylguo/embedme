/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __UTILS_H__
#define __UTILS_H__

#include "BaseType.h"
#include <iostream>
#include <vector>
using namespace std;

/**
 *  \file   Utils.h   
 *  \class  Utils
 *  \brief  ������	
 */
class Utils{
public:
    static int random(int max);
    static bool isBigEndian(void);
    static uint16 host2LitEndian(uint16 value);
    static uint32 host2LitEndian(uint32 value);
    static uint16 host2BigEndian(uint16 value);
    static uint32 host2BigEndian(uint32 value);
    static string unicodeOneToUtf8String(uint32 unicode);
    static uint32 utf8OneToUnicode(const char* utf8code);
    static string toLower(const char* str);
    static string toUpper(const char* str);
    static string findString(const string& source, const string& start, const string& end);
    static string findString(const string& source, const string& pattern, const string& before, const string& after);
    static string trimBeginWith(const string& source,const string& trimch);
    static string trimEndingBlank(const string& source);
    static string trimAllBlank(const string& source);
    static vector<string> cutString(const string& source,const string& startFlag,const string& stopFlag,const string& cutFlag);
    static int stringPatternCount(const string& source, const string& pattern);
    static sint8 ascii2digital(char ch);
    static char digital2ascii(uint8 val);
    static int string2code(const string codestr,char* codebuf,int len);
    static string code2string(const char* codebuf,int len);
    static uint16 bitsReverse(uint16 value, uint8 bits);
    static string trimFilePath(const char* filePath);
	static int eval(const char* expression);
};
#endif