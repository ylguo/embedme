/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MD5_CHECK_H__
#define __MD5_CHECK_H__
#include "BaseType.h"
#include <iostream>

/**
 *  \file   MD5Check.h   
 *  \class  MD5_Context_S
 *  \brief  none
 */
typedef struct {
    uint32  A,B,C,D;	  /* chaining variables */
    uint32  nblocks;
    uint8   buf[64];
    sint32  count;
}MD5_Context_S;

/**
 *  \file   MD5Check.h   
 *  \class  MD5Check
 *  \brief  MD5У����
 */
class MD5Check{
public:
    MD5Check();
    ~MD5Check();
    int checkFile(std::string fileName,std::string& md5sum);
    int checkString(std::string srcString,std::string& md5sum);
private:
    void initContext(MD5_Context_S *ctx);
    void transform( MD5_Context_S* ctx, uint8* data);
    void md5Write(MD5_Context_S *hd, uint8* inbuf, int inlen);
    void md5Final(MD5_Context_S *hd);
};

#endif