#****************************************************************************
#
# Makefile for TinyXml test.
# Lee Thomason
# www.grinninglizard.com
#
# This is a GNU make (gmake) makefile
#****************************************************************************

# DEBUG can be set to YES to include debugging info, or NO otherwise
DEBUG          := NO

# PROFILE can be set to YES to include profiling info, or NO otherwise
PROFILE        := NO

# TINYXML_USE_STL can be used to turn on STL support. NO, then STL
# will not be used. YES will include the STL files.
TINYXML_USE_STL := NO

#****************************************************************************

INSTALL_DIR := $(PREFIX)

ifeq ($(HOST),)
CROSS_COMPILE:=
else
CROSS_COMPILE:=$(HOST)-
endif

CC     := ${CROSS_COMPILE}gcc
CXX    := ${CROSS_COMPILE}g++
LD     := ${CROSS_COMPILE}g++
AR     := ${CROSS_COMPILE}ar rc
RANLIB := ${CROSS_COMPILE}ranlib

DEBUG_CFLAGS     := -Wall -Wno-format -g -DDEBUG
RELEASE_CFLAGS   := -Wall -Wno-unknown-pragmas -Wno-format -O3

LIBS		 :=

DEBUG_CXXFLAGS   := ${DEBUG_CFLAGS} 
RELEASE_CXXFLAGS := ${RELEASE_CFLAGS}

DEBUG_LDFLAGS    := -g
RELEASE_LDFLAGS  :=

ifeq (YES, ${DEBUG})
   CFLAGS       := ${DEBUG_CFLAGS}
   CXXFLAGS     := ${DEBUG_CXXFLAGS}
   LDFLAGS      := ${DEBUG_LDFLAGS}
else
   CFLAGS       := ${RELEASE_CFLAGS}
   CXXFLAGS     := ${RELEASE_CXXFLAGS}
   LDFLAGS      := ${RELEASE_LDFLAGS}
endif

ifeq (YES, ${PROFILE})
   CFLAGS   := ${CFLAGS} -pg -O3
   CXXFLAGS := ${CXXFLAGS} -pg -O3
   LDFLAGS  := ${LDFLAGS} -pg
endif

#****************************************************************************
# Preprocessor directives
#****************************************************************************

ifeq (YES, ${TINYXML_USE_STL})
  DEFS := -DTIXML_USE_STL
else
  DEFS :=
endif

#****************************************************************************
# Include paths
#****************************************************************************

#INCS := -I/usr/include/g++-2 -I/usr/local/include
INCS :=


#****************************************************************************
# Makefile code common to all platforms
#****************************************************************************

CFLAGS   := ${CFLAGS}   ${DEFS}
CXXFLAGS := ${CXXFLAGS} ${DEFS}

ifeq ($(LINKTYPE),shared)
CFLAGS += -fPIC
CXXFLAGS += -fPIC
endif

#****************************************************************************
# Targets of the build
#****************************************************************************

LIB_NAME := libtinyxml

ifeq ($(LINKTYPE),shared)
TARGET := ${LIB_NAME}.so
else
TARGET := ${LIB_NAME}.a
endif

all: ${TARGET}

#****************************************************************************
# Source files
#****************************************************************************

SRCS := tinyxml.cpp tinyxmlparser.cpp tinyxmlerror.cpp tinystr.cpp

# Add on the sources for libraries
SRCS := ${SRCS}

OBJS := $(addsuffix .o,$(basename ${SRCS}))

#****************************************************************************
# Output
#****************************************************************************

${LIB_NAME}.a: ${OBJS}
	${AR} $@ ${OBJS}
${LIB_NAME}.so: ${OBJS}
	${CC} -fPIC -shared -o $@ ${OBJS}
#****************************************************************************
# common rules
#****************************************************************************

# Rules for compiling source files to object files
%.o : %.cpp
	${CXX} -c ${CXXFLAGS} ${INCS} $< -o $@

%.o : %.c
	${CC} -c ${CFLAGS} ${INCS} $< -o $@

install:
	-mkdir ${INSTALL_DIR}/include
	-mkdir ${INSTALL_DIR}/lib
	cp -pR tinyxml.h ${INSTALL_DIR}/include/
	cp -pR tinystr.h ${INSTALL_DIR}/include/
	cp -pR ${TARGET} ${INSTALL_DIR}/lib/
dist:
	bash makedistlinux

clean:
	-rm -f core ${OBJS} *.a *.so

depend:
	#makedepend ${INCS} ${SRCS}

tinyxml.o: tinyxml.h tinystr.h
tinyxmlparser.o: tinyxml.h tinystr.h
xmltest.o: tinyxml.h tinystr.h
tinyxmlerror.o: tinyxml.h tinystr.h
